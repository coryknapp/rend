//
//  MountainRegionGenerator.h
//  Strategy
//
//  Created by Cory Knapp on 8/24/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__MountainRegionGenerator__
#define __Strategy__MountainRegionGenerator__


#include <iostream>

#include "RegionGeneratorAbstract.h"

#include "PossionDisk.h"
#include "StageState.h"
#include "Geometry.h"

#include "ContainerAssist.h"

#include <deque>
#include <set>

class MountainRegionGenerator : public RegionGeneratorAbstract {
	
	typedef
	PossonDiscGenerator<
	Waypoint,
	WaypointSetFirstCoordFunctor,
	WaypointSetSecondCoordFunctor,
	WaypointGetFirstCoordFunctor,
	WaypointGetSecondCoordFunctor,
	WaypointNetwork,
	WaypointNetworkAddFunctor
	> generatorType;
	
	std::unique_ptr< generatorType > _generator;
	WaypointNetwork * _network;

	std::deque<Waypoint*> openList;
	
public:
	MountainRegionGenerator();
	void setNetwork( WaypointNetwork * network );
	bool iterate();
	
	void connectNodesInternally();
};

#endif /* defined(__Strategy__MountainRegionGenerator__) */
