//
//  FlyCameraInputInterpreter.cpp
//  Strategy
//
//  Created by Cory Knapp on 7/15/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "FlyCameraInputInterpreter.h"
#include "GreaterContext.h"// this causes a cyclical dependancy :(
#include "Waypoint.h"

void FlyCameraInputInterpreter::_clickPoint( float * results, const float &z ){

    
	
    glm::mat4 projMat = greaterContext()->userCamera()->projectionMatrix();
	glm::mat4 viewMat = greaterContext()->userCamera()->viewTransformMatrix();
	float windowWidth = greaterContext()->userCamera()->viewWidth();
	float windowHeight= greaterContext()->userCamera()->viewHeight();
	
	auto eyeLoc = greaterContext()->userCamera()->location();
	
    //viewport space is differnt from screen space.
    float adjMouseX = static_cast<float>(_lastMouseX)*2
        /greaterContext()->userCamera()->viewWidth() - 1;
    float adjMouseY = static_cast<float>(_lastMouseY)*2
        /greaterContext()->userCamera()->viewHeight() - 1;

	auto ray =
	glm::unProject(glm::vec3( _lastMouseX, _lastMouseY, 100.0f ), // click location
				   viewMat,
				   projMat,
				   glm::vec4( 0, 0, windowHeight, windowWidth) );
	
	float p1[3] = {0,0,z};
	float p2[3] = {1,0,z};
	float p3[3] = {0,1,z};
	float intersection[4];
	linePlaneIntersection(intersection,
						  glm::value_ptr( eyeLoc ),
						  glm::value_ptr( ray ),
						  p1, p2, p3);
    std::cout <<"eye = " << eyeLoc[0] << "\t\t"
		<< eyeLoc[1] << "\t\t" << eyeLoc[2] << "\t\t"
		<< eyeLoc[3] << "\n";
    std::cout << "world = "<< ray[0] << "\t\t"
		<< ray[1] << "\t\t"
		<< ray[2] << "\n";
    std::cout << "int = "<< results[0] << "\t\t"
		<< intersection[1] << "\t\t"
		<< intersection[2] << "\t\t"
		<< intersection[3] << "\n";
    if( keyDown( 'v' ) ){

    }
	results[0] = ray[0];
	results[1] = ray[1];
	results[2] = ray[2];
}

Waypoint * FlyCameraInputInterpreter::_closestWayPoint(){
}

void FlyCameraInputInterpreter::adjustCamera(){
    greaterContext()->userCamera()->setForwardMovment( forwardKey()-backwardKey() );
    greaterContext()->userCamera()->setLeftMovment( leftKey()-rightKey() );
    greaterContext()->userCamera()->setUpMovment( upKey()-downKey());
    
}

void FlyCameraInputInterpreter::mouseUp( unsigned x, unsigned y, int button ){
    _mouseButtonMap[button] = false;
}

void FlyCameraInputInterpreter::mouseMove( unsigned x, unsigned y, int button){
    // only move if the shift key is pressed
    _lastMouseX = x;
    _lastMouseY = y;
    auto dPair = mouseDelta(); // always call for the delta to clear it.
    if( modifierKeyDown(0) ){
        greaterContext()->userCamera()->mouseDrag(dPair.first, dPair.second);
    }
}

void FlyCameraInputInterpreter::mouseDown( unsigned x, unsigned y, int button ){
    std::cout << "CLICK==================================\n";
    _mouseButtonMap[button] = true;
	float clickPoint[4];
	_clickPoint( clickPoint, 0.0);
	
	// create new object
	GameObjectID myNewObjectID =
		greaterContext()->currentStage()._gameObjects.create();
	GameObjectRef myNewObject =
		greaterContext()->currentStage()._gameObjects.object( myNewObjectID );
	
	// set up coords.
	myNewObject->install<CoordBasedWorldGameObjectComponent>();
	myNewObject->get<CoordBasedWorldGameObjectComponent>().i = clickPoint[0];
	myNewObject->get<CoordBasedWorldGameObjectComponent>().j = clickPoint[1];

	myNewObject->install<DrawnGameObjectComponent>();
	myNewObject->get<DrawnGameObjectComponent>()._rendAssetID = 0;
}

void FlyCameraInputInterpreter::setKeyDown( keyCode k ){
    UserInputInterpreter::setKeyDown(k);
    adjustCamera();
}

void FlyCameraInputInterpreter::setKeyUp( keyCode k ){
    UserInputInterpreter::setKeyUp(k);
    adjustCamera();
}

int FlyCameraInputInterpreter::forwardKey(){
    return keyDown('w');
}

int FlyCameraInputInterpreter::backwardKey(){
    return keyDown( 's' );
}

int FlyCameraInputInterpreter::leftKey(){
    return keyDown( 'a' );
}

int FlyCameraInputInterpreter::rightKey(){
    return keyDown( 'd' );
}

int FlyCameraInputInterpreter::upKey(){
    return keyDown( 'r' );
}

int FlyCameraInputInterpreter::downKey(){
    return keyDown( 'f' );
}

std::pair<int, int> FlyCameraInputInterpreter::mouseDelta(){
    std::pair<int, int> ret( _lastMouseX - _lastReportedX, _lastMouseY - _lastReportedY);
    _lastReportedX = _lastMouseX;
    _lastReportedY = _lastMouseY;
    return ret;
}
