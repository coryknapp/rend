//
//  Geometry.cpp
//  Strategy
//
//  Created by Cory Knapp on 7/31/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "Geometry.h"

void normalizeVector( const size_t &size, float * vector){
	int i;
	float length = 0;
	for( i = 0; i < size; i++)
		length += vector[i] * vector[i];
	for( i = 0; i < size; i++)
		vector[i] /= length;
}

float distance2( const float * a, const float * b ){
	float distance = 0;
	for( int i = 0; i < 2; i++)
		distance += (a[i] - b[i]) * (a[i] - b[i]);
	return distance;
}
bool isPointOnLine(float pai, float paj, float pbi, float pbj, float qi, float qj) {
    // Move the image, so that a.first is on (0|0)
    float ai = pbi - pai;
    float aj = pbj - paj;
    float bi = qi - pai;
    float bj = qj - paj;
    float r = (ai * bj) - (bi * aj);
    return fabs(r) < .000001;
}

bool isPointOnLine3( float * l1, float * l2, float * p ){
	// make sure l1 and l2 form a line
	if((fabsf(l1[0]-l2[0])<.00001)&&
	   (fabsf(l1[1]-l2[1])<.00001)&&
	   (fabsf(l1[2]-l2[2])<.00001) )
		return false;
	return (
		isPointOnLine( l1[0], l1[1], l2[0], l2[1], p[0], p[1]) &&
		isPointOnLine( l1[0], l1[2], l2[0], l2[2], p[0], p[2])
			);
}

bool isPointRightOfLine(float pai, float paj, float pbi, float pbj, float qi, float qj) {
    float ai = pbi - pai;
    float aj = pbj - paj;
    float bi = qi - pai;
    float bj = qj - paj;
    float r = (ai * bj) - (bi * aj);
    return r < 0;
}

bool lineSegmentCrossesLine(float pai, float paj, float pbi, float pbj,
                            float qai, float qaj, float qbi, float qbj) {
    //!!! sort of unclear that p is the line and q is the segment
	return
    (isPointRightOfLine(pai, paj, pbi, pbj, qai, qaj) ^
     isPointRightOfLine(pai, paj, pbi, pbj, qbi, qbj) );
}

bool doSegmentsIntersect(float pai, float paj, float pbi, float pbj,
                      float qai, float qaj, float qbi, float qbj) {
    // if the lines share an end point, they can't cross :)
    if( (pai == qai) && (paj == qaj))
        return false;
    if( (pbi == qai) && (pbj == qaj))
        return false;
    if( (pbi == qbi) && (pbj == qbj))
        return false;
    if( (pai == qai) && (pbj == qbj))
        return false;
    return  lineSegmentCrossesLine( pai, paj, pbi, pbj,
                                   qai, qaj, qbi, qbj) &&
    lineSegmentCrossesLine( qai, qaj, qbi, qbj,
                           pai, paj, pbi, pbj);
}

void linesIntersection(float * result,
					   const float * a1,
					   const float * a2,
					   const float * b1,
					   const float * b2){
	// !!! optimize.  the diviser is the same for both elements.
	result[0] = ( (a1[0]*a2[1]-a2[0]*a1[1])*(b1[0]-b2[0]) -
				  ( a1[0]-a2[0])*(b1[0]*b2[1]-b1[1]*b2[0]) ) /
		((a1[0]-a2[0])*(b1[1]-b2[1]) - (a1[1]-a2[1])*(b1[0]-b2[1]));
	result[1] = ( (a1[0]*a2[1]-a2[0]*a1[1])*(b1[1]-b2[1]) -
				 (a1[1]-a2[1])*(b1[0]*b2[1]-b1[1]*b2[0]) ) /
	((a1[0]-a2[0])*(b1[1]-b2[1]) - (a1[1]-a2[1])*(b1[0]-b2[1]));
}

bool linePlaneIntersection(float * result,
						   float * l1, float * l2,
						   float * p1, float * p2, float * p3){

	// should be better commented !!!
	
	// make sure l1 and l2 form a line
	if(	(fabsf(l1[0]-l2[0])<.00001)&&
		(fabsf(l1[1]-l2[1])<.00001)&&
		(fabsf(l1[2]-l2[2])<.00001) )
		 return false;
	
	// make sure the ps form a plane
	if( isPointOnLine3( p1, p2, p3 ))
		return false;
	
	glm::mat3 mat(glm::vec3(l1[0] - l2[0],
							l1[1] - l2[1],
							l1[2] - l2[2]) ,
				  glm::vec3(p2[0] - p1[0],
							p2[1] - p1[1],
							p2[2] - p1[2]) ,
				  glm::vec3(p3[0] - p1[0],
							p3[1] - p1[1],
							p3[2] - p1[2]) );
	
	glm::vec3 vec(	l1[0] - p1[0],
					l1[1] - p1[1],
					l1[2] - p1[2] );
	glm::mat3 invMat = glm::inverse( mat );
	glm::vec3 stu = invMat * vec;
	result[0] = l1[0] + (l2[0]-l1[0])*stu[0];
	result[1] = l1[1] + (l2[1]-l1[1])*stu[0];
	result[2] = l1[2] + (l2[2]-l1[2])*stu[0];
	result[3] = 1.0;

	return true;
}

float distanceFromPointToSegment(const float * l1,
								 const float * l2,
								 const float * const p){
	//determine if the the point closest to p on l is on the line, or one of
	//the end points
	const float l_length = distance2( l1, l2 );//

	float v[3] = {	p[0] - l1[0],
					p[1] - l1[1], 0 };
	float w[3] = {	l2[0] - l1[0],
					l2[1] - l1[1], 0 };
	const float t = (v[0] * w[0] + v[1] * w[1]) / (l_length * l_length);
	if (t < 0.0){
		return distance2(p, l1);
	}
	else if (t > 1.0){
		return distance2( p, l2);
	}
	
	float projection[2] = { l1[0] + t * (w[0]),
							l1[1] + t * (w[1])};
	
	return distance2(p, projection);
}