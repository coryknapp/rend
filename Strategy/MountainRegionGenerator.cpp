//
//  MountainRegionGenerator.cpp
//  Strategy
//
//  Created by Cory Knapp on 8/24/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "MountainRegionGenerator.h"


MountainRegionGenerator::MountainRegionGenerator(){
	
}

void MountainRegionGenerator::setNetwork( WaypointNetwork * network ){
	_network = network;
	RegionGeneratorAbstract::setNetwork( _network );
	_generator.reset(
		new generatorType(*_network,
						  static_cast<RegionGeneratorAbstract*>( this )
						  )
					 );
}

bool MountainRegionGenerator::iterate(){
	return _generator->iterate();
}

void MountainRegionGenerator::connectNodesInternally(){
	
	const float minListingDistance = 5;
	
	// add all points to the open list
	for(auto m = _network->begin();
		m != _network->end();
		m++){
		if( m->region == _rId )
			openList.push_back( *m );
		
	}// end m loop
		
	while( !openList.empty() ){
		auto waypoint = openList[0];
		auto closePointList = closestWaypointsTo( *waypoint, *_network, 5 );
	}
}