//
//  FlyCameraInputInterpreter.h
//  Strategy
//
//  Created by Cory Knapp on 7/15/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__FlyCameraInputInterpreter__
#define __Strategy__FlyCameraInputInterpreter__

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "UserInputInterpreter.h"
#include "GreaterContextAware.h"

class Waypoint;

class FlyCameraInputInterpreter : public UserInputInterpreter, public GreaterContextAware {

    static const unsigned _mouseButtonCount = 3;
    bool _mouseButtonMap[ _mouseButtonCount ];
    
    unsigned _lastMouseX;
    unsigned _lastMouseY;
    
    unsigned _lastReportedX = 0;
    unsigned _lastReportedY = 0;
    
    void adjustCamera();
    
	void m_windowToScreenSpace(const float * const window, float * screen);
	void _clickPoint( float * results, const float &z );
    Waypoint * _closestWayPoint();
	
	
	
public:
    
    void mouseUp( unsigned x, unsigned y, int button );
    void mouseDown( unsigned x, unsigned y, int button );
    
    void mouseMove( unsigned x, unsigned y, int button);
   
    void setKeyUp( keyCode );
    
    void setKeyDown( keyCode );
    
    int forwardKey();
    int backwardKey();
    int leftKey();
    int rightKey();
    int upKey();
    int downKey();

    std::pair<int, int> mouseDelta();

};

#endif
