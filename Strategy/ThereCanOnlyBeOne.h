//
//  ThereCanOnlyBeOne.h
//
//  Created by Cory Knapp on 8/4/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __ThereCanOnlyBeOne__
#define __ThereCanOnlyBeOne__

#include <cassert>
#include <map>

namespace ThereCanOnlyBeOneNS{
	static std::map<const std::type_info*,bool> ThereCanOnlyBeOneMap;
}

template <typename T>
class ThereCanOnlyBeOne {
	
public:
	ThereCanOnlyBeOne(){
		auto v = ThereCanOnlyBeOneNS::ThereCanOnlyBeOneMap.find( &typeid(T) );
		if( v == ThereCanOnlyBeOneNS::ThereCanOnlyBeOneMap.end() )
			ThereCanOnlyBeOneNS::ThereCanOnlyBeOneMap[ &typeid(T) ] = true;
		else
			assert( false );
	}
	
	~ThereCanOnlyBeOne(){
		auto v = ThereCanOnlyBeOneNS::ThereCanOnlyBeOneMap.find( &typeid(T) );
		ThereCanOnlyBeOneNS::ThereCanOnlyBeOneMap.erase( v );
	}
};

#endif
