//
//  GameObjectManagerTests.m
//  GameObjectManagerTests
//
//  Created by Cory Knapp on 8/5/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "GameObjectManager.h"

class testComponent{
	//
};

class testComponentWithMembers{
public:
	int value = 0;
};

@interface GameObjectManagerTests : XCTestCase
{
	GameObjectManager * gom;
}
@end

@implementation GameObjectManagerTests

- (void)setUp
{
    [super setUp];
	gom = new GameObjectManager;
}


- (void)tearDown
{
	delete gom;
    [super tearDown];
}

- (void)testExists{
	GameObjectID gOId = 999;
	XCTAssertFalse( gom->exists( gOId ) );
}

- (void)testAddAndGet
{
	GameObjectID gOId = gom->create();
	GameObjectRef object1 = gom->object( gOId );
	object1->install<testComponent>();
	
	GameObjectRef object2 = gom->object( gOId );
	XCTAssertEqual( object1, object2 );
	XCTAssertTrue( object2->checkComponent<testComponent>() );
	XCTAssertFalse( object2->checkComponent<testComponentWithMembers>() );
}

- (void)testComponenetWithMembers
{
	GameObjectID gOId = gom->create();
	GameObjectRef object1 = gom->object( gOId );
	object1->install<testComponentWithMembers>();
	object1->get<testComponentWithMembers>().value = 999;
	
	GameObjectRef object2 = gom->object( gOId );
	XCTAssertTrue( object2->checkComponent<testComponentWithMembers>() );
	XCTAssertFalse( object2->checkComponent<testComponent>() );
	XCTAssertEqual( object2->get<testComponentWithMembers>().value , 999);

	
}

@end
