//
//  Geometry.h
//  Strategy
//
//  Created by Cory Knapp on 7/31/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__Geometry__
#define __Strategy__Geometry__

#include <math.h>
#include <stddef.h>
#include <glm/glm.hpp>

void normalizeVector( const size_t &size, float * vector);
float distance2( const float * a, const float * b );

bool isPointOnLine(float pai, float paj, float pbi, float pbj,
				   float qi, float qj);
bool isPointOnLine3( float * l1, float * l2, float * p );
bool isPointRightOfLine(float pai, float paj, float pbi, float pbj,
						float qi, float qj);
bool lineSegmentCrossesLine(float pai, float paj, float pbi, float pbj,
                            float qai, float qaj, float qbi, float qbj);
bool doSegmentsIntersect(float pai, float paj, float pbi, float pbj,
                      float qai, float qaj, float qbi, float qbj);

void linesIntersection(float * result,
					   const float * a1,
					   const float * a2,
					   const float * b1,
					   const float * b2);
bool linePlaneIntersection(float * result,
						   float * l1, float * l2,
						   float * p1, float * p2, float * p3);

float distanceFromPointToSegment(	const float * l1,
									const float * l2,
									const float * p);

#endif /* defined(__Strategy__Geometry__) */
