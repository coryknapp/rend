//
//  UserInputInterpreter.cpp
//  Strategy
//
//  Created by Cory Knapp on 7/13/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "UserInputInterpreter.h"
void UserInputInterpreter::mouseUp( unsigned x, unsigned y, int button ){
    
}

void UserInputInterpreter::mouseDown( unsigned x, unsigned y, int button ){

}
    
void UserInputInterpreter::mouseMove( unsigned x, unsigned y, int button){
    
}
    

void UserInputInterpreter::setKeyDown( keyCode k ){
    _keyDownMap[k] = true;
}

void UserInputInterpreter::setKeyUp( keyCode k ){
    _keyDownMap[k] = false;
}

bool UserInputInterpreter::keyDown( keyCode k ){
    return _keyDownMap[k];
}

void UserInputInterpreter::setModifierKeysDown( int keyNum ){
    // !!! bound checking? assert
    _modDownMap[keyNum] = true;
}

void UserInputInterpreter::setModifierKeysUp( int keyNum ){
    // !!! bound checking? assert
    _modDownMap[keyNum] = false;
}

bool UserInputInterpreter::modifierKeyDown( int keyNum ){
    return _modDownMap[keyNum];
}

std::pair<unsigned, unsigned> UserInputInterpreter::mouseLocation(){
    return std::pair<unsigned, unsigned>( 0, 0 );
}