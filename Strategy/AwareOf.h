//
//  AwareOf.h
//  Strategy
//
//  Created by Cory Knapp on 7/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef Strategy_AwareOf_h
#define Strategy_AwareOf_h

template <typename T>
class AwareOf {
    
    
    T * _v = nullptr;
    
public:
    
    void set( T * v ){
        _v = v;
    }
    
    T * get(){
        return _v;
    }
    
};


#endif
