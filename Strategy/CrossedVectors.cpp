//
//  CrossedVectors.cpp
//  Strategy
//
//  Created by Cory Knapp on 7/12/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "CrossedVectors.h"

bool isPointOnLine(float pai, float paj, float pbi, float pbj, float qi, float qj) {
    // Move the image, so that a.first is on (0|0)
    float ai = pbi - pai;
    float aj = pbj - paj;
    float bi = qi - pai;
    float bj = qj - paj;
    float r = (ai * bj) - (bi * aj);
    return fabs(r) < .000001;
}

bool isPointRightOfLine(float pai, float paj, float pbi, float pbj, float qi, float qj) {
    float ai = pbi - pai;
    float aj = pbj - paj;
    float bi = qi - pai;
    float bj = qj - paj;
    float r = (ai * bj) - (bi * aj);
    return r < 0;
}

bool lineSegmentCrossesLine(float pai, float paj, float pbi, float pbj,
                            float qai, float qaj, float qbi, float qbj) {
    return
    (isPointRightOfLine(pai, paj, pbi, pbj, qai, qaj) |
     isPointRightOfLine(pai, paj, pbi, pbj, qbi, qbj) );
}

bool doLinesIntersect(float pai, float paj, float pbi, float pbj,
                      float qai, float qaj, float qbi, float qbj) {
    // hack !!! no good.
    // if the lines share an end point, they can't cross :)
    if( (pai == qai) && (paj == qaj))
        return false;
    if( (pbi == qai) && (pbj == qaj))
        return false;
    if( (pbi == qbi) && (pbj == qbj))
        return false;
    if( (pai == qai) && (pbj == qbj))
        return false;
    return  lineSegmentCrossesLine( pai, paj, pbi, pbj,
                                   qai, qaj, qbi, qbj) &&
    lineSegmentCrossesLine( qai, qaj, qbi, qbj,
                           pai, paj, pbi, pbj);
}