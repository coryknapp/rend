//
//  ContainerAssist_Tests.m
//  ContainerAssist Tests
//
//  Created by Cory Knapp on 8/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#import <XCTest/XCTest.h>

#include <set>
#include "ContainerAssist.h"

@interface ContainerAssist_Tests : XCTestCase

@end

@implementation ContainerAssist_Tests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInsertWithLimit
{
	std::set<int, std::greater<int>> list;
	insertWithLimit(list, 4, 3);
	insertWithLimit(list, 2, 3);
	insertWithLimit(list, 3, 3);
	insertWithLimit(list, 1, 3);
	insertWithLimit(list, -1, 3);
	insertWithLimit(list, 0, 3);
	insertWithLimit(list, 6, 3);
	
	auto iter = list.begin();
	XCTAssertEqual(*iter++, 6);
	XCTAssertEqual(*iter++, 4);
	XCTAssertEqual(*iter++, 3);
	XCTAssertEqual(list.size(), 3);
}

@end
