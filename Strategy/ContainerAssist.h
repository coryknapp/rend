//
//  ContainerAssist.h
//  Strategy
//
//  Created by Cory Knapp on 8/14/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__ContainerAssist__
#define __Strategy__ContainerAssist__

#include <iostream>

template <typename C, typename T>
void insertWithLimit(C &list,
					 const T& object,
					 const size_t &limit ){
	list.insert( object);
	if( list.size() > limit ){
		auto lastElement = list.rbegin();
		list.erase( *lastElement );
	}
}

#endif