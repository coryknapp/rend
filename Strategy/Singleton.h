//
//  Singleton.h
//  Strategy
//
//  Created by Cory Knapp on 8/6/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__Singleton__
#define __Strategy__Singleton__

#include <map>
#include <memory>
namespace SingletonNS{
	static std::map<
					const std::type_info*,
					void *
					> singletonMap;
}

template <typename T>
static inline
T * getSingletonPtr(){
	if( SingletonNS::singletonMap.find(&typeid(T)) ==
			SingletonNS::singletonMap.end() ){
		SingletonNS::singletonMap[&typeid(T)] = static_cast<void*>(new T);
	}
	return static_cast<T*>( SingletonNS::singletonMap[&typeid(T)] );
}

template<typename T>
void deleteSingleton(){
	auto v = SingletonNS::singletonMap.find(&typeid(T));
	SingletonNS::singletonMap.erase(v);
}

template <typename T>
class Singleton{
public:
	Singleton(){
		SingletonNS::singletonMap[&typeid(T)] = static_cast<void*>(this);
	}
};

#endif /* defined(__Strategy__Singleton__) */
