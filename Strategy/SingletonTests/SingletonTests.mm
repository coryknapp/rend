//
//  SingletonTests.m
//  SingletonTests
//
//  Created by Cory Knapp on 8/6/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "Singleton.h"

class testClass : public Singleton<testClass>{
public:
	int value = 0;
};

@interface SingletonTests : XCTestCase{
	testClass * tc;
}

@end

@implementation SingletonTests

- (void)setUp
{
    [super setUp];
	tc = new testClass;
	tc->value = 999;
}

- (void)tearDown
{
	delete tc;
    [super tearDown];
}

- (void)testExample
{
	XCTAssertEqual( getSingletonPtr<testClass>()->value, 999 );
}

@end
