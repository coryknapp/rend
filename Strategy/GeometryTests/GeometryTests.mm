//
//  GeometryTests.m
//  GeometryTests
//
//  Created by Cory Knapp on 7/31/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Geometry.h"

@interface GeometryTests : XCTestCase

@end

@implementation GeometryTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testIsPointOnLine
{
	/*bool isPointOnLine(float pai, float paj, float pbi, float pbj,
					   float qi, float qj);*/
	float pai, paj, pbi, pbj;
	float qi, qj;
	
	//	p(line)
	//		a(point)
	pai = 0; paj = 1;
	//		b(point)
	pbi = 2; pbj = 1;
	//	q(point)
	qi = 1; qj = 1;
	XCTAssert( isPointOnLine(pai, paj, pbi, pbj,
							 qi, qj),
			  @"isPointOnLine with horizontal slope through (1,1)");
	
	//	p(line)
	//		a(point)
	pai = 1; paj = 0;
	//		b(point)
	pbi = 1; pbj = 2;
	//	q(point)
	qi = 1; qj = 1;
	XCTAssert( isPointOnLine(pai, paj, pbi, pbj,
							 qi, qj),
			  @"isPointOnLine with vertical slope through (1,1)");
	
	//	p(line)
	//		a(point)
	pai = 1; paj = 0;
	//		b(point)
	pbi = 1; pbj = 2;
	//	q(point)
	qi = 1; qj = 0;
	XCTAssert( isPointOnLine(pai, paj, pbi, pbj,
							 qi, qj),
			  @"point is the same as the line start.");
	
	//	p(line)
	//		a(point)
	pai = 1; paj = 0;
	//		b(point)
	pbi = 1; pbj = 2;
	//	q(point)
	qi = 1; qj = 2;
	XCTAssert( isPointOnLine(pai, paj, pbi, pbj,
							 qi, qj),
			  @"point is same as line end.");
	

}

- (void)testIsPointOnLine3{
	float a[3] = {0,0,0};
	float b[3] = {-3,-3,-3};
	float c[3] = {1,1,1};
	float d[3] = {0,1,2};

	// straight forward success cases
	XCTAssertTrue( isPointOnLine3( a, b, c) );
	XCTAssertTrue( isPointOnLine3( a, c, b) );
	XCTAssertTrue( isPointOnLine3( b, a, c) );
	XCTAssertTrue( isPointOnLine3( b, c, a) );
	XCTAssertTrue( isPointOnLine3( c, a, b) );
	XCTAssertTrue( isPointOnLine3( c, b, a) );

	// where the point tested in the endpoint of the line.
	XCTAssertTrue( isPointOnLine3( a, b, a) );
	XCTAssertTrue( isPointOnLine3( a, b, b) );

	// we fail to pass a valid line
	XCTAssertFalse( isPointOnLine3( a, a, a) );
	XCTAssertFalse( isPointOnLine3( a, a, b) );
	
	// point not on the line
	XCTAssertFalse( isPointOnLine3( a, b, d) );
	XCTAssertFalse( isPointOnLine3( a, d, b) );

}

- (void)testIsPointRightOfLine
{
	//bool isPointRightOfLine(float pai, float paj, float pbi, float pbj,
	//						float qi, float qj);

	// test normal conditions
	XCTAssertTrue( isPointRightOfLine(0, -1, 0 , 1,
									  1, 0 ) );
	XCTAssertFalse( isPointRightOfLine(0, -1, 0 , 1,
									   -1, 0 ) );
	XCTAssertTrue( isPointRightOfLine(0, 1, 0 , -1,
									  -1, 0 ) );
	XCTAssertFalse( isPointRightOfLine(0, 1, 0 , -1,
									   1, 0 ) );
	
	// test bad parameter conditions
	
}

- (void)testLineSegmentCrossLine
{
	//bool lineSegmentCrossesLine(float pai, float paj, float pbi, float pbj,
	//							float qai, float qaj, float qbi, float qbj);
	// test normal conditions
	XCTAssertTrue( lineSegmentCrossesLine(	0, -1, 0 , 1,
											-1, 0, 1, 0 ) );
	XCTAssertFalse( lineSegmentCrossesLine(	0, -1, 0 , 1,
											-1, 0, -2, 0 ) );
}

- (void)testDoSegmentsIntersect
{
	float a1[2];
	float a2[2];
	float b1[2];
	float b2[2];
	
	a1[0] = -1; a1[1] = 0;
	a2[0] = 1 ; a2[1] = 0;
	b1[0] = 0; b1[1] = -1;
	b2[0] = 0; b2[1] = 1;
	XCTAssertTrue( doSegmentsIntersect(a1[0], a1[1], a2[0], a2[1],
									   b1[0], b1[1], b2[0], b2[1] ) );
	
	a1[0] = -3; a1[1] = 0;
	a2[0] = -1 ; a2[1] = 0;
	b1[0] = 0; b1[1] = -1;
	b2[0] = 0; b2[1] = 1;
	XCTAssertFalse( doSegmentsIntersect(a1[0], a1[1], a2[0], a2[1],
									   b1[0], b1[1], b2[0], b2[1] ) );
	
	a1[0] = 1; a1[1] = 0;
	a2[0] = 3 ; a2[1] = 0;
	b1[0] = 0; b1[1] = -1;
	b2[0] = 0; b2[1] = 1;
	XCTAssertFalse( doSegmentsIntersect(a1[0], a1[1], a2[0], a2[1],
									   b1[0], b1[1], b2[0], b2[1] ) );

	//bool doLinesIntersect(float pai, float paj, float pbi, float pbj,
	//					  float qai, float qaj, float qbi, float qbj);
}

- (void)testLinesIntersection{
	float result[2] = {999,999};
	float a1[2];
	float a2[2];
	float b1[2];
	float b2[2];
	
	a1[0] = -1; a1[1] = 0;
	a2[0] = 1 ; a2[1] = 0;
	b1[0] = 0; b1[1] = -1;
	b2[0] = 0; b2[1] = 1;
	
	linesIntersection(result,
					  a1, a2,
					  b1, b2);
	XCTAssertEqual(result[0], 0);
	XCTAssertEqual(result[1], 0);
}

- (void)testLinePlaneIntersection
{
	//bool linePlaneIntersection(float * result,
	//						   float * l1, float * l2,
	//						   float * p1, float * p2, float * p3);
	float result[3];
	float l1[3];
	float l2[3];
	float p1[3];
	float p2[3];
	float p3[3];
	
	// test some fail conditions
	// 1. where l1 and l2 don't form a disinct line
	l1[0]= 0; l1[1]= 0; l1[2]= 0;
	l2[0]= 0; l2[1]= 0; l2[2]= 0;
	XCTAssertFalse(
				   linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				   @"linePlaneIntersection(...) when an invalid line is passed."
				   );
	
	// 2. where the line is parallel to the plane
	l1[0]= 0; l1[1]= 0; l1[2]= 1; // line along the x axis, one unit above
	l2[0]= 1; l2[1]= 0; l2[2]= 1; // the z=0 plane
	p1[0]= 0; p1[1]= 0; p1[2]= 0; //
	p2[0]= 1; p2[1]= 0; p2[2]= 0;
	p3[0]= 0; p3[1]= 1; p3[2]= 0;
	XCTAssertFalse(
				   linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				   @"linePlaneIntersection(...) return a value when there is no\
				    intersection (line and plane are parallel.)"
				   );

	// 3. Where no plane is formed
	p1[0]= 0; p1[1]= 0; p1[2]= 0;
	p2[0]= 1; p2[1]= 0; p2[2]= 0;
	p3[0]= 2; p3[1]= 0; p3[2]= 0;
	XCTAssertFalse(
				   linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				   @"linePlaneIntersection(...) return a value when no plane is\
				    formed"
				   );
	
	// test the z=0 plane
	l1[0]= 0; l1[1]= 0; l1[2]= 0; // straight down into the origin.
	l2[0]= 1; l2[1]= 0; l2[2]= 1;
	p1[0]= 0; p1[1]= 0; p1[2]= 0;
	p2[0]= 1; p2[1]= 0; p2[2]= 0;
	p3[0]= 0; p3[1]= 1; p3[2]= 0;
	XCTAssertTrue(
				   linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				   @"linePlaneIntersection(...) returned false where an \
					intersection should occure."
				   );
	XCTAssertEqualWithAccuracy( 0, result[0], .0001, @"x value wrong");
	XCTAssertEqualWithAccuracy( 0, result[1], .0001, @"y value wrong");
	XCTAssertEqualWithAccuracy( 0, result[2], .0001, @"z value wrong");

	l1[0]= -5; l1[1]= -4; l1[2]= -3; // line along the x axis, one unit above
	l2[0]= 5; l2[1]= 4; l2[2]= 3; // the z=0 plane
	p1[0]= 0; p1[1]= 0; p1[2]= 0; //
	p2[0]= 1; p2[1]= 0; p2[2]= 0;
	p3[0]= 0; p3[1]= 1; p3[2]= 0;
	XCTAssertTrue(
				  linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				  @"linePlaneIntersection(...) returned false where an \
				  intersection should occure."
				  );
	XCTAssertEqualWithAccuracy( 0, result[0], .0001, @"x value wrong");
	XCTAssertEqualWithAccuracy( 0, result[1], .0001, @"y value wrong");
	XCTAssertEqualWithAccuracy( 0, result[2], .0001, @"z value wrong");

	l1[0]= 2; l1[1]= 4; l1[2]= -3; // line along the x axis, one unit above
	l2[0]= 2; l2[1]= 4; l2[2]= 3; // the z=0 plane
	p1[0]= 0; p1[1]= 0; p1[2]= 0; //
	p2[0]= 1; p2[1]= 0; p2[2]= 0;
	p3[0]= 0; p3[1]= 1; p3[2]= 0;
	XCTAssertTrue(
				  linePlaneIntersection( result, l1, l2, p1, p2, p3 ),
				  @"linePlaneIntersection(...) returned false where an \
				  intersection should occure."
				  );
	XCTAssertEqualWithAccuracy( 2, result[0], .0001, @"x value wrong");
	XCTAssertEqualWithAccuracy( 4, result[1], .0001, @"y value wrong");
	XCTAssertEqualWithAccuracy( 0, result[2], .0001, @"z value wrong");

}

-(void)testDistanceFromPointToSegment{
	float a1[2];
	float a2[2];
	float p[2];
	
	/*
	 a1-----o-----a2
			
			p
	 */
	a1[0] = -1; a1[1] = 0;
	a2[0] = 1 ; a2[1] = 0;
	p[0] = 0; p[1] = -1;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
			p
	 
	 a1-----o-----a2
	 */
	p[0] = 0; p[1] = 1;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );

	/*
		  (p/o)
	 
	 a1-----------a2
	 */
	a1[0] = -1; a1[1] = -1;
	a2[0] = 1 ; a2[1] = -1;
	p[0] = 0; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );

	/*
	 a1-----------a2
	 
		  (p/o)
	 */
	a1[0] = -1; a1[1] = 01;
	a2[0] = 1 ; a2[1] = 01;
	p[0] = 0; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	

	/*
	 a2
	 |
	 |
	 o	p
	 |
	 |
	 a1
	 */
	a1[0] = 0; a1[1] = -1;
	a2[0] = 0 ; a2[1] = 1;
	p[0] = 1; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
		 a2
		 |
		 |
	p	 o
		 |
		 |
		 a1
	 */
	a1[0] = 0; a1[1] = -1;
	a2[0] = 0; a2[1] = 01;
	p[0] = -1; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
	 a1
	 |
	 |
	 o	p
	 |
	 |
	 a2
	 */
	// left of a vertical line
	a1[0] = 0; a1[1] = 1;
	a2[0] = 0 ; a2[1] = -1;
	p[0] = 1; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
		 a1
		 |
		 |
	 p	 o
		 |
		 |
		 a2
	 */
	a1[0] = 0; a1[1] = 1;
	a2[0] = 0; a2[1] = -1;
	p[0] = -1; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
	 p	a1-----o-----a2
	 */
	a1[0] = -1; a1[1] = 0;
	a2[0] = 1; a2[1] = 0;
	p[0] = -2; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
	/*
	 a1-----o-----a2	p
	 */
	a1[0] = -1; a1[1] = 0;
	a2[0] = 1; a2[1] = 0;
	p[0] = 2; p[1] = 0;
	XCTAssertEqual(	distanceFromPointToSegment(a1, a2, p), 1 );
	
}

@end
