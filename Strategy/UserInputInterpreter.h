//
//  UserInputInterpreter.h
//  Strategy
//
//  Created by Cory Knapp on 7/13/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__UserInputInterpreter__
#define __Strategy__UserInputInterpreter__

#include <iostream>

class UserInputInterpreter {
    
    static const unsigned _mouseButtonCount = 3;
    static const unsigned _modButtonCount = 5;
    // keyboard input is handled by polling
    bool _keyDownMap[std::numeric_limits<char>::max()];
    bool _modDownMap[_modButtonCount];
    
public:
    
    UserInputInterpreter(){
        
    }
    
    typedef char keyCode;
    
    // following are called by the system.
    virtual void mouseUp( unsigned x, unsigned y, int button ) = 0;
    virtual void mouseDown( unsigned x, unsigned y, int button ) = 0;
    
    virtual void mouseMove( unsigned x, unsigned y, int button) = 0;
    
    virtual void setKeyDown( keyCode );
    virtual void setKeyUp( keyCode );
    virtual void setModifierKeysDown( int keyNum );
    virtual void setModifierKeysUp( int keyNum );

    
    void windowResize();
    
    // these are for querying
    virtual bool keyDown( keyCode );
    virtual bool modifierKeyDown( int keyNum );
    
    std::pair<unsigned, unsigned> mouseLocation();
};

#endif /* defined(__Strategy__UserInputInterpreter__) */
