//
//  AppDelegate.h
//  Strategy
//
//  Created by Cory Knapp on 8/31/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "RendGLView.h"

#import "MainRenderer.h"
#import "GreaterContext.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    NSPoint _lastMouseLocation;

    GreaterContext * greaterContext;
}
@property (assign) IBOutlet NSWindow *window;

-(void)mouseMoved:(NSEvent *)theEvent;

- (void)keyDown:(NSEvent *)theEvent;
- (void)keyUp:(NSEvent *)theEvent;

@end
