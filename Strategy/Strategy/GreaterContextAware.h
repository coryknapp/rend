//
//  GreaterContexAware.h
//  Strategy
//
//  Created by Cory Knapp on 7/16/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef Strategy_GreaterContexAware_h
#define Strategy_GreaterContexAware_h

class GreaterContext;

class GreaterContextAware {
    
    GreaterContext * _context = nullptr;
    
public:
    
    void setGreaterContext( GreaterContext * gc );
    GreaterContext * greaterContext();
    
};


#endif
