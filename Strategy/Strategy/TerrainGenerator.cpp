//
//  TerrainGenerator.cpp
//  Strategy
//
//  Created by Cory Knapp on 3/27/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "TerrainGenerator.h"

TerrainGenerator::TerrainGenerator(){
    
    // set up the module
    _perlinMod.SetOctaveCount( 8 );
    _perlinMod.SetFrequency( 1 );
    _perlinMod.SetSeed( 1000 ); //lol
    
    _generateHeightMapsForRange( -10,10,-10,10);
    
    // TODO inprove
}

void TerrainGenerator::_generateHeightMapsForRange( int i0, int i1, int j0, int j1){
    
    
    std::cout << "????????????????" << "\n"
        << "heightMapForRange( "<< i0 << ", "<< i1 << ", "
        << j0 << ", " << j1 << std::endl;
    
    _heightMapBuilder.SetSourceModule (_perlinMod);
    _heightMapBuilder.SetDestNoiseMap (_heightMap);
    _heightMapBuilder.SetDestSize ( i1-i0, j1-j0 );
    
    //_heightMapBuilder.SetBounds (i0, i1, j0, j1);
    _heightMapBuilder.SetBounds (i0, i1+1, j0, j1+1);
    _heightMapBuilder.Build ();
    
    for( int i = 0; i< i1-i0; i++ )
        for( int j = 0; j< j1-j0; j++ ){
            float zValue = _heightMap.GetValue( i, j );
            // zValue ranges from [-1,1] needs to be converted to [0,sizeof uint8]
            uint8_t converted8Bit = ( zValue + 1 ) * ( std::numeric_limits<uint8_t>::max()/2 );
            std::cout << i << "," << j << "===" << (int)converted8Bit << std::endl;
            _map.setValue( i+i0, j+j0 , converted8Bit);
        }
}

std::unique_ptr< PlanarData<uint8_t> >
TerrainGenerator::heightMapForRange( int i0, int i1, int j0, int j1){
    
    std::unique_ptr< PlanarData<uint8_t> > retValue( new PlanarData<uint8_t>( i1-i0+1, j1-j0+1, 1 ));
    uint8_t v;
    for( int i = 0; i < retValue->width(); i++ )
        for( int j = 0; j < retValue->height(); j++ ){
            if( !_map.getValue( i+i0, j+j0, v ) )
                assert( false );
            retValue->setValue( i, j, 0, v );
        }
    
    return retValue;
}

const ExpandablePlanar<uint8_t> & TerrainGenerator::masterMap() const{
    return _map;
}