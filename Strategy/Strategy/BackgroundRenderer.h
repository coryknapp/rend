//
//  BackgroundRenderer.h
//  Strategy
//
//  Created by Cory Knapp on 10/24/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__BackgroundRenderer__
#define __Strategy__BackgroundRenderer__

#include <iostream>
#include "TextureBlendingRenderer.h"
#include "GeneratedGLSLProgram.h"
#include "Texture.h"
#include "FrameBufferObjectCollection.h"

#include "GreaterContext.h"

class BackgroundRendererProgram : public GeneratedGLSLProgram{

    GLint _textureName;

public:
    void build();
    void initBindings();
    void setTexture( GLint textureName );
};

class BackgroundRenderer : public Renderer, public GreaterContextAware{

    Texture _tex;
    BackgroundRendererProgram _program;

    VAO _fullscreenQuadVAO;

    void _renderFullFrameQuad();

public:

    BackgroundRenderer( GLuint fbo );

    virtual void resize(GLuint width, GLuint height);
    void render();
    void destroyLayerTextures();

    virtual void initialize();

};


const char * const backgroundProgramVertSourceTop = "\
#ifdef GL_ES                            \n\
precision highp float;                  \n\
#endif                                  \n";

// attributes will be inserted here

const char * const backgroundProgramVertSourceBottom =
"out vec2 texcoord;"
"void main (void)"
"{"
"gl_Position	= POSITION_ATTRIBUTE_INDEX_0;"
"texcoord = TEXTURECOORD_ATTRIBUTE_INDEX_0;"
"}";

const char * const backgroundProgramFragSource = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif                      \n"
"in vec2 texcoord;"
"uniform sampler2D backTex;"
"out vec4 fragColor;"
"void main (void)"
"{"
"vec4 sampleColor = texture(backTex, texcoord);"
"fragColor = sampleColor;"
"}";


#endif /* defined(__Strategy__BackgroundRenderer__) */
