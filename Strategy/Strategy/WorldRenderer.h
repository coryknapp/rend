//
//  WorldRenderer.h
//  Strategy
//
//  Created by Cory Knapp on 9/1/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__WorldRenderer__
#define __Strategy__WorldRenderer__

#define DEBUG_WorldRenderer_h

#include "ThreeDRenderer.h"
#include "GeneratedGLSLProgram.h"
#include "AssimpVAO.h"

#include "WorldCamera.h"
#include "WorldRenderProgram.h"

#include "NormalMapRenderer.h"
#include "ImageFilePlanarData.h"

#include "TerrainRenderProgram.h"
#include "TerrainRectVAO.h"

#include "SimState.h"
#include "SimPhysics.h"

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
//debugging
#include "ZDiffDebugProgram.h"

#include "GreaterContext.h"
#include "GameObjectComponents.h"

class WorldRenderer : public ThreeDRenderer, public GreaterContextAware {

    AssimpVAO _cellVAO;

    WorldRendererProgram _program;
    TerrainRenderProgram _terrainProgram;

    ZDiffDebugProgram _ZDiffProgram;
    
    Texture _textureMap;
    Texture _redTex;
    Texture _greenTex;
    Texture _blueTex;

    Texture _normalMapTex;
        
    StageState * _currentStage;
    
public:
    WorldRenderer( GLint fbo ) :
        ThreeDRenderer( fbo )
    {
        
    };
    using ThreeDRenderer::resize;

    void initialize();
    void render();

    //void buildHexVAOInRange( size_t minI, size_t maxI, size_t minJ, size_t maxJ );
    void buildTerrainVAOsInRange( int minI, int maxI, int minJ, int maxJ );
};

#endif /* defined(__Strategy__WorldRenderer__) */
