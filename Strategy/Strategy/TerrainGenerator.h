//
//  TerrainGenerator.h
//
//  Created by Cory Knapp on 3/27/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __TerrainGenerator__
#define __TerrainGenerator__

#include <iostream>

#include <boost/utility.hpp>
#include "noiseutils.h"

#include "PlanarData.h"
#include "ExpandablePlanar.h"

template class ExpandablePlanar<uint8_t>;

class TerrainGenerator : boost::noncopyable {
    
    noise::module::Perlin _perlinMod;
    
    noise::utils::NoiseMap _heightMap;
    noise::utils::NoiseMapBuilderPlane _heightMapBuilder;
        
    ExpandablePlanar<uint8_t> _map;
    
    void _generateHeightMapsForRange( int i0, int i1, int j0, int j1);
    
public:
    
    TerrainGenerator();
    
    // returns height map for range, inclusive
    std::unique_ptr< PlanarData<uint8_t> > heightMapForRange( int i0, int i1, int j0, int j1);
    
    const ExpandablePlanar<uint8_t> & masterMap() const;
};

#endif /* defined(__Strategy__TerrainGenerator__) */
