//
//  WorldCamera.h
//  Strategy
//
//  Created by Cory Knapp on 9/20/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__WorldCamera__
#define __Strategy__WorldCamera__

#include <iostream>

#include "FlyCamera.h"
#include "FlyCameraInputInterpreter.h"
#include "GreaterContext.h"

class WorldCamera: public FlyCamera, public GreaterContextAware {

public:
    virtual void tick( const SimTime &d );

};


#endif /* defined(__Strategy__WorldCamera__) */
