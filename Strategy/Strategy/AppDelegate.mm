//
//  AppDelegate.m
//  Strategy
//
//  Created by Cory Knapp on 8/31/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)awakeFromNib{
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    new Resources( (char *)[resourcePath cStringUsingEncoding:NSASCIIStringEncoding] );
}

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[(RendGLView *)[_window contentView] openGLContext] makeCurrentContext];
    MainRenderer * mainRend = new MainRenderer(0);

    mainRend -> resize( [[_window contentView] frame].size.width , [[_window contentView] frame].size.height );
    mainRend -> initialize();
    //mainRend -> resize( [[_window contentView] frame].size.width , [[_window contentView] frame].size.height );
    mainRend -> declareReadyToRender();

    greaterContext = new GreaterContext(getSimTime());
    mainRend -> setGreaterContext( greaterContext );

    [(RendGLView *)[_window contentView] setRenderer: mainRend];

    _lastMouseLocation = [NSEvent mouseLocation];
}

-(void)mouseDown:(NSEvent *)theEvent{
    NSPoint m = [NSEvent mouseLocation];
    greaterContext->inputInterpreter() -> mouseDown( m.x, m.y, 1);
}

-(void)mouseUp:(NSEvent *)theEvent{
    NSPoint m = [NSEvent mouseLocation];
    greaterContext->inputInterpreter() -> mouseUp( m.x, m.y, 1);
}

-(void)mouseMoved:(NSEvent *)theEvent{

    NSPoint currentMouseLocation = [[_window contentView] convertPoint: [theEvent locationInWindow] fromView: nil];
    greaterContext->inputInterpreter() -> mouseMove( currentMouseLocation.x, currentMouseLocation.y, 1);
}


- (void) flagsChanged:(NSEvent *)event {
    
    if (([event modifierFlags] & NSShiftKeyMask) == NSShiftKeyMask)
        greaterContext->inputInterpreter()->setModifierKeysDown( 0 );
    else
        greaterContext->inputInterpreter()->setModifierKeysUp( 0 );
    
    if (([event modifierFlags] & NSFunctionKeyMask) == NSFunctionKeyMask)
        greaterContext->inputInterpreter()->setModifierKeysDown( 1 );
    else
        greaterContext->inputInterpreter()->setModifierKeysUp( 1 );
    
    
    if (([event modifierFlags] & NSControlKeyMask) == NSControlKeyMask)
        greaterContext->inputInterpreter()->setModifierKeysDown( 2 );
    else
        greaterContext->inputInterpreter()->setModifierKeysUp( 2 );
    
    if (([event modifierFlags] & NSAlternateKeyMask) == NSAlternateKeyMask)
        greaterContext->inputInterpreter()->setModifierKeysDown( 3 );
    else
        greaterContext->inputInterpreter()->setModifierKeysUp( 3 );
    
    if (([event modifierFlags] & NSCommandKeyMask) == NSCommandKeyMask)
        greaterContext->inputInterpreter()->setModifierKeysDown( 3 );
    else
        greaterContext->inputInterpreter()->setModifierKeysUp( 3 );
}

- (void)keyDown:(NSEvent *)theEvent {
    //char code = [theEvent keyCode];
    char code = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
    greaterContext->inputInterpreter()->setKeyDown( code );
}

- (void)keyUp:(NSEvent *)theEvent {
    //char code = [theEvent keyCode];
    char code = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
    greaterContext->inputInterpreter()->setKeyUp( code );
}

@end
