//
//  PlainsRegionGenerator.h
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__PlainsRegionGenerator__
#define __Strategy__PlainsRegionGenerator__

#include <iostream>

#include "RegionGeneratorAbstract.h"

#include "PossionDisk.h"
#include "StageState.h"
#include "Geometry.h"

#include "ContainerAssist.h"

#include <set>

class PlainsRegionGenerator : public RegionGeneratorAbstract {

	typedef
		PossonDiscGenerator<
		Waypoint,
		WaypointSetFirstCoordFunctor,
		WaypointSetSecondCoordFunctor,
		WaypointGetFirstCoordFunctor,
		WaypointGetSecondCoordFunctor,
		WaypointNetwork,
		WaypointNetworkAddFunctor
	> generatorType;
	
	std::unique_ptr< generatorType > _generator;
	WaypointNetwork * _network;
	
public:
	PlainsRegionGenerator();
	void setNetwork( WaypointNetwork * network );
	bool iterate();

	void connectNodesInternally();
};

#endif /* defined(__Strategy__PlainsRegionGenerator__) */
