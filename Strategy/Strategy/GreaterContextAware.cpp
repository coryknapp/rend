//
//  GreaterContextAware.cpp
//  Strategy
//
//  Created by Cory Knapp on 7/18/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "GreaterContextAware.h"

void GreaterContextAware::setGreaterContext( GreaterContext * gc ){
    _context = gc;
}

GreaterContext * GreaterContextAware::greaterContext(){
    return _context;
}