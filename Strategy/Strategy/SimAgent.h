//
//  SimAgent.h
//  Strategy
//
//  Created by Cory Knapp on 9/2/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__SimAgent__
#define __Strategy__SimAgent__

#define DEBUG_SimAgent_h

#include <iostream>
#include <cassert>
#include "SimTypesAndConstants.h"
#include "SimMovementBehavior.h"

class SimAgent {

    float _lastlocationCache[4] = {0,0,0,1};

    SimMovementBehavior * movementBehavior = nullptr;

public:

    SimAgent();

    // constructors and assignments
    SimAgent( const SimAgent &other );              // copy constructor
    SimAgent& operator = (const SimAgent & other);  // copy assignment
    SimAgent( SimAgent &&other );                   // move constructor
    SimAgent& operator = (SimAgent&& other);        // move assignment
    
    void location(float * location);

    void setMovementBehavior( SimMovementBehavior * mb );

    virtual void step( const SimTime &currentTime );
    
    virtual void movementBehaviorCompleted( const SimTime &currentTime );
};

#endif /* defined(__Strategy__SimAgent__) */
