//
//  GameObject.h
//
//  Created by Cory Knapp on 6/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __GameObject__
#define __GameObject__

#include <iostream>
#include <unordered_map>
#include <cassert>

class GameObjectManager;

typedef uint64_t GameObjectID;

class GameObject {
    
    GameObjectID _id;
    std::unordered_map<const std::type_info* , void *> _components;
    
	friend GameObjectManager;
	
public:
    
	GameObjectID id() const{
		return _id;
	}
	
    template< typename T >
    void install(){
        _components[&typeid(T)] = new T;
    }
    
    template< typename T >
    void remove(){
        delete static_cast<T*>(_components[&typeid(T)]);
        _components[&typeid(T)] = nullptr;
    }
    
    template< typename T >
    bool checkComponent(){
        return (_components.find(&typeid(T)) != _components.end());
    }
    
    template< typename T >
    T& get(){
        if(_components.count(&typeid(T)) != 0){
            return *static_cast<T*>(_components[&typeid(T)]);
        }
        else{
            assert( false );
        }
    }
    
    
};
#endif /* defined(__Strategy__GameObject__) */
