//
//  SimTypesAndConstants.h
//  Strategy
//
//  Created by Cory Knapp on 9/11/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef Strategy_SimConstants_h
#define Strategy_SimConstants_h

#define DEBUG_SimTypesAndConstants_h
#include <cassert>

//////////////////////////////////////////////////////////
// time keeping
//////////////////////////////////////////////////////////

#include <sys/time.h>

typedef long SimTime;
#define SimTimeSecond (1000000)

static inline SimTime getSimTime(){
    timeval t;
    gettimeofday( &t, nullptr );
    return ( t.tv_sec * 1000000 ) + t.tv_usec;
};

static inline float getSimTimeSecond( const SimTime& time ){
    return time / (float)SimTimeSecond;
}



//////////////////////////////////////////////////////////
// random numbers
//////////////////////////////////////////////////////////

#include <cstdlib>
#include <math.h>

static inline float rand0to1(){
    return std::rand()/(float)RAND_MAX;
}

static inline float randInRange( const float &a, const float &b){
#ifdef DEBUG_SimTypesAndConstants_h
    assert( b > a );
#endif
    return ( rand0to1() * (b - a) ) + a;
}

template <typename T>
static inline void randomNormalArrayImpl( size_t size, T * vec ){
    float lengthSquared = 0;
    float length;
    int n;
    for( n = 0; n < size; ++n ){
        vec[n] = rand0to1();
        lengthSquared += vec[n] * vec[n];
    }
    length = sqrtf( lengthSquared );
    for( n = 0; n < size; ++n ){
        vec[n] /= lengthSquared;
    }
}

static inline void randomNormalArray(  size_t size, float * vec  ) {    randomNormalArrayImpl<float>( size, vec );     }
static inline void randomNormalArray(  size_t size, double * vec ) {    randomNormalArrayImpl<double>( size, vec );    }


#endif
