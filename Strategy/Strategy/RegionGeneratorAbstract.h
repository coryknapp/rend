//
//  RegionGeneratorAbstract.h
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__RegionGeneratorAbstract__
#define __Strategy__RegionGeneratorAbstract__

#include <iostream>
#include "Waypoint.h"
#include "RegionBoundaryGenerator.h"

class RegionBoundaryGenerator;

class RegionGeneratorAbstract {

protected:
	WaypointNetwork * _network = nullptr;
	RegionBoundaryGenerator * _boundaryGen = nullptr;
	RegionID _rId;
public:
	RegionGeneratorAbstract();
	virtual void setNetwork( WaypointNetwork * network );
	virtual void setGenerator( RegionBoundaryGenerator * boundaryGen );
	virtual void setRegionID( const RegionID &rId );
	virtual bool iterate() = 0;

	RegionID rId();
	virtual bool inBounds( float i, float j );
	virtual std::pair<float, float> center();
};

#endif /* defined(__Strategy__RegionGeneratorAbstract__) */
