//
//  TessellatedSquare.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/23/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "TessellatedSquareVAO.h"

void TessellatedSquareVAO::build( unsigned xDiv, unsigned yDiv, float xMin, float yMin, float xMax, float yMax){

    const float lowerBound = xMin;
    const float upperBound = xMax;

    const size_t bufferSize = 6 * 4;
    GLfloat * data = (GLfloat *)std::malloc( sizeof(GLfloat)*( 29 ) );

    data[0] = lowerBound;     data[1] = lowerBound;     data[2] = -1;  data[3] = 1;        // bottom left
    data[4] = 0.0;     data[5] = 0.0; data[6] = 1.0;

    data[7] = lowerBound;     data[8] = upperBound;     data[9] = -1; data[10] = 1;        // top left
    data[11] = 0.0;     data[12] = 0.0; data[13] = 1.0;

    data[14] = upperBound;     data[15] = upperBound;     data[16] = -1;   data[17] = 1;     // top right
    data[18] = 0.0;     data[19] = 0.0; data[20] = 1.0;

    data[21] = upperBound;     data[22] = lowerBound;     data[23] = -1;   data[24] = 1;     // bottom right
    data[25] = 0.0;     data[26] = 0.0; data[17] = 1.0;

    Buffer<GLfloat> quadBuffer( data, bufferSize, false );

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 1;

    GLuint vaoName = buildVAO( quadBuffer, menu, 4);
    VAO fullscreenQuadVAO;
    set( vaoName, 4, GL_TRIANGLE_FAN );
    return;

/*
#define VERT_AT_( i, j ) (i*(yDiv+1)+j)

    float xSliceSize = (xMax - xMin) / ( xDiv );
    float ySliceSize = (yMax - yMin) / ( yDiv );

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX]    = 1;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;

#ifdef DEBUG_TessellatedSquareVAO_h
    assert( strideForMenu(menu) == 6);
#endif

    Buffer<GLfloat> vaoBuffer( (xDiv+1)*(yDiv+1)*6 );

    GLfloat * verts = vaoBuffer.buffer();
    GLint indexes[yDiv+1][xDiv+1];
    int index = 0;
    for( unsigned i = 0; i <= xDiv; i++ ){
        for( unsigned j = 0; j <= yDiv; j++ ){
            const size_t pos = strideForMenu(menu) * index;
            // positions
            verts[VERT_AT_(i,j)] = xSliceSize * i;
            verts[VERT_AT_(i,j)+1] = ySliceSize * j;
            verts[VERT_AT_(i,j)+2] = 0;
            verts[VERT_AT_(i,j)+3] = 1;
            /// textures
            verts[pos+4] = xSliceSize * i;
            verts[pos+5] = ySliceSize * j;
            
            indexes[j][i] = index;
            index++;
        }
    }

    const size_t elementArraySize = (xDiv+1)*(yDiv+1) * 2 + 2 * (yDiv);
    GLint elementArray[ elementArraySize  ];
    index = 0;
    unsigned i;
    unsigned j;
    for( j = 0; j <= yDiv; j++ ){
        for( i = 0; i <= xDiv; i++ ){
#ifdef DEBUG_TessellatedSquareVAO_h
        assert( index < elementArraySize );
#endif
            elementArray[index] = indexes[i][j];
            index++;
            elementArray[index] = indexes[i][j+1];
            index++;
        }

        if( j != yDiv ){
            // close the the row at the end by adding the last element of this last row again
            // then add the first element of the next row.
            elementArray[index] = indexes[xDiv][j+1];
            index++;
            elementArray[index] = indexes[0][j+1];
            index++;
        } else {

        }
    }

    GLuint vaoName = buildVAO( vaoBuffer, menu, (xDiv+1)*(yDiv+1), elementArray, (GLint)elementArraySize );
    set( vaoName, (GLint)elementArraySize, GL_TRIANGLE_STRIP);

    GetGLError();


#ifdef DEBUG_TessellatedSquareVAO_h
/**/    // test to make sure our elementArray was exactly the right size.
    /**/    //assert( index == elementArraySize );
/**/
/**/    // Test to make sure all points are in the parametric bounds. (maybe sort of useless)
    /**/   // for( unsigned i = 0; i <= xDiv; i++ ){
    /**/  //      for( unsigned j = 0; j <= yDiv; j++ ){
    /**/        //    assert( verts[i][j][0] >= xMin );
    /**/         //   assert( verts[i][j][0] <= xMax );
    /**/         //   assert( verts[i][j][1] >= yMin );
    /**/        //    assert( verts[i][j][1] <= yMax );
    /**/    //    }
/**/  //  }
/**/
/**/    // test the corners to make sure they're positioned where they are supposed to be.
    /**/   // assert( verts[0][0][0] == xMin );
    /**/    //assert( verts[0][0][0] == yMin );
    /**/    //assert( verts[xDiv][0][0] == xMax );
    /**/    //assert( verts[xDiv][0][1] == yMin );
    /**/    //assert( verts[0][yDiv][0] == xMin );
    /**/    //assert( verts[0][yDiv][1] == yMax );
    /**/    //assert( verts[xDiv][yDiv][0] == xMax );
    /**/    //assert( verts[xDiv][yDiv][1] == yMax );
/**/
/**/    // make sure all elements of the element array really refer to real vertices
    /**/    //size_t counter;
    /**/    //for( counter = 0; counter < elementArraySize; counter++){
    /**/       // assert( elementArray[counter] < (yDiv+1)*(xDiv+1 ) );
    /**/    //}
/**/


    //#endif

}