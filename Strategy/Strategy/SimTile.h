//
//  SimTile.h
//  Strategy
//
//  Created by Cory Knapp on 12/11/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__SimTile__
#define __Strategy__SimTile__

#include <iostream>



class SimTile {

public:

    // scores
    int _mobility;
    int _potableWater;
    int _lowCalorieVegetation;
    int _highCalorieVegetation;

};

#endif /* defined(__Strategy__SimTile__) */
