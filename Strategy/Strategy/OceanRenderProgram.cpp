//
//  OceanRenderProgram.cpp
//  Strategy
//
//  Created by Cory Knapp on 2/9/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "OceanRenderProgram.h"

void OceanRenderProgram::build(){

    setVertexShaderHead( oceanRendererProgramVertSourceTop );
    setVertexShaderBody( oceanRendererProgramVertSourceBottom );
    setFragmentShaderBody( oceanRendererProgramFragSource );
    includeUniform( "mat4", "projectionMatrix");
    includeUniform( "mat4", "viewTransformMatrix");
    includeUniform( "mat4", "modelTransformMatrix");
    includeUniform( "vec4", "globalTintColor");

    includeUniform( "vec2", "textureTransform" );
    includeUniform( "float", "textureScaleTransform" );

    includeUniform( "vec4", "lightPosition");

    includeUniform( "sampler2D", "noiseTex");
    includeUniform( "sampler2D", "noiseNormalTex");

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 0;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;

    GeneratedGLSLProgram::build( menu );

}

void OceanRenderProgram::initBindings(){
    getUniform( _projectionMatrixUniform, "projectionMatrix" );
    getUniform( _viewTransformMatrixUniform, "viewTransformMatrix" );
    getUniform( _modelTransformMatrixUniform, "modelTransformMatrix" );

    getUniform( _textureTransformUniform, "textureTransform");
    getUniform( _textureScaleTransform, "textureScaleTransform");

    getUniform( _lightPositionUniform, "lightPosition");

    getUniform( _noiseTex, "noiseTex" );
    getUniform( _noiseNormalTex, "noiseNormalTex");

}

void OceanRenderProgram::setProjectionMatrix( const GLfloat * m ){
    glUniformMatrix4fv( _projectionMatrixUniform, 1, false, m);

}
void OceanRenderProgram::setViewTransformMatrix( const GLfloat * m ){
    glUniformMatrix4fv( _viewTransformMatrixUniform, 1, false, m); //xxx

}
void OceanRenderProgram::setModelTransformMatrix( const GLfloat * m ){
    glUniformMatrix4fv( _modelTransformMatrixUniform, 1, false, m);

}
void OceanRenderProgram::setTextureTransform( GLfloat s, GLfloat t ){
    glUniform2f( _textureTransformUniform, s, t);
}

void OceanRenderProgram::setTextureScaleTransform( GLfloat s ){
    glUniform1f( _textureScaleTransform, s);
}

void OceanRenderProgram::setLightPositionUniform( GLfloat * v ){
    glUniform4fv( _lightPositionUniform, 1, v);
}

void OceanRenderProgram::setNoiseTexture( GLint textureName ){
    glUniform1i( _noiseTex, textureName);
}

void OceanRenderProgram::setNNoiseNormalTexture( GLint textureName ){
    glUniform1i( _noiseNormalTex, textureName);
}