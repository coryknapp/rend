//
//  HexGrid.h
//  Strategy
//
//  Created by Cory Knapp on 9/16/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__HexGrid__
#define __Strategy__HexGrid__

#include <iostream>
#include <vector>
#include <cassert>
#define _USE_MATH_DEFINES
#include <math.h>

#define DEBUG_HexGrid_h

#define HEX_INSCRIBED_RADIUS (1.0)
#define HEX_X_OFF (HEX_INSCRIBED_RADIUS*cosf(M_PI/3.0))
#define HEX_Y_OFF (HEX_INSCRIBED_RADIUS*sinf(M_PI/3.0))

template <typename T>
class HexGrid {
    size_t _width;
    size_t _height;

    T * _objects;

public:

    HexGrid(const size_t &width, const size_t &height) :
        _width(width),
        _height(height){
        _objects = (T*)std::malloc( sizeof(T) * _width * _height );
    }

    ~HexGrid(){
        std::free( _objects );
    }

    size_t width() const { return _width; };
    size_t height() const { return _height; };

    // storage access

    T& getObject( const size_t &i, const size_t &j) const{
        assert( i < _width );
        assert( j < _height );
        return _objects[ j * _width + i ];
    }

    void setObject( const size_t &i, const size_t &j, T &&object){
        _objects[ j * _width + i ] = std::move( object );
    }

    void setObject( const size_t &i, const size_t &j, const T &object){
        _objects[ j * _width + i ] = object;
    }

    // geometry
    
    float inscribedCircleRadius() const{ return HEX_INSCRIBED_RADIUS; }
    
    void getCenter( const size_t &i, const size_t &j, float * twoFloats ) const{

        twoFloats[0] = i * HEX_X_OFF * 3;
        twoFloats[1] = (i % 2 == 0)?
            j * HEX_Y_OFF * 2 :
            j * HEX_Y_OFF * 2 + HEX_Y_OFF ;

        //twoFloats[0] += .0001;
        //twoFloats[1] += .0001;
    }

    void getVertex( const size_t &i, const size_t &j, float * twelveFloats ) const{

        float center[2];
        getCenter(i, j, center);

        /*
           67        45

        89      *      23
         
           10,11     01
         */
        twelveFloats[0] = center[0] +  HEX_X_OFF;
        twelveFloats[2] = center[0] + (HEX_X_OFF) * 2;
        twelveFloats[4] = center[0] +  HEX_X_OFF;
        twelveFloats[6] = center[0] -  HEX_X_OFF;
        twelveFloats[8] = center[0] - (HEX_X_OFF) * 2;
        twelveFloats[10]= center[0] -  HEX_X_OFF;

        twelveFloats[1] = center[1] - HEX_Y_OFF;
        twelveFloats[3] = center[1];
        twelveFloats[5] = center[1] + HEX_Y_OFF;
        twelveFloats[7] = center[1] + HEX_Y_OFF;
        twelveFloats[9] = center[1];
        twelveFloats[11]= center[1] - HEX_Y_OFF;
    }

    void getClosestHexToPoint( const size_t &i, const size_t &j, size_t * hexCoords ){

    }

};

#endif /* defined(__Strategy__HexGrid__) */
