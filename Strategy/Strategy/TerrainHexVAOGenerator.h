//
//  TerrainHexVAOGenerator.h
//  Strategy
//
//  Created by Cory Knapp on 12/12/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TerrainHexVAOGenerator__
#define __Strategy__TerrainHexVAOGenerator__

#include <iostream>
#include "VAO.h"
#include "PlanarData.h"
#include "HexGrid.h"

#define DEBUG_TerrainHexVAOGenerator_h

/*const VAO& VAOforHex(
                     float centerX, float centerY, // in world coords
                     float mapScale, // small number (1,0).  1 world unit is how much of the map.
                     PlanarData<float> * heightMap,
                     PlanarData<float> * normalMap,
                     PlanarData<float> * textureTypeMap
                     ){
    
    // xxx we should copy the VAO into a basic VAO object to get rid of the overhead
    // assosiated with building the VAO.

}*/

// use the factory method rather then the class directly
// to shed the over head of constructing the VAO.
class TerrainHexVAO : public VAO {

    int _vertexArrayWriteHead = 0;
    float * _vertexArray;
    size_t _vertexStride = 0;

    size_t _i;
    size_t _j;
    float _scale;
    PlanarData<uint8_t> * _heightMap;
    PlanarData<uint8_t> * _normalMap;
    PlanarData<uint8_t> * _textureTypeMap;

    void _addSubdividedTriangle( const float *v1,
                                const float *v2,
                                const float *v3,
                                const int &depth );
    void _fillArrayWithVertex( const float *vertex );

public:

    TerrainHexVAO(size_t i, size_t j,
                  float mapScale,
                  PlanarData<uint8_t> * heightMap,
                  PlanarData<uint8_t> * normalMap,
                  PlanarData<uint8_t> * textureTypeMap
                  ) :   _scale( mapScale ),
                        _i(i),
                        _j(j),
                        _heightMap( heightMap ),
                        _normalMap( normalMap ),
                        _textureTypeMap( textureTypeMap ){

                            build( 2 ); // magic defult depth

    }

    virtual void build( int depth );

#ifdef DEBUG_TessellatedHexVAO_h
    int finalTriangleCount = 0;
#endif

};

#endif /* defined(__Strategy__TerrainHexVAOGenerator__) */
