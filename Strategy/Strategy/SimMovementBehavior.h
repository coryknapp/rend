//
//  SimMovementBehavior.h
//  Strategy
//
//  Created by Cory Knapp on 9/10/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__SimMovementBehavior__
#define __Strategy__SimMovementBehavior__

#include <iostream>

#include "SimTypesAndConstants.h"

class SimMovementBehavior {

public:
    virtual void positionAtTime(    float * position,
                                SimTime time ) = 0;
    virtual bool moveCompleted() = 0;

    virtual ~SimMovementBehavior(){}

};

class SimStubMovementBehavior : public SimMovementBehavior {

public:
    SimStubMovementBehavior(){
        
    }
    void positionAtTime( float * position, SimTime time ){
        position[0] = position[1] = position[2] = 0.0;
    }
    bool moveCompleted(){
        return true;
    }

};

// should this be templated to work in n-dim?
class SimJumpMovementBehavior : public SimMovementBehavior {

    SimTime _startTime;
    float _jumpStart[3];
    SimTime _endTime;
    float _jumpEnd[3];

    bool _moveCompleted = false;

public:

    SimJumpMovementBehavior(    const SimTime startTime,
                                const float  jumpStart[3],
                                const SimTime endTime,
                                const float  jumpEnd[3]    ):
    SimMovementBehavior(),
    _startTime( startTime ),
    _endTime( endTime )
    {
        for( int n = 0; n < 3; n++ ){
            _jumpStart[n] = jumpStart[n];
            _jumpEnd[n] = jumpEnd[n];
        }
    }


    void positionAtTime(    float * position,  // pass an array of length 3 (at least)
                            SimTime time ){
        // fills the memory at position with the location of the agent at time.
        // Returns a bool indicating id it's finished it's "jump"
        if( time > _endTime ){
            std::memcmp( position, _jumpEnd, sizeof( float ) * 3 );
            _moveCompleted = true;
            return;
        }
        if( time <= _startTime ){
            std::memcmp( position, _jumpStart, sizeof( float ) * 3 );
            return;
        }
        
        float s = (float)(time - _startTime)/(float)(_endTime - _startTime);
        for( int n = 0; n < 3; n++ ){
            position[n] = (_jumpEnd[n] * s) + (_jumpStart[n] * ( 1 - s ) );
        }
        return;
    }

    bool moveCompleted(){
        return _moveCompleted;
    }
};

#endif /* defined(__Strategy__SimMovementBehavior__) */
