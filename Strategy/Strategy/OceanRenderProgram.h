//
//  OceanRenderProgram.h
//  Strategy
//
//  Created by Cory Knapp on 2/9/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__OceanRenderProgram__
#define __Strategy__OceanRenderProgram__


#include <iostream>

#include "GLSLProgram.h"
#include "GeneratedGLSLProgram.h" // for attributeCodeForMenu(...)

class OceanRenderProgram : public GeneratedGLSLProgram {

    // uniform locations

    GLint _projectionMatrixUniform;
    GLint _viewTransformMatrixUniform;
    GLint _modelTransformMatrixUniform;

    GLint _textureTransformUniform;
    GLint _textureScaleTransform;

    GLint _lightPositionUniform;

    GLint _noiseTex;
    GLint _noiseNormalTex;

public:

    void build();

    void initBindings();

    void setProjectionMatrix( const GLfloat * m );
    void setViewTransformMatrix( const GLfloat * m );
    void setModelTransformMatrix( const GLfloat * m );
    void setTextureTransform( GLfloat s, GLfloat t);
    void setTextureScaleTransform( GLfloat s );
    void setLightPositionUniform( GLfloat * v );

    void setNoiseTexture( GLint textureName );
    void setNNoiseNormalTexture( GLint textureName );
};

const char * const oceanRendererProgramVertSourceTop = "    \
#ifdef GL_ES                            \n\
precision highp float;                  \n\
#endif                                  \n\
\
out vec2 scaledTextureCords;"
"out vec2 textureCords;"
"out vec4 lightDir;";

// attributes will be inserted here

const char * const oceanRendererProgramVertSourceBottom =
"void main (void)"
"{"
"scaledTextureCords = (TEXTURECOORD_ATTRIBUTE_INDEX_0 + textureTransform) * textureScaleTransform;"
"textureCords = TEXTURECOORD_ATTRIBUTE_INDEX_0;"
"lightDir = normalize( lightPosition );"
"vec4 adjustedPos = POSITION_ATTRIBUTE_INDEX_0;"
//"vec4 sampleColor = texture(setTextureMap, scaledTextureCords);"
"mat4 mv = viewTransformMatrix * modelTransformMatrix;"
"adjustedPos = mv * adjustedPos;"
"adjustedPos = adjustedPos.yzxw;"

"gl_Position	= (projectionMatrix * adjustedPos);"

"}";

const char * const oceanRendererProgramFragSource = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif                      \n      \
\
in vec2 textureCords;"
"in vec2 scaledTextureCords;"
"in vec4 lightDir;"
"uniform sampler2D noiseTex;"
"uniform sampler2D noiseNormalTex;"

"out vec4 fragColor;                 \
\
void main (void)                    \
{"
/*"if( gl_FragDepth < .01 ){"
    "fragColor = vec4(0,0,1,1);"
    "return;"
"}"
"if( gl_FragDepth < gl_FragCoord.z ){"
    "discard;"
    "fragColor = vec4(gl_FragCoord.z,0,0,1);"
    "return;"
"}"*/
"gl_FragDepth = gl_FragCoord.z;"

"fragColor = texture(noiseTex, scaledTextureCords);"
"fragColor = texture(noiseNormalTex, scaledTextureCords);"
"fragColor = vec4(0,.2,1,.5);"

"}";

#endif /* defined(__Strategy__OceanRenderProgram__) */
