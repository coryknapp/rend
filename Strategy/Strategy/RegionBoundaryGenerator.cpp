//
//  RegionBoundaryGenerator.cpp
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "RegionBoundaryGenerator.h"

RegionBoundaryGenerator::RegionBoundaryGenerator(float northBoundry,
												float eastBoundry)
:_northBoundry(northBoundry)
,_eastBoundry(eastBoundry){
	
}

std::pair<float, float>
RegionBoundaryGenerator::center( const RegionID &rId){
	std::pair<float, float> ret;
	ret.first = _eastBoundry/4 * (1+rId*2);
	ret.second = _northBoundry/2;
	return ret;
}

RegionID
RegionBoundaryGenerator::idForPosition( const float &i, const float &j){
	if( i < 0 || i > _eastBoundry )
		return std::numeric_limits<RegionID>::max();
	if( j < 0 || j > _northBoundry)
		return std::numeric_limits<RegionID>::max();
	return (i<_eastBoundry/2.0) ? 0 : 1;
}