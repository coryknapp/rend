//
//  BackgroundRenderer.cpp
//  Strategy
//
//  Created by Cory Knapp on 10/24/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "BackgroundRenderer.h"


////////////////////////////////////////////////////////////////////////////
//  NormalMapProgram  //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void BackgroundRendererProgram::build(){

    setVertexShaderHead( backgroundProgramVertSourceTop );
    setVertexShaderBody( backgroundProgramVertSourceBottom );
    setFragmentShaderBody( backgroundProgramFragSource );

    includeUniform( "sampler2D", "backTex");

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 0;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;

    GeneratedGLSLProgram::build( menu );
    GetGLError();

}

void BackgroundRendererProgram::initBindings(){
    getUniform( _textureName, "backTex");
    GetGLError();

}

void BackgroundRendererProgram::setTexture( GLint textureName ){
    glUniform1i( _textureName, textureName);
}

////////////////////////////////////////////////////////////////////////////
//  NormalMapRenderer  /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

BackgroundRenderer::BackgroundRenderer(GLuint defaultFBOName)
:Renderer( defaultFBOName )
{
    GetGLError();
}

void BackgroundRenderer::_renderFullFrameQuad(){
    GetGLError();

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    GetGLError();
    glBindVertexArray( _fullscreenQuadVAO.vaoName() );
    glDrawElements( _fullscreenQuadVAO.mode(), _fullscreenQuadVAO.elementCount(), GL_UNSIGNED_INT, 0 );
    GetGLError();

}

void BackgroundRenderer::render(){

    // clear the fbo we're supposed to be drawing on
    glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    glClearColor( 1,0,0,0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    GetGLError();

    _program.use();
    glActiveTexture(GL_TEXTURE0 + _tex.textureName() );
    glBindTexture(GL_TEXTURE_2D, _tex.textureName() );
    _program.setTexture( _tex.textureName() );
    GetGLError();
    _renderFullFrameQuad();

    GetGLError();
}


void BackgroundRenderer::resize(GLuint width, GLuint height){
    Renderer::resize( width, height);
}

void BackgroundRenderer::initialize(){
    _fullscreenQuadVAO = createFullFrameQuadVAO();
    _tex.loadFile( ResourcesSingleton()->pathForResource( "background.png" ).c_str() );
    _program.build();
    GetGLError();
    Renderer::initialize();
}