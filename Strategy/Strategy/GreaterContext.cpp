//
//  GreaterContext.cpp
//  Strategy
//
//  Created by Cory Knapp on 3/19/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "GreaterContext.h"


GreaterContext::GreaterContext( const SimTime& time ){
    _lastTime = time;
    _userCamera.reset( new WorldCamera );
    _userCamera->setGreaterContext( this );
    _inputInterpreter.reset( new FlyCameraInputInterpreter );
    _inputInterpreter->setGreaterContext( this );
	_currentStage.initWithSimpleDemo();
}

void GreaterContext::updateContext( const SimTime& time ){
    SimTime delta = time - _lastTime;
    
    if( _userCamera ){
        _userCamera -> tick( delta );
    }
    
    _lastTime = time;
}

WorldCamera * GreaterContext::userCamera() const{
    // shouldn't we just be returning a Camera?
    return (WorldCamera *)_userCamera.get();
}

UserInputInterpreter * GreaterContext::inputInterpreter(){
    return (UserInputInterpreter *)_inputInterpreter.get();
}

float * GreaterContext::sunPosition(){
    
    // TODO calc sun position
    return _sunPosition;
};

