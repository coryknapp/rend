//
//  SimState.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/2/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "SimState.h"
/*
std::unique_ptr<class SimState> SimStateFactory(
                                              size_t grid_i,
                                              size_t grid_j,
                                              HeatMap * heightMap,
                                              HeatMap * normalMap,
                                              HeatMap * temperatureMap,
                                              HeatMap * precipitationMap
                                              ){

    std::unique_ptr<class SimState> ptr( new SimState(grid_i, grid_j) );

    ptr -> setHeightMap( heightMap );
    ptr -> setNormalMap( normalMap );
    ptr -> setTemperatureMap( temperatureMap );
    ptr -> setPrecipitationMap( precipitationMap );

    ptr -> calculateTileScores(0,grid_i,0,grid_j);
    return ptr;
}

SimState::SimState( size_t grid_i, size_t grid_j) :
    _grid(grid_i,grid_j){

    _simStartTime = getSimTime();
    _lastTime = getSimTime();
    const size_t agentCount = 5;
    for( int n = 0; n < agentCount; n++ ){
        SimAgent * newAgent = new SimAgent;
        SimStubMovementBehavior * mb = new SimStubMovementBehavior(); // xxx poor memory management.
        newAgent -> setMovementBehavior( mb );
        _agentVec.push_back( newAgent );
    }
}

SimState::~SimState(){

}

void SimState::setHeightMap( HeatMap * map){
    assert( _heightMap == nullptr ); // map already set.
    _heightMap = map;
}

void SimState::setNormalMap( HeatMap * map){
    assert( _normalMap == nullptr ); // map already set.
    _normalMap = map;
}

void SimState::setTemperatureMap( HeatMap * map){
    assert( _temperatureMap == nullptr ); // map already set.
    _temperatureMap = map;
}

void SimState::setPrecipitationMap( HeatMap * map){
    assert( _precipitationMap == nullptr ); // map already set.
    _precipitationMap = map;
}


HeatMap * SimState::heightMap(){
    return _heightMap;
}

HeatMap * SimState::normalMap(){
    return _normalMap;
}

HeatMap * SimState::temperatureMap(){
    return _temperatureMap;
}

HeatMap * SimState::precipitationMap(){
    return _precipitationMap;
}

void SimState::calculateTileScores( size_t minI, size_t maxI, size_t minJ, size_t maxJ){
    float iPercent, jPercent;



    for( size_t i = minI; i < maxI; i++ )
        for( size_t j = minJ; i < maxJ; i++ ){
            iPercent = float(i)/_grid.width();
            jPercent = float(j)/_grid.height();

            SimTile& tile = _grid.getObject(i, j);
            tile._mobility = 8;
            tile._potableWater = 8;
            tile._lowCalorieVegetation = 8;
            tile._highCalorieVegetation = 8;
        }

    for( size_t i = minI; i < maxI; i++ )
        for( size_t j = minJ; i < maxJ; i++ ){
            iPercent = float(i)/_grid.width();
            jPercent = float(j)/_grid.height();

            SimTile& tile  = _grid.getObject(i, j);
            assert( tile._mobility == 8 );
        }
}

void SimState::step( const SimTime &currentTime ){

    SimTime delta = currentTime - _lastTime;
    // update the agents
    for(auto it = agentVectorStart(); it != agentVectorEnd(); ++it) {
        SimAgent * agent = *it;
        agent->step( currentTime );
    }

    // update the light source position
    _sunPosition[0] = sinf( getSimTimeSecond( currentTime - _simStartTime ) /2.0 );
    _sunPosition[2] = cosf( getSimTimeSecond( currentTime - _simStartTime ) /2.0 );

    _lastTime = currentTime;
}

float * SimState::sunPosition(){
    return _sunPosition;
}*/