//
//  RegionBoundaryGenerator.h
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__RegionBoundaryGenerator__
#define __Strategy__RegionBoundaryGenerator__

#include <iostream>

typedef unsigned RegionID;

class RegionBoundaryGenerator {
	
	const unsigned regionTypeCount = 2;
	const unsigned regionCount = 2;
	
	float _northBoundry;
	float _eastBoundry;
	
public:
	RegionBoundaryGenerator(float northBoundry,
						   float eastBoundry);
	
	std::pair<float, float> center( const RegionID &rId);
	RegionID idForPosition( const float &i, const float &j);
};

#endif /* defined(__Strategy__RegionBoundaryGenerator__) */
