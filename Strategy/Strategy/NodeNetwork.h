//
//  NodeNetwork.h
//  Strategy
//
//  Created by Cory Knapp on 6/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__NodeNetwork__
#define __Strategy__NodeNetwork__

#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <functional>
#include <cassert>

template< typename T >
class NodeNetwork;

template< typename T >
class Node {
    
public:
    
    // !!! public?
    std::unique_ptr<T> _v;
    std::vector<Node*> _adjacentEdges;

    void takeOwnershipOf( T * newObj ){
        _v.reset( newObj );
    }
    void connectToNode( Node * adj ){
        //assert that we're not already connected  // fix !!!
        //assert( std::find(_adjacentEdges.begin(),
        //                   _adjacentEdges.end(),
        //                   adj) == _adjacentEdges.end());
        assert( adj->_v != _v);
        if( std::find(_adjacentEdges.begin(),
                      _adjacentEdges.end(),
                      adj) == _adjacentEdges.end() )
            _adjacentEdges.push_back( adj );
    }
    
    void disconnectToNode( Node * adj ){

        for(auto iter = _adjacentEdges.begin();
            iter != _adjacentEdges.end();
            iter++){
            if( *iter != adj ){
                _adjacentEdges.erase( iter );
                return;
            }
        }
    }

};


// NodeNetworkIterator to totally hide the Node class.  This will direct straight
// to the templated type.

template< typename T>
class NodeNetworkIterator{

    typedef typename std::map<T *, std::unique_ptr< Node<T> >>::iterator internalIteratorType;

public:
    internalIteratorType _internalIterator;
    
    NodeNetworkIterator( internalIteratorType itt ) :
        _internalIterator(itt){}
    
    bool operator==(NodeNetworkIterator const& rhs) const {
        return (_internalIterator==rhs._internalIterator);
    }
    
    bool operator!=(NodeNetworkIterator const& rhs) const {
        return !(*this==rhs);
    }

    NodeNetworkIterator& operator++(int) {
        _internalIterator++;
        return *this;
    }
    
    NodeNetworkIterator& operator++() {
        _internalIterator++;
        return *this;
    }
    
    T * operator* () const {
        return _internalIterator->second->_v.get();
    }

    T * operator->(){
        return _internalIterator->second->_v.get();
    }
    
};

template< typename T>
class NodeNetworkEdgeIterator{
    
    typedef typename std::vector<Node<T>*>::iterator internalIteratorType;
    
public:
    internalIteratorType _internalIterator;
    
    NodeNetworkEdgeIterator( const internalIteratorType & itt ) :
    _internalIterator(itt){}
    
    bool operator==(NodeNetworkEdgeIterator const& rhs) const {
        return (_internalIterator==rhs._internalIterator);
    }
    
    bool operator!=(NodeNetworkEdgeIterator const& rhs) const {
        return !(*this==rhs);
    }
    
    NodeNetworkEdgeIterator& operator++() {
        _internalIterator++;
        return *this;
    }
    
    T * operator* () const {
        return (*_internalIterator)->_v.get(); // what the fuck is this nonsense?
    }
    
    T * operator->(){
        return operator*();
    }
    
};

// the function of the NodeNetwork classes have shift to maintain more info
// about geography.  Maybe we shoudl think about renaming it. !!!
template <typename E>
struct NodeNetworkEdge {
    // so far unused !!!
    std::unique_ptr<E> _v;
};

template< typename T >
class NodeNetwork {
    
    std::map< T *, std::unique_ptr< Node<T> > > _masterMap;
	std::vector< std::pair<T*, T*> > _edgeList;
public:
    
    void newNodeByTakingOwnership( T * newObj ){
        auto nn = new Node<T>;
        nn->takeOwnershipOf( newObj );
        _masterMap[newObj].reset( nn );
    }

    T * newNodeByCopyingNode( const T& newObj ){
        auto nn = new Node<T>;
        T * newT = new T;   // what the shit? !!!
        *newT = newObj;
        nn->takeOwnershipOf( newT );
        _masterMap[newT].reset( nn );
        return newT;
    }
    
    void connectNodes( T * a, T * b){
		if( a == b )
			return; //don't connect nodes to themselves.
					//silently return.
        auto aNode = _masterMap[a].get();
        auto bNode = _masterMap[b].get();
        
        aNode->connectToNode( bNode );
        bNode->connectToNode( aNode );
		// we can optimize in the future by sorting aNode and bNode ?
		std::pair<T*, T*> newEdge( a, b);
		_edgeList.push_back( newEdge );
    }

    void disconnectNodes( T * a, T * b){
		// does not currently remove node from edge list.
		assert(false);
        auto aNode = _masterMap[a].get();
        auto bNode = _masterMap[b].get();
        
        aNode->disconnectToNode( bNode );
        bNode->disconnectToNode( aNode );
    }
    
    bool contains( const T * p ){
        return _masterMap.find( const_cast<T*>( p ) ) != _masterMap.end();
    }
    
    size_t size() const{
        return _masterMap.size();
    }
    
    NodeNetworkIterator<T> begin() { //!!! all this shit should be marked const.
        return NodeNetworkIterator<T>( _masterMap.begin() );
    };
    
    NodeNetworkIterator<T> end() {
        return NodeNetworkIterator<T>( _masterMap.end() );
    };
    
    NodeNetworkEdgeIterator<T> beginingOfNodesConnectedTo( T * n ) {
        return NodeNetworkEdgeIterator<T>( _masterMap[n]->_adjacentEdges.begin() );
    };
    
    NodeNetworkEdgeIterator<T>  endingOfNodesConnectedTo( T * n ) {
        return NodeNetworkEdgeIterator<T>( _masterMap[n]->_adjacentEdges.end() );
    };
    
	auto edgeListBegin() -> typename decltype(_edgeList)::iterator {
		return _edgeList.begin();
	}
	
	auto edgeListEnd() -> typename decltype(_edgeList)::iterator {
		return _edgeList.end();
	}
	
    template< typename distanceFunc>
    T * mostLike( T * node){
        distanceFunc functor;
        T * candidate = _masterMap.begin()->second->_v.get();
        float distance = std::numeric_limits<float>::max();
        for(auto iter = _masterMap.begin(); // why are we not using the
                                            // NodeNetworkIterator stuff we
                                            // worked so hard on? !!!
            iter != _masterMap.end();
            iter++){
            if( iter->second->_v.get() != node ){
                float newDistance = functor(*(iter->second->_v.get()),
                                                *node );
                if( newDistance < distance){
                    distance = newDistance;
                    candidate = (iter->second->_v.get());
                }
            }
        }

        return candidate;
    }
    
};

#endif /* defined(__Strategy__NodeNetwork__) */
