//
//  WorldRenderProgram.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/3/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "WorldRenderProgram.h"

void WorldRendererProgram::build(){
    GLSLProgram::build();

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 1;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 0;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;
    
    std::string vertCode = worldRendererProgramVertSourceTop + attributeCodeForMenu( menu ) + worldRendererProgramVertSourceBottom;
    std::string fragCode( worldRendererProgramFragSource );

    _programName = buildProgram(vertCode, fragCode, menu);

    
}

void WorldRendererProgram::initBindings(){
    //ThreeD
    getUniform( _projectionMatrixUniform, "projectionMatrix" );
    getUniform( _viewTransformMatrixUniform, "viewTransformMatrix" );
    getUniform( _modelTransformMatrixUniform, "modelTransformMatrix" );
    getUniform( _globalTintColor, "globalTintColor" );
}

void WorldRendererProgram::setProjectionMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _projectionMatrixUniform, 1, false, m);

}
void WorldRendererProgram::setViewTransformMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _viewTransformMatrixUniform, 1, false, m); //xxx

}
void WorldRendererProgram::setModelTransformMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _modelTransformMatrixUniform, 1, false, m);

}


void WorldRendererProgram::setGlobalTintColor( GLfloat r, GLfloat g, GLfloat b, GLfloat a ){
    glUniform4f( _globalTintColor, r, g, b, a);
    GetGLError();
}