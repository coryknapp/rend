//
//  RenderableAsset.cpp
//  Strategy
//
//  Created by Cory Knapp on 6/20/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "RenderableAsset.h"

RenderableAsset::RenderableAsset(const RendAssetID &assignedId,
                                 const std::string &modelName ):
    _id( assignedId ){

	_vao.reset( new AssimpVAO() );
	_vao->loadFile( modelName );
	
	assert( _vao->vaoName() > 0 );
}

void RenderableAssetManager::_loadAssetWithName( const std::string &name ){
    assert( _idsByName.find(name) == _idsByName.end() );
    _lastAsignedId++;
    _assetsById[_lastAsignedId].reset(
									  new RenderableAsset(_lastAsignedId, name)
									  );
	_idsByName[name] = _lastAsignedId;
}

RendAssetID RenderableAssetManager::IdforName( const std::string &name ){
    if( _idsByName.find(name) == _idsByName.end() ){
        _loadAssetWithName( name );
    }
	assert( _idsByName[name] > 0 );
    return _idsByName[name];
}

RenderableAsset * RenderableAssetManager::assetForId( const RendAssetID &id ){
    return _assetsById[id].get();
}