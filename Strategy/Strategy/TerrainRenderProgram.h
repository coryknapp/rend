//
//  TerrainRenderProgram.h
//  Strategy
//
//  Created by Cory Knapp on 9/24/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TerrainRenderProgram__
#define __Strategy__TerrainRenderProgram__

#include <iostream>

#include "GLSLProgram.h"
#include "GeneratedGLSLProgram.h" // for attributeCodeForMenu(...)

class TerrainRenderProgram : public GeneratedGLSLProgram {

    // uniform locations

    GLint _projectionMatrixUniform;
    GLint _viewTransformMatrixUniform;
    GLint _modelTransformMatrixUniform;

    GLint _textureTransformUniform;
    GLint _textureScaleTransform;

    GLint _lightPositionUniform;

    GLint _TextureMapTexture; // xxx to be removed
    GLint _normalMapTexture;
    GLint _redTexture;
    GLint _greenTexture;
    GLint _blueTexture;

public:

    void build();

    void initBindings();

    void setProjectionMatrix(const GLfloat * m );
    void setViewTransformMatrix(const GLfloat * m );
    void setModelTransformMatrix(const GLfloat * m );
    //void setGlobalTintColor( GLfloat r, GLfloat g, GLfloat b, GLfloat a );
    void setTextureTransform( GLfloat s, GLfloat t);
    void setTextureScaleTransform( GLfloat s );
    void setLightPositionUniform( GLfloat * v );

    void setTextureMapTexture( GLint textureName );
    void setNormalMapTexture( GLint textureName );
    void setRedTexture( GLint textureName );
    void setGreenTexture( GLint textureName );
    void setBlueTexture( GLint textureName );
};

const char * const terrainRendererProgramVertSourceTop = "    \
#ifdef GL_ES                            \n\
precision highp float;                  \n\
#endif                                  \n\
\
 out vec2 scaledTextureCords;"
"out vec2 textureCords;"
"out vec4 lightDir;";

// attributes will be inserted here

const char * const terrainRendererProgramVertSourceBottom =
"void main (void)"
"{"
    "scaledTextureCords = (TEXTURECOORD_ATTRIBUTE_INDEX_0 + textureTransform) * textureScaleTransform;"
    "textureCords = TEXTURECOORD_ATTRIBUTE_INDEX_0;"
    "lightDir = normalize( lightPosition );"
    "vec4 adjustedPos = POSITION_ATTRIBUTE_INDEX_0;"
    "vec4 sampleColor = texture(setTextureMap, scaledTextureCords);"
    "mat4 mv = viewTransformMatrix * modelTransformMatrix;"
    "adjustedPos = mv * adjustedPos;"
    "adjustedPos = adjustedPos.yzxw;"

    "sampleColor = vec4(0,1,0,1);vec4( adjustedPos.z, 0, 1-adjustedPos.z,1);"

    "gl_Position	= (projectionMatrix * adjustedPos);"

"}";

const char * const terrainRendererProgramFragSource = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif                      \n      \
\
in vec2 textureCords;"
"in vec2 scaledTextureCords;"
"in vec4 lightDir;"
"uniform sampler2D textureMap;"
"uniform sampler2D normalMap;"
"uniform sampler2D redTex;"
"uniform sampler2D greenTex;"
"uniform sampler2D blueTex;"

"out vec4 fragColor;                 \
\
void main (void)                    \
{                                   \
gl_FragDepth = gl_FragCoord.z;"
"vec3 colorMap = normalize( texture(textureMap, scaledTextureCords).rgb );"
//"vec3 colorMap = texture(textureMap, scaledTextureCords).rgb;"
"vec4 redColor = texture(redTex, textureCords);"
"vec4 greenColor = texture(greenTex, textureCords);"
"vec4 blueColor = texture(blueTex, textureCords);\
fragColor = redColor * colorMap.r + greenColor * colorMap.g + blueColor * colorMap.b;"
"vec4 adjustedNormal = (texture(normalMap, scaledTextureCords) * 2 -1).yzxw;"
"float lightIntensity = dot( lightDir.xyz, adjustedNormal.xyz );"
"if( lightIntensity < 0 )"
   "lightIntensity = 0;"
"const float globalLightPercent = .3;"
"fragColor *= (lightIntensity * (1-globalLightPercent) + globalLightPercent);"
"fragColor = texture(normalMap, scaledTextureCords);"
"fragColor.a = 1.0;"
"}";

#endif /* defined(__Strategy__TerrainRenderProgram__) */
