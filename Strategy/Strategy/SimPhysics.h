//
//  SimPhysics.h
//  Strategy
//
//  Created by Cory Knapp on 11/20/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__SimPhysics__
#define __Strategy__SimPhysics__

#include <iostream>

#include "FBOLinkedPlanarData.h"

class SimPhysics {

    // aggrigates all the information about the physics
    // gathers from the renderer.

    FBOLinkedPlanarData * _normalMap = nullptr;

public:

    void setNormalMapFBO( const FrameBufferObjectCollection &fbo ){

        // get render buffer dimentions
        GLint height;
        GLint width;

        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        _normalMap = new FBOLinkedPlanarData( fbo, (size_t)height, (size_t)width );
        _normalMap->updateAll();
    }

};

#endif /* defined(__Strategy__SimPhysics__) */
