//
//  Waypoint.h
//  Strategy
//
//  Created by Cory Knapp on 6/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Waypoint__
#define __Waypoint__

#include <iostream>
#include <memory>
#include <math.h>
#include "NodeNetwork.h"
#include "Geometry.h"
#include "ContainerAssist.h"
#include <set>

#include "RegionBoundaryGenerator.h"

class Waypoint {
    
public:
    
    float mapPosition[2]; // !!! public?
	RegionID region = std::numeric_limits<RegionID>::max();
};

typedef NodeNetwork<Waypoint> WaypointNetwork;

bool crossExistingEdge( WaypointNetwork &net, Waypoint &a, Waypoint &b );
float smallestDistanceToExistingEdge(WaypointNetwork &net,
									 Waypoint &a);

std::set<Waypoint*> closestWaypointsTo( Waypoint &wp, WaypointNetwork &wn,
										 unsigned const &maxReturnSize);

struct WaypointDistanceBetweenFunctor {
    float operator()(const Waypoint& a, const Waypoint& b){
        return sqrtf( (a.mapPosition[0] - b.mapPosition[0]) * (a.mapPosition[0] - b.mapPosition[0]) +
                     (a.mapPosition[1] - b.mapPosition[1]) * (a.mapPosition[1] - b.mapPosition[1]));
    }
};

struct WaypointSetFirstCoordFunctor {
    void operator()(Waypoint& a, float i){
        a.mapPosition[0] = i;
    };
};

struct WaypointSetSecondCoordFunctor {
    void operator()(Waypoint& a, float j){
        a.mapPosition[1] = j;
    };
};

struct WaypointGetFirstCoordFunctor {
    float operator()(const Waypoint& a){
        return a.mapPosition[0];
    };
};

struct WaypointGetSecondCoordFunctor {
    float operator()(const Waypoint& a){
        return a.mapPosition[1];
    };
};

struct WaypointNetworkAddFunctor{
    Waypoint * operator ()(WaypointNetwork &n, Waypoint &node){
        return n.newNodeByCopyingNode( node );
    }
};

struct WaypointNetworkConnectFunctor{
    void operator ()(WaypointNetwork &n, Waypoint &a, Waypoint &b){
        n.connectNodes(&a, &b);
    }
};

#endif /* defined(__Strategy__Waypoint__) */
