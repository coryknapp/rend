//
//  RegionGeneratorAbstract.cpp
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "RegionGeneratorAbstract.h"

RegionGeneratorAbstract::RegionGeneratorAbstract(){
	
}

void RegionGeneratorAbstract::setNetwork( WaypointNetwork * network ){
	_network = network;
}

void RegionGeneratorAbstract::setGenerator(
										   RegionBoundaryGenerator * boundaryGen
										   ){
	_boundaryGen = boundaryGen;
}

void RegionGeneratorAbstract::setRegionID( const RegionID &rId ){
	_rId = rId;
}

RegionID RegionGeneratorAbstract::rId(){
	return _rId;
}

bool RegionGeneratorAbstract::inBounds( float i, float j ){
	return ( _rId == _boundaryGen->idForPosition(i, j) );
}

std::pair<float, float> RegionGeneratorAbstract::center(){
	return _boundaryGen->center( _rId );
}