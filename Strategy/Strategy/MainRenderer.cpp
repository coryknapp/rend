//
//  MainRenderer.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/1/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "MainRenderer.h"

void MainRenderer::prepareTexturesForRender(){

    // update greater context.  XXX this shouldn't be here.
    SimTime now = getSimTime();
    greaterContext()->updateContext(now);

    _safeCamera = *(greaterContext()->userCamera());

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    _samplesFBOGroup.bind(regular_fbo_index);
    _worldRenderer -> render();
    GetGLError();

    _samplesFBOGroup.bind(ocean_fbo_index);
    _oceanRenderer -> render();
    GetGLError();
    _samplesFBOGroup.bind(background_fbo_index);
    _backgroundRenderer -> render();
    GetGLError();
}

void MainRenderer::initProgram(){

    _worldRenderer = new WorldRenderer( _samplesFBOGroup.textureColorName(regular_fbo_index) );
    _worldRenderer->resize( this->viewWidth(),this->viewHeight() );
    GetGLError();
    _oceanRenderer = new OceanRenderer( _samplesFBOGroup.textureColorName(ocean_fbo_index) );
    _oceanRenderer->resize( this->viewWidth(),this->viewHeight() );
    GetGLError();
    _worldRenderer->setCamera( &_safeCamera );
    _oceanRenderer->setCamera( &_safeCamera );
    GetGLError();
    _worldRenderer->initialize();
    _oceanRenderer->initialize();
    GetGLError();
    _worldRenderer->declareReadyToRender();
    _oceanRenderer->declareReadyToRender();
    GetGLError();

    _backgroundRenderer = new BackgroundRenderer( _samplesFBOGroup.textureColorName(background_fbo_index) );
    _backgroundRenderer ->Renderer::resize( this->viewWidth(),this->viewHeight() );
    _backgroundRenderer->initialize();
    _backgroundRenderer->declareReadyToRender();

    MainBlenderProgram * mainProgram = new MainBlenderProgram;
    mainProgram -> build();
    setProgram( mainProgram );
}

void MainRenderer::resize(GLuint width, GLuint height){
    TextureBlendingRenderer::resize( width, height);
    _safeCamera.setViewSize( width, height);
    if( _worldRenderer ){
        _worldRenderer->resize( this->viewWidth(),this->viewHeight() );
        _backgroundRenderer ->Renderer::resize( this->viewWidth(),this->viewHeight() );
    }

    if( _oceanRenderer ){
        _oceanRenderer->resize( this->viewWidth(),this->viewHeight() );
        _oceanRenderer ->Renderer::resize( this->viewWidth(),this->viewHeight() );
    }

    if( greaterContext() ){
        greaterContext()->userCamera() -> setViewSize( width, height);
    }
}