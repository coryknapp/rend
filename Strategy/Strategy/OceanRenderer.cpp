//
//  OceanRenderer.cpp
//  Strategy
//
//  Created by Cory Knapp on 2/9/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "OceanRenderer.h"

void OceanRenderer::_initOceanRendering(){

    // init VBO
    _oceanSurfaceVAO.build(0,0,-1,-1,30,30);

}

void OceanRenderer::initialize(){

    ////////////////////////////////////////////////////////////////////////////////////
    //   generate the ocean
    ////////////////////////////////////////////////////////////////////////////////////
    _initOceanRendering();

    ////////////////////////////////////////////////////////////////////////////////////
    //  Build the program
    ////////////////////////////////////////////////////////////////////////////////////

    _program.build();
    _program.initBindings();

    _program.setTextureScaleTransform( 1/16.0f );

    _program.use();

/*    glActiveTexture(GL_TEXTURE0 + _textureMap.textureName() );
    glBindTexture(GL_TEXTURE_2D, _textureMap.textureName() );
    _terrainProgram.setTextureMapTexture( _textureMap.textureName() );
    GetGLError();

*/
}

void OceanRenderer::render(){
    ThreeDRenderer::render(); // clears the buffer and sets up the matrix

    glClearColor( 1,0,0,0 );
    glClear( GL_COLOR_BUFFER_BIT );
    GetGLError();

    _program.use();
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    GetGLError();

    _program.setProjectionMatrix( glm::value_ptr( camera()->projectionMatrix() ) );
    _program.setViewTransformMatrix( glm::value_ptr( camera()->viewTransformMatrix() ) );

	glm::mat4 modelMat;
    // render ocean
    modelMat = glm::scale( modelMat, glm::vec3( .1, .1, .1) );
	modelMat = glm::translate(modelMat,
							  glm::vec3(0, 0, _oceanLevel) );
    _program.use();
    _program.setModelTransformMatrix( glm::value_ptr( modelMat ) );
    //_program.setGlobalTintColor( 0, .4, 1, 1);

    

    GetGLError();
    glBindVertexArray( _oceanSurfaceVAO.vaoName() );
    //glDrawElements( _oceanSurfaceVAO.mode(), _oceanSurfaceVAO.elementCount(), GL_UNSIGNED_INT, nullptr );
    GetGLError();

}
