//
//  TreeLayoutGenerator.h
//  Strategy
//
//  Created by Cory Knapp on 7/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TreeLayoutGenerator__
#define __Strategy__TreeLayoutGenerator__

#include <math.h>
#include <cassert>

#include <iostream>
#include <deque>
#include <vector>

#include "Geometry.h"

template<
typename T,
typename firstSetter,  // (const T&)
typename secondSetter, // (const T&)
typename firstGetter,  // ()
typename secondGetter, // ()
typename Network,
typename addToNetworkFunctor, // T * (Network, const T&)
typename connectFunctor // (Network N, const T&, const T&)
>
class TreeLayoutGenerator{
    Network &_network;
    std::deque<T*> _openList;
    const float _annulus = .50; // !!! should be a global value or customizable
    const int _candidateAttemptsLimit = 16;
    
    firstSetter _firstSetter;
    secondSetter _secondSetter;
    firstGetter _firstGetter;
    secondGetter _secondGetter;
    addToNetworkFunctor _addNodeToNetworkFunctor;
    connectFunctor _connectFunctor;
    
    float _calculateStartingAngle( const T &p ){ // based on the nearest bounds
        
        float angle = NAN;
        float x = _firstGetter(p);
        float y = _secondGetter(p);
            // lets try and move away from the closest edge.
            // we have very limited info about our bounds; we'll send out eight
            // "tracers" and stop when one is out of bounds.
            float dist = 0;
            do{
                assert( dist < 99999 ); // we should have hit some bounds by now
                //north
                if( !_inBounds( x, y + dist) )
                    angle = M_PI_2;
                //south
                if( !_inBounds( x, y - dist) )
                    angle = M_PI_2 + M_PI;
                //east
                if( !_inBounds( x + dist, y ) )
                    angle = 0;
                //west
                if( !_inBounds( x, y - dist) )
                    angle = M_PI;
                dist += .1;
            }while ( isnan( angle ) );
            angle += M_PI;  //we found the angle twords the nearest boundry, we
                            //want to go away from that.
        return angle;
        
    }
    
    float _angleFor( const T &p){// !!! a lot of this stuff breaks the templating
                                 // if the user is using something other then
                                 // out own NodeNetwork class.
        // find an aproprate angle for a branch coming off p.
        
        std::vector<T*> connectedNodes;
        // if an error happens on the next line, it's likely because p does not,
        // exist on _network.
        if( _network.size() ){
            for(    auto iter = _network.beginingOfNodesConnectedTo( const_cast<T*>( &p ) );
                    iter != _network.endingOfNodesConnectedTo( const_cast<T*>( &p ) );
                    ++iter ){
                connectedNodes.push_back( *iter );
            }
        }
        
        // A lone point.  We'll make a smart guess about which way we should
		//start
        
		//
		//going.
        if( connectedNodes.size() == 0 ){
            return _calculateStartingAngle( p );
        }
        // if there is only one point connected to p, then the angle
        // will be an extrapolation of that line.
        if( connectedNodes.size() == 1 )
            return _getAngleBetween( p, *connectedNodes[0]);
        // if there are two nodes connected, suggest we split off a branch that
        // maximizes the differace between the angles.
        if( connectedNodes.size() == 2 ){
            // !!! for now, just do 90 degrees of the vector formed by p and one
            // of it's buddies.
            return _getAngleBetween( p, *connectedNodes[0]) + M_PI/2;
        }
        assert( true ); // we should never be connected to more then 2 nodes in
                        // a tree layout.
        return 0; // silence the warning
    }
    
    float _getAngleBetween( const T &a, const T &b){
        float vec[3];
        float angle;
        vec[0] = _firstGetter( b ) - _firstGetter( a );
        vec[1] = _secondGetter( b ) - _secondGetter( a );
        vec[2] = 0;
        normalizeVector( 3, vec);
        angle = asinf( vec[0] );
        if( vec[0] < 0 )
            angle += ((rand()%2)*2-1) * M_PI/2.0; // randomize if we add 90 or subtract 90 degrees.
        return angle;
    }
    
    T * _addPoint( T &p ){ // returns a referance to the added point.
        auto added = _addNodeToNetworkFunctor( _network, p);
        _openList.push_back( added );
        return added;
    }
    
public:
    TreeLayoutGenerator( Network &net ) :
    _network( net ){
        srand( (unsigned int)time(nullptr) );
        T dummy;
        _firstSetter( dummy, 0 );
        _secondSetter( dummy, 0 );
        
        auto candidate = _candidate( dummy );
        _addPoint( *candidate);
        //TreeLayoutGenerator( net, dummy);
    }
    
    TreeLayoutGenerator( Network &net, T &firstNode ) :
        _network( net ){
        _addPoint(firstNode);
    }
    
    bool iterate(){
        // Take the first point on the open list and expand it.
        // If it can't be expanded, put it on the closed list.

        //if( _network.size() > 36 )  // !!! if we ever want to implement a limit,
                                    // do it here.
        //    return false;
        if( _openList.empty() ){
            return false;
        }
        
        T &openPoint = *_openList.back();
        
        for (int i = 0; i < _candidateAttemptsLimit; i++) {
            auto candidate = _candidate( openPoint );
            if( candidate.get() == nullptr ){
                _openList.pop_back();
                return true; // keep looking for more points
            }
            // all elements in openList should exist on the network already so no
            // need to check the openList.
            for( auto iter = _network.begin(); iter != _network.end(); ++iter ){
                if( _inRange( **iter, *candidate ) ){
					std::cout<<"rejecting(range)\n";
                    goto retry;
                }
                if( _crossesExisitingEdge( *candidate, openPoint) ){
					std::cout<<"rejecting(crosses edge)\n";
                    goto retry;
                }
				if( _toCloseToExisitingEdge( *candidate ) ){
					std::cout<<"rejecting(to close to edge)\n";
					goto retry;
				}
            }
            // we got through both loops without rejecting the point, so it's good.
            {   // the compiler complanes when there is more then one line preseding
                // the retry: label, so this is a hack around that possible
                // compiler bug? (???)
                _addPoint( *candidate );
                auto p = _addPoint( *candidate );
                _connectFunctor( _network, *p, openPoint );
                return true;
            }
        retry:
            //std::cout << "retrying" << std::endl;
            0; // can't have a label at the end of a block, i guess.
        }
        _openList.pop_back();
        return true; // we couldn't do anything this iteration but let's try again.
    }
    
    std::unique_ptr<T> _candidate( const T &p ){
        // find a suitable point near p.  p will usaully be an existing point on
        // _network, but may be a dummy.
		// _candidate only guaranties the result will be inbounds.
        float radius;
        T * candidate = new T;
        float angle = _angleFor(p);
        //std::cout << "angle = " << angle << std::endl;
        // set a limiter.
        float randAngle;
        int counter = 0;
        do {
            radius = rand()/(float)RAND_MAX*(_annulus) + _annulus;
            randAngle = angle + (rand()/(float)RAND_MAX)*(M_PI*2)/8;
            //std::cout << "rand angle = " << randAngle << std::endl;
 
            _firstSetter(   *candidate, _firstGetter(p) + radius * cosf( randAngle ) );
            _secondSetter(  *candidate, _secondGetter(p) + radius * sinf( randAngle ) );
            counter++;
            if(counter > _candidateAttemptsLimit){
                delete candidate;
                return std::unique_ptr<T>();
            }
        } while(!_inBounds(*candidate) );
        return std::unique_ptr<T>(candidate);
    }
    
    bool _inBounds( const T &p ){   //!!! maybe move _inbounds into some kind of
                                    //multi inhearatince thing so different
                                    //generators can share code for differnent
                                    //shapes
        return _inBounds( _firstGetter(p), _secondGetter(p) );
        
    }
    
    bool _inBounds( const float &x, const float y ){

        /*return  ((x > 0) &&
                 (x < 4) &&
                 (y > 0) &&
                 (y < 4));*/
        return (x*x + y*y) < 500;
        
    }
    
    bool _inRange( const T &a, const T &b ){
        return  ( (_firstGetter(a) - _firstGetter(b)) * (_firstGetter(a) - _firstGetter(b)) ) +
        ( (_secondGetter(a) - _secondGetter(b)) * (_secondGetter(a) - _secondGetter(b)) ) < _annulus * _annulus ? true : false;
    }
    
    bool _crossesExisitingEdge( T &a, T &b ){ // should have const parameters
        return crossExistingEdge(_network, a, b);
    }
	
	bool _toCloseToExisitingEdge( T &a ){ // should have const parameters
        return ( smallestDistanceToExistingEdge(_network, a) < .5 );//!!! magic number
    }
};

#endif /* defined(__Strategy__TreeLayoutGenerator__) */
