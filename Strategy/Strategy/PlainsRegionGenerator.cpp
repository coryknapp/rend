//
//  PlainsRegionGenerator.cpp
//  Strategy
//
//  Created by Cory Knapp on 8/7/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "PlainsRegionGenerator.h"

PlainsRegionGenerator::PlainsRegionGenerator(){
	
}

void PlainsRegionGenerator::setNetwork( WaypointNetwork * network ){
	_network = network;
	RegionGeneratorAbstract::setNetwork( _network );
	_generator.reset(new generatorType( *_network,
					static_cast<RegionGeneratorAbstract*>( this ) ) );
}

bool PlainsRegionGenerator::iterate(){
	return _generator->iterate();
}

void PlainsRegionGenerator::connectNodesInternally(){

	// make a list of possible edges.
	// we can exclude edges that would be really long (cont.)
	const float minListingDistance = 5;
	//and edges that don't connect waypoints of our type (as in RegionID)
	
	typedef std::pair<Waypoint*, Waypoint*> edge;
	std::set< std::pair<float, edge> > possibleEdgeList;
	
	for(auto m = _network->begin();
		m != _network->end();
		m++){
		if( m->region != _rId )
			goto skipNode;
		//find the nearest node to m.
		for(auto n = _network->begin();
			n != _network->end();
			n++){
			if( n->region != _rId )
				goto skipNode;
			float distance = distance2( m->mapPosition, n->mapPosition);
			if( distance > minListingDistance )
				goto skipNode;
			std::pair<float, edge> newEdgeListing;
			newEdgeListing.first = distance;
			newEdgeListing.second.first = *m;
			newEdgeListing.second.second = *n;
			possibleEdgeList.insert( newEdgeListing );
		}
	skipNode:
		0;
	}// end m loop

	// now we'll go through the list of possible edges and connect them iff
	// they don't cross an existing edge.
	for (auto e = possibleEdgeList.begin();
		 e != possibleEdgeList.end();
		 e++) {
		//does e cross any existing connections?
		for (auto existingEdge = _network->edgeListBegin();
			 existingEdge != _network->edgeListEnd();
			 existingEdge++){
			// notice e and existingEdge are not the same type
			if( doSegmentsIntersect(e->second.first->mapPosition[0],
									e->second.first->mapPosition[1],
									e->second.second->mapPosition[0],
									e->second.second->mapPosition[1],
									existingEdge->first->mapPosition[0],
									existingEdge->first->mapPosition[1],
									existingEdge->second->mapPosition[0],
									existingEdge->second->mapPosition[1]) )
				goto doNotAdd;
		}
		
		//is the line t close to an existing node?
		for (auto nodeIter = _network->begin();
			 nodeIter != _network->end();
			 nodeIter++){
			if( (*nodeIter != e->second.first)	&&
				(*nodeIter != e->second.second)	)
				if( .25 > distanceFromPointToSegment( // !!! magic number
												e->second.first->mapPosition,
												e->second.second->mapPosition,
												nodeIter->mapPosition) ) {
				goto doNotAdd;
			}
		}
		//
		_network->connectNodes(e->second.first, e->second.second);
	doNotAdd:
		std::cout << "";
	}
}