
//
//  WorldRenderer.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/1/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "WorldRenderer.h"

void WorldRenderer::initialize(){


    _redTex.loadFile( ResourcesSingleton()->pathForResource("red.png").c_str());
    _greenTex.loadFile( ResourcesSingleton()->pathForResource("green.png").c_str());
    _blueTex.loadFile( ResourcesSingleton()->pathForResource("blue.png").c_str());

    //generate the stage.  Later this will be done by the greater context.
    //_currentStage = new StageState;
    //_currentStage->initWithSimpleDemo();
    
    
    ////////////////////////////////////////////////////////////////////////////////////
    //  Load the cell model
    ////////////////////////////////////////////////////////////////////////////////////
    const char * fileName =  "cell.obj";
    std::string filePath = ResourcesSingleton()->pathForResource( fileName );
    Assimp::Importer importer;
    const aiScene*  myScene = importer.ReadFile( filePath, aiProcess_Triangulate );

    if( !myScene ){
        std::cerr << importer.GetErrorString() << std::endl;

    }
    const aiMesh* myMesh = myScene->mMeshes[0];
    _cellVAO.loadMesh( *myMesh );

    ////////////////////////////////////////////////////////////////////////////////////
    //  Build the program
    ////////////////////////////////////////////////////////////////////////////////////

    _program.build();
    _program.initBindings();

    _terrainProgram.build();
    _terrainProgram.initBindings();
    _terrainProgram.setTextureScaleTransform( 1/16.0f );
    GetGLError();

    _ZDiffProgram.build();
    _ZDiffProgram.initBindings();
    GetGLError();
    
    //_normalMapFBO.resize( 512, 512 );
    //_normalMapFBO.build();
    //normalMapTextureForHeightMap( _normalMapFBO, 512, 512, _textureMap );
    GetGLError();

    ////////////////////////////////////////////////////////////////////////////////////
    // xxx testing
    SimPhysics physics;
    //physics.setNormalMapFBO( _normalMapFBO );
    ////////////////////////////////////////////////////////////////////////////////////

    _terrainProgram.use();
    GetGLError();

    //glActiveTexture(GL_TEXTURE0 + _textureMap.textureName() );
    //glBindTexture(GL_TEXTURE_2D, _textureMap.textureName() );
    //_terrainProgram.setTextureMapTexture( _textureMap.textureName() );
    GetGLError();

    //glActiveTexture(GL_TEXTURE0 + _normalMapTex.textureName() );
    //glBindTexture(GL_TEXTURE_2D, _normalMapTex.textureName() );
    //_terrainProgram.setNormalMapTexture( _normalMapTex.textureName() );
    GetGLError();

    //glActiveTexture(GL_TEXTURE0 + _redTex.textureName() );
    //glBindTexture(GL_TEXTURE_2D, _redTex.textureName() );
    _terrainProgram.setRedTexture( _redTex.textureName() );

    //glActiveTexture(GL_TEXTURE0 + _greenTex.textureName() );
    //glBindTexture(GL_TEXTURE_2D, _greenTex.textureName() );
    _terrainProgram.setGreenTexture( _greenTex.textureName() );

    //glActiveTexture(GL_TEXTURE0 + _blueTex.textureName() );
    //glBindTexture(GL_TEXTURE_2D, _blueTex.textureName() );
    _terrainProgram.setBlueTexture( _blueTex.textureName() );

    ThreeDRenderer::initialize();
    GetGLError();


}

void WorldRenderer::buildTerrainVAOsInRange( int minI, int maxI, int minJ, int maxJ ){
     assert( minI <= maxI );
     assert( minJ <= maxJ );
    
    //const int vbosPerTerrainRegion = 8;
    
     for( int i = minI; i < maxI ; i++ ){
        for( int j = minJ; j < maxJ ; j++ ){
            /*TerrainRegion * thisRegion = _terrainManager.region(
                // the logic is different for positive and negitive values.
                i >= 0 ? i/vbosPerTerrainRegion : (i+1)/vbosPerTerrainRegion - 1,
                j >= 0 ? j/vbosPerTerrainRegion : (j+1)/vbosPerTerrainRegion - 1
                                                                );
            assert( _terrainVAOs[i][j].get() == nullptr);
            _terrainVAOs[i][j].reset( new TerrainRectVAO );
            _terrainVAOs[i][j] -> build( thisRegion -> heightMap(),
                                        i, j,
                                        i+vbosPerTerrainRegion, j+vbosPerTerrainRegion );
             */
            
        }
     }
    
}

//void WorldRenderer::buildHexVAOInRange( size_t minI, size_t maxI, size_t minJ, size_t maxJ ){
/*    assert( _hexVAOGridPtr != nullptr );
    assert( minI <= maxI );
    assert( minJ <= maxJ );
    assert( maxI <= _hexVAOGridPtr->width() );
    assert( maxJ <= _hexVAOGridPtr->height() );

    //auto heightMap = planarDataForImageFile( ResourcesSingleton()->pathForResource("map.png"));
    // heightData->updateAll();

    for( size_t i = minI; i < maxI ; i++ ){
        for( size_t j = minJ; j < maxJ ; j++ ){
            _hexVAOGridPtr->setObject(i, j, std::move( TerrainHexVAO( i, j, 16.0, _terrainInfoPtr->heightMap() , nullptr, nullptr) ) );
        }
    }
*/
//}

void WorldRenderer::render(){
    if( !greaterContext() )
        return;
	_currentStage = &greaterContext()->currentStage();
    ThreeDRenderer::render(); // clears the buffer and sets up the matrix

    // get the common RenderableAssetManager.
	RenderableAssetManager* assetManager =
		getSingletonPtr<RenderableAssetManager>();
	
    glClearColor( 1,1,0,0 );
    glClear( GL_COLOR_BUFFER_BIT );
    GetGLError();

    _program.use();
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    GetGLError();

    glBindVertexArray( _cellVAO.vaoName() );
    GetGLError();

    _program.setProjectionMatrix( glm::value_ptr( camera()->projectionMatrix() ) );
    _program.setViewTransformMatrix( glm::value_ptr( camera()->viewTransformMatrix() ));
    
    // draw the road network
    int counter = 0; // !!!
    for ( auto n = _currentStage->_roadWays->begin(); n != _currentStage->_roadWays->end(); ++n ) {
        counter++;
        //!!!
        //if( n == _currentStage->_roadWays->begin() )
        //    _program.setGlobalTintColor( 1, 1, 0, 1);
        //else
		glm::mat4 modelMat;
        const float colorScale = 32.0;
        _program.setGlobalTintColor( 1, n->region, 1-n->region, 1);
		modelMat = glm::translate( modelMat, glm::vec3( n->mapPosition[0], n->mapPosition[1], 0 ) );
		modelMat = glm::scale( modelMat, glm::vec3( .1, .1, .1 ) );
        _program.setModelTransformMatrix( glm::value_ptr( modelMat ) );
        glDrawElements( _cellVAO.mode(), _cellVAO.elementCount(), GL_UNSIGNED_INT, nullptr );
        
        // draw some dots to connected nodes
        _program.setGlobalTintColor( 0, 1, 1, 1);
        for (auto endNode = _currentStage->_roadWays->beginingOfNodesConnectedTo( *n );
             endNode != _currentStage->_roadWays->endingOfNodesConnectedTo( *n );
             ++endNode ) {
            const float increments = 16;
            for (int t = 0; t < increments; t++) {
                float x = (n->mapPosition[0] * t/increments) + (endNode->mapPosition[0] * (1-t/increments));
                float y = (n->mapPosition[1] * t/increments) + (endNode->mapPosition[1] * (1-t/increments));
                //mtxLoadTranslate( modelMat, x,y, 0);
                //mtxScaleApply( modelMat, .05, .05, .05);
                //_program.setModelTransformMatrix( modelMat );
                //glDrawElements( _cellVAO.mode(), _cellVAO.elementCount(), GL_UNSIGNED_INT, nullptr );
            }
        }
    }


    
    GetGLError();
    // draw all game objects
    for (auto n = _currentStage->_gameObjects.begin();
		 n != _currentStage->_gameObjects.end();
		 ++n ) {
        if( n.operator*()->checkComponent< DrawnGameObjectComponent >() ){
            float objXY[2];
            if(n.operator*()->
			   checkComponent<NodeBasedWorldGameObjectComponent >() ){
                NodeBasedWorldGameObjectComponent comp =
					n.operator*()->get<NodeBasedWorldGameObjectComponent>();
                //objXY[0] = comp.
                //objXY[1] = comp.j;
            } else if(n.operator*()->checkComponent< CoordBasedWorldGameObjectComponent >() ){
                CoordBasedWorldGameObjectComponent comp = n.operator*()->get<CoordBasedWorldGameObjectComponent>();
                objXY[0] = comp.i;
                objXY[1] = comp.j;
            } else {
                assert( false );
                // maybe the best way to handle this is to log it, and remove
                // DrawnGameObjectComponent from the offending object.
            }
            DrawnGameObjectComponent comp = n.operator*()->get<DrawnGameObjectComponent>();
			
			glm::mat4 modelMat;
			modelMat = glm::translate( modelMat, glm::vec3( objXY[0], objXY[1], 0 ) );
			modelMat = glm::scale( modelMat, glm::vec3( .2, .2, .2 ) );
			
			_program.setModelTransformMatrix( glm::value_ptr( modelMat ) );
			if (comp._rendAssetID == 0){
				_program.setGlobalTintColor( 1, 1, 0, 1);
				glBindVertexArray( _cellVAO.vaoName() );
                glDrawElements(_cellVAO.mode(),
							   _cellVAO.elementCount(),
							   GL_UNSIGNED_INT,
							   nullptr );
			} else {
				_program.setGlobalTintColor( 1, 0, 0, 1);
				RenderableAsset * thisAsset =
					assetManager->assetForId( comp._rendAssetID );
				_program.setModelTransformMatrix( glm::value_ptr( modelMat ) );
				glBindVertexArray( thisAsset->vao()->vaoName() );
				glDrawElements( thisAsset->vao()->mode(), thisAsset->vao()->elementCount(), GL_UNSIGNED_INT, nullptr );
			}
		}
    }
}
