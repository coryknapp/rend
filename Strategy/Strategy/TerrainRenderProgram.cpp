//
//  TerrainRenderProgram.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/24/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "TerrainRenderProgram.h"


void TerrainRenderProgram::build(){

    setVertexShaderHead( terrainRendererProgramVertSourceTop );
    setVertexShaderBody( terrainRendererProgramVertSourceBottom );
    setFragmentShaderBody( terrainRendererProgramFragSource );
    includeUniform( "mat4", "projectionMatrix");
    includeUniform( "mat4", "viewTransformMatrix");
    includeUniform( "mat4", "modelTransformMatrix");
    includeUniform( "vec4", "globalTintColor");

    includeUniform( "vec2", "textureTransform" );
    includeUniform( "float", "textureScaleTransform" );

    includeUniform( "vec4", "lightPosition");

    includeUniform( "sampler2D", "setTextureMap");
    includeUniform( "sampler2D", "normalMap");
    includeUniform( "sampler2D", "redTex");
    includeUniform( "sampler2D", "greenTex");
    includeUniform( "sampler2D", "blueTex");

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 0;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;

    GeneratedGLSLProgram::build( menu );

}

void TerrainRenderProgram::initBindings(){
    getUniform( _projectionMatrixUniform, "projectionMatrix" );
    getUniform( _viewTransformMatrixUniform, "viewTransformMatrix" );
    getUniform( _modelTransformMatrixUniform, "modelTransformMatrix" );

    getUniform( _textureTransformUniform, "textureTransform");
    getUniform( _textureScaleTransform, "textureScaleTransform");

    getUniform( _lightPositionUniform, "lightPosition");

    getUniform( _TextureMapTexture, "textureMap" );
    getUniform( _normalMapTexture, "normalMap");
    getUniform( _redTexture, "redTex" );
    getUniform( _greenTexture, "greenTex" );
    getUniform( _blueTexture, "blueTex" );

}

void TerrainRenderProgram::setProjectionMatrix( const GLfloat * m ){
    glUniformMatrix4fv( _projectionMatrixUniform, 1, false, m);

}
void TerrainRenderProgram::setViewTransformMatrix( const GLfloat * m ){
    glUniformMatrix4fv( _viewTransformMatrixUniform, 1, false, m); //xxx

}
void TerrainRenderProgram::setModelTransformMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _modelTransformMatrixUniform, 1, false, m);

}
void TerrainRenderProgram::setTextureTransform( GLfloat s, GLfloat t ){
    glUniform2f( _textureTransformUniform, s, t);
}

void TerrainRenderProgram::setTextureScaleTransform( GLfloat s ){
    glUniform1f( _textureScaleTransform, s);
}

void TerrainRenderProgram::setLightPositionUniform( GLfloat * v ){
    glUniform4fv( _lightPositionUniform, 1, v);
}

void TerrainRenderProgram::setTextureMapTexture( GLint textureName ){
    glUniform1i( _TextureMapTexture, textureName);
}

void TerrainRenderProgram::setNormalMapTexture( GLint textureName ){
    glUniform1i( _normalMapTexture, textureName);
}

void TerrainRenderProgram::setRedTexture( GLint textureName ){
    glUniform1i( _redTexture, textureName);
}

void TerrainRenderProgram::setGreenTexture( GLint textureName ){
    glUniform1i( _greenTexture, textureName);
}

void TerrainRenderProgram::setBlueTexture( GLint textureName ){
    glUniform1i( _blueTexture, textureName);
}