//
//  TessellatedSquare.h
//  Strategy
//
//  Created by Cory Knapp on 9/23/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TessellatedSquare__
#define __Strategy__TessellatedSquare__

#include "VAO.h"

#include <iostream>

#define DEBUG_TessellatedSquareVAO_h

/*
 0----1----2----3----4----5
 |    |    |    |    |    |
 |    |    |    |    |    |
 |    |    |    |    |    |
 5----7----8----9----10---11
 |    |    |    |    |    |
 |    |    |    |    |    |
 |    |    |    |    |    |
 */

class TessellatedSquareVAO : public VAO {

public:

    TessellatedSquareVAO(){
        
    }

    void build( unsigned xDiv, unsigned yDiv, float xMin = 0, float yMin = 0, float xMax = 1, float yMax = 1);

};
#endif /* defined(__Strategy__TessellatedSquare__) */
