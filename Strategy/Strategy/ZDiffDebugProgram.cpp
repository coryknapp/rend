//
//  ZDiffDebugProgram.cpp
//  Strategy
//
//  Created by Cory Knapp on 5/18/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "ZDiffDebugProgram.h"


void ZDiffDebugProgram::build(){
    
    setVertexShaderHead( ZDiffDebugProgramVertSourceTop );
    setVertexShaderBody( ZDiffDebugProgramVertSourceBottom );
    setFragmentShaderBody( ZDiffDebugProgramFragSource );
    includeUniform( "mat4", "projectionMatrix");
    includeUniform( "mat4", "viewTransformMatrix");
    includeUniform( "mat4", "modelTransformMatrix");

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 0;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 0;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;
    
    GeneratedGLSLProgram::build( menu );
    
}

void ZDiffDebugProgram::initBindings(){
    getUniform( _projectionMatrixUniform, "projectionMatrix" );
    getUniform( _viewTransformMatrixUniform, "viewTransformMatrix" );
    getUniform( _modelTransformMatrixUniform, "modelTransformMatrix" );

}

void ZDiffDebugProgram::setProjectionMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _projectionMatrixUniform, 1, false, m);
}

void ZDiffDebugProgram::setViewTransformMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _viewTransformMatrixUniform, 1, false, m);
}

void ZDiffDebugProgram::setModelTransformMatrix(const GLfloat * m ){
    glUniformMatrix4fv( _modelTransformMatrixUniform, 1, false, m);
}