//
//  TerrainRectVAO.cpp
//  Strategy
//
//  Created by Cory Knapp on 5/19/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "TerrainRectVAO.h"


void TerrainRectVAO::build(
                           PlanarData<uint8_t> * heightMap,
                           int xPosMin, int yPosMin,
                           int xPosMax, int yPosMax ){


    int width = xPosMax - xPosMin +1;
    int height = yPosMax - yPosMin + 1;
    
     attributeMenu_t menu;
     CLEAR_ATTRIBUTE_MENU( menu );
     menu[POSITION_ATTRIBUTE_INDEX]    = 1;
     menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
     
     assert( strideForMenu(menu) == 6);
    
    std::vector< GLfloat > vaoArray;
    GLint indexes[width][height];
    
    std::cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    
     int index = 0;
     for( unsigned i = 0; i < width; i++ ){
         for( unsigned j = 0; j < height; j++ ){
             
             float posLocX = i;
             float posLocY = j;
             
             // positions
             vaoArray.push_back( posLocX );
             vaoArray.push_back( posLocY );
             vaoArray.push_back( (float)heightMap->getValue(posLocX, posLocY, 0 )/
                                 std::numeric_limits<uint8_t>::max() );
             std::cout << posLocX << ',' << posLocY << " == " << (float)heightMap->getValue(posLocX, posLocY, 0 )/
             std::numeric_limits<uint8_t>::max() << std::endl;
             vaoArray.push_back( 1 );
             
             // tex coords
             vaoArray.push_back( posLocX );
             vaoArray.push_back( posLocY );
             
             indexes[j][i] = index;
             index++;
         }
     }
     
    std::vector<GLint> elementArray;
     index = 0;
     unsigned row = 0;
     unsigned col = 0;
    elementArray.push_back( indexes[0][0] );
    
    while( row < height-1 ){
        if( row % 2 == 0 ){ // even row
            col = 0;
            while( col < width - 1 ) {
                // add south stroke
                elementArray.push_back( indexes[col][row+1] );
                // add northeast stroke
                elementArray.push_back( indexes[col+1][row] );
                col++;
            }
            // additional south stroke
            elementArray.push_back( indexes[col][row+1] );
        } else { // odd row
            col = width - 1;
            while( col > 0 ) {
                // add south stroke
                
                elementArray.push_back( indexes[col][row+1] );
                // add northeast stroke
                elementArray.push_back( indexes[col-1][row] );
                col--;
            }
            // additional south stroke
            elementArray.push_back( indexes[col][row+1] );
        }
        row++;
    }
    
    Buffer<GLfloat> vaoBuffer( vaoArray.data(), vaoArray.size(), true );
     GLuint vaoName = buildVAO( vaoBuffer, menu, width*height, elementArray.data(), (GLint)elementArray.size() );
     set( vaoName, (GLint)elementArray.size() , GL_TRIANGLE_STRIP);
    
}

void TerrainRectVAO::build(
          TerrainRegion * region,
           unsigned resolution,
           float sizeMultiplyer,
           float posX, float posY ){
    /*
    float xSliceSize = region->boxSpanX() / ( resolution );
    float ySliceSize = region->boxSpanY() / ( resolution );
    
    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX]    = 1;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
    
    assert( strideForMenu(menu) == 6);
    
    std::vector< GLfloat > vaoArray;
    GLint indexes[resolution][resolution];
    
    int index = 0;
    for( unsigned i = 0; i < resolution; i++ ){
        for( unsigned j = 0; j < resolution; j++ ){
            
            float iCut = ((float)i/resolution);
            float jCut = ((float)j/resolution);
            
            float posLocX = ( i == resolution-1 ) ? xPosMax : xPosMin + (iCut * (xPosMax - xPosMin);
            float posLocY = ( j == resolution-1 ) ? yPosMax : yPosMin + ((float)j/resolution) * (yPosMax - yPosMin);
            
            float mapLocX = ( i == resolution-1 ) ? xMapMax : xMapMin + ((float)i/resolution) * (xMapMax - xMapMin);
            float mapLocY = ( j == resolution-1 ) ? yMapMax : yMapMin + ((float)j/resolution) * (yMapMax - yMapMin);
            
            // positions
            vaoArray.push_back( posLocX );
            vaoArray.push_back( posLocY );
            vaoArray.push_back( heightMap->getWrappedInterpolationValue(mapLocX, mapLocY, 0)/256 );
            vaoArray.push_back( 1 );
            
            // tex coords
            vaoArray.push_back( posLocX );
            vaoArray.push_back( posLocY );
            
            indexes[j][i] = index;
            index++;
        }
    }
    
    std::vector<GLint> elementArray;
    index = 0;
    unsigned row = 0;
    unsigned col = 0;
    elementArray.push_back( indexes[0][0] );
    
    while( row < height-1 ){
        if( row % 2 == 0 ){ // even row
            col = 0;
            while( col < width - 1 ) {
                // add south stroke
                elementArray.push_back( indexes[col][row+1] );
                // add northeast stroke
                elementArray.push_back( indexes[col+1][row] );
                col++;
            }
            // additional south stroke
            elementArray.push_back( indexes[col][row+1] );
        } else { // odd row
            col = width - 1;
            while( col > 0 ) {
                // add south stroke
                
                elementArray.push_back( indexes[col][row+1] );
                // add northeast stroke
                elementArray.push_back( indexes[col-1][row] );
                col--;
            }
            // additional south stroke
            elementArray.push_back( indexes[col][row+1] );
        }
        row++;
    }
    
    Buffer<GLfloat> vaoBuffer( vaoArray.data(), vaoArray.size(), true );
    GLuint vaoName = buildVAO( vaoBuffer, menu, (xDiv+1)*(yDiv+1), elementArray.data(), (GLint)elementArray.size() );
    set( vaoName, (GLint)elementArray.size() , GL_TRIANGLE_STRIP);
    */
    
}