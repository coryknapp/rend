//
//  StageState.cpp
//  Strategy
//
//  Created by Cory Knapp on 6/18/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "StageState.h"

#include "PlainsRegionGenerator.h"

StageState::StageState(){
    
    // !!! set up a simple stage to test.
    //initWithSimpleDemo();
}

void StageState::initWithSimpleDemo(){
    
    // make some random waypoints
    _roadWays.reset( new WaypointNetwork );
/*    PossonDiscGenerator<
        Waypoint,
        WaypointSetFirstCoordFunctor,
        WaypointSetSecondCoordFunctor,
        WaypointGetFirstCoordFunctor,
        WaypointGetSecondCoordFunctor,
        Waypoint,
        WaypointNetworkAddFunctor,
        WaypointNetworkConnectFunctor
    >
    generator( (*_roadWays), 0, 8, 0, 8);
    while (generator.iterate()) {
    }
    */
  
	RegionBoundaryGenerator boundryGen( 5, 10 );
	PlainsRegionGenerator regionGen;
	regionGen.setRegionID( 0 );
	regionGen.setGenerator( &boundryGen );
	regionGen.setNetwork( _roadWays.get() );
    
    while (regionGen.iterate()) {
        std::cout << "generator.iterate()\n";
    }
	PlainsRegionGenerator regionGen2;
	regionGen2.setRegionID( 1 );
	regionGen2.setGenerator( &boundryGen );
	regionGen2.setNetwork( _roadWays.get() );
    while (regionGen2.iterate()) {
        std::cout << "generator.iterate()\n";
    }
	regionGen.connectNodesInternally();
	
    // get the RenderableAssetManager so we can assign the
	// DrawnGameObjectComponent

    RenderableAssetManager * assetManager =
		getSingletonPtr<RenderableAssetManager>();
    
    // make some trees
    // create
    /*GameObjectID newGOId = _gameObjects.create();
    GameObjectRef newOnj = _gameObjects.object(newGOId);
    
    // install and configure draw component
    newOnj->install<DrawnGameObjectComponent>();
    newOnj->get<DrawnGameObjectComponent>()._rendAssetID =
        assetManager->IdforName( "pine_tree.obj" );
    
    // install and configure CoordBasedWorldGameObjectComponent
    newOnj->install<CoordBasedWorldGameObjectComponent>();
    newOnj->get<CoordBasedWorldGameObjectComponent>().i = 1.0;
    newOnj->get<CoordBasedWorldGameObjectComponent>().j = 1.0;
    
    // create a second tree
    newGOId = _gameObjects.create();
    newOnj = _gameObjects.object(newGOId);
    
    // install and configure draw component
    newOnj->install<DrawnGameObjectComponent>();
    newOnj->get<DrawnGameObjectComponent>()._rendAssetID =
    assetManager->IdforName( "pine_tree.obj" );
    
	std::cout << assetManager->IdforName( "pine_tree.obj" )
	<< " - " << newOnj->get<DrawnGameObjectComponent>()._rendAssetID <<
	" ===========================================\n";
	
    // install and configure CoordBasedWorldGameObjectComponent
    newOnj->install<CoordBasedWorldGameObjectComponent>();
    newOnj->get<CoordBasedWorldGameObjectComponent>().i = 1.0;
    newOnj->get<CoordBasedWorldGameObjectComponent>().j = -1.4;*/
}