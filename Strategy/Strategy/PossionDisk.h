//
//  PossionDisk.h
//  Strategy
//
//  Created by Cory Knapp on 6/28/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__PossionDisk__
#define __Strategy__PossionDisk__

#include <memory>
#include <deque>
#include <stdlib.h> // srand, rand (we should have our own project-wide rand stuff somewhere else?
                    // for uniformity and unit testing
#include <ctime>
#include "NodeNetwork.h"
#include "RegionGeneratorAbstract.h"
#include "Waypoint.h"

class PlainsRegionGenerator;

template<
    typename T,
    typename firstSetter,
    typename secondSetter,
    typename firstGetter,
    typename secondGetter,
    typename Network,
    typename addNodeToNetworkFunctor
>
class PossonDiscGenerator {

public:
	
	PossonDiscGenerator(){
		assert( false );
	}
    PossonDiscGenerator(Network &network,
                        RegionGeneratorAbstract * regionGen) :
    _network( network ),
	_regionGen( regionGen )
	{
        //srand( time(nullptr) );
        srand( 0 );
        
        // generate first point
        T first;
		auto startPoint = _regionGen->center();
        _firstSetter( first, startPoint.first );
        _secondSetter( first, startPoint.second);
        _openPoints.push_back(first);
    }
    
    bool iterate(){
		if( _network.size() > _maxNodeSanityCheck )
			return  false;
		
        //get an random open point, and return false if we can't
        if( _openPoints.empty() )
            return false;
        std::swap( _openPoints[rand()%_openPoints.size()] , _openPoints.back() );
        auto &openP = _openPoints.back();

        
        //look for candidates
        for (int i = 0; i < _candidateAttemptsLimit; i++) {
            T candidate = _candidate( openP );
            //we'll check the candidate against every point on both lists,
            //This is a likely place to optomize in the future.
            for( auto iter = _openPoints.begin(); iter != _openPoints.end(); iter++ ){
                if( _inRange( *iter, candidate ) ){
                    goto retry;
                }
            }
            for( auto iter = _network.begin(); iter != _network.end(); ++iter ){
                if( _inRange( **iter, candidate ) ){
                    goto retry;
                }
            }
            // we got through both loops without rejecting the point, so it's good.
            _openPoints.push_back( candidate );
            return true;
        retry:
            0; // can't have a label at the end of a block, i guess.
        }
        
        // couldn't find a point, so we'll close this.
        _addNodeToNetworkFunctor( _network, _openPoints.back() );
        _openPoints.pop_back();
		
        return true; // return true, there may still be points on the open list.
        
    }
    
private:
    
    const float _annulus = .5; // The radius of the inside of the annulus.
                                // The outside is assumed to be twice this.
                                // !!! should be adjustable.
    const int _candidateAttemptsLimit = 32;
    
	const int _maxNodeSanityCheck = 200;
	
    Network &_network;
    RegionGeneratorAbstract * _regionGen;
    
    firstSetter _firstSetter;
    secondSetter _secondSetter;
    firstGetter _firstGetter;
    secondGetter _secondGetter;
    addNodeToNetworkFunctor _addNodeToNetworkFunctor;
    
	
    std::vector<T> _openPoints;
    
    T _candidate( const T &p ){
        float radius;
        float angle;
        T candidate;
        do {
            radius = rand()/(float)RAND_MAX*(_annulus) + _annulus;
            angle = rand()/(float)RAND_MAX*(M_PI*2);
            _firstSetter(   candidate, _firstGetter(p) + radius * cosf( angle ) );
            _secondSetter(  candidate, _secondGetter(p) + radius * sinf( angle ) );
        } while(!_inBounds(candidate));
		candidate.region = _regionGen->rId();
        return candidate;
    }
    
    bool _inRange( const T &a, const T &b ){

        return  ( (_firstGetter(a) - _firstGetter(b)) * (_firstGetter(a) - _firstGetter(b)) ) +
                ( (_secondGetter(a) - _secondGetter(b)) * (_secondGetter(a) - _secondGetter(b)) ) < _annulus * _annulus ? true : false;
    }
    
    bool _inBounds( const T &p){
		_regionGen->inBounds( _firstGetter(p), _secondGetter(p) ) << '\n';
		return _regionGen->inBounds( _firstGetter(p), _secondGetter(p) );
    }

};

#endif /* defined(__Strategy__PossionDisk__) */
