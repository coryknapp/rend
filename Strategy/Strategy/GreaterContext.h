//
//  GreaterContext.h
//  Strategy
//
//  Created by Cory Knapp on 3/19/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__GreaterContext__
#define __Strategy__GreaterContext__

#include <iostream>
#include <memory>
#include "SimTypesAndConstants.h"
#include "Camera.h"
#include "WorldCamera.h"
#include "StageState.h"

#include "FlyCameraInputInterpreter.h"

class WorldCamera;

class GreaterContext {

    // time keeping
    SimTime _lastTime;

    // subsystems
    std::unique_ptr< WorldCamera > _userCamera;
    std::unique_ptr< FlyCameraInputInterpreter > _inputInterpreter;
    
    //sun tracking
    float _sunPosition[4]; // includes w value.
    StageState _currentStage;
	
public:

	StageState& currentStage(){
		return _currentStage;
	}
	
    GreaterContext( const SimTime& time );
    void updateContext( const SimTime& time );

    WorldCamera * userCamera() const; // ??? why is this called userCamera() instead of camera()?
    UserInputInterpreter * inputInterpreter();
    
    float * sunPosition();


};


#endif /* defined(__Strategy__GreaterContext__) */
