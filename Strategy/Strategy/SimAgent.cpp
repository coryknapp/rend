//
//  SimAgent.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/2/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "SimAgent.h"

SimAgent::SimAgent(){
}

// constructors and assignments
SimAgent::SimAgent( const SimAgent &other ){ // copy constructor
    assert( false );
    memcpy( _lastlocationCache, other._lastlocationCache, sizeof( float ) * 3 );
}

SimAgent& SimAgent::operator = (const SimAgent & other){ // copy assignment

    assert( false );
    memcpy( _lastlocationCache, other._lastlocationCache, sizeof( float ) * 3 );
    return *this;
}

SimAgent::SimAgent( SimAgent &&other ){ // move constructor

    assert( false );
    memcpy( _lastlocationCache, other._lastlocationCache, sizeof( float ) * 3 );
}

SimAgent& SimAgent::operator = (SimAgent&& other) //  move assignment
{
    assert( false );
    memcpy( _lastlocationCache, other._lastlocationCache, sizeof( float ) * 3 );
    return *this;
}


void SimAgent::location(float * location){
    memcpy( location, _lastlocationCache, sizeof(float)*3 );
}

void SimAgent::setMovementBehavior( SimMovementBehavior * mb ){
#ifdef DEBUG_SimAgent_h
    assert( nullptr != mb );
#endif
    if( movementBehavior ){
        delete movementBehavior;
        movementBehavior = nullptr;
    }
    movementBehavior = mb;
}

void SimAgent::step( const SimTime &currentTime ){
    if ( movementBehavior ) {

        movementBehavior -> positionAtTime( _lastlocationCache, currentTime);
        if( movementBehavior -> moveCompleted() )
            movementBehaviorCompleted( currentTime );
    }
}

void SimAgent::movementBehaviorCompleted( const SimTime &currentTime ){
#ifdef DEBUG_SimAgent_h
    assert( nullptr != movementBehavior );
#endif
    
    delete movementBehavior;
    movementBehavior = nullptr;
    float jumpTo[3] = { randInRange( -10, 10 ), randInRange( -10, 10 ), randInRange( -10, 10 ) };
    movementBehavior = new SimJumpMovementBehavior( currentTime, _lastlocationCache, currentTime + SimTimeSecond, jumpTo );
}
