//
//  SimState.h
//  Strategy
//
//  Created by Cory Knapp on 9/2/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__SimState__
#define __Strategy__SimState__

#include <iostream>
#include <vector>

#include "SimTypesAndConstants.h"
#include "SimAgent.h"
#include "SimTile.h"

//#include "HexGrid.h"
#include "FBOLinkedPlanarData.h"


/*
 
What do we need to simulate?
 
    Weather
        Cloads
        Persipitation
        Sea levels
        Rivers
    Wild life
        Flora
        Herds of Fauna
 
    Cultures...
 
 */
/*
typedef PlanarData<float> HeatMap;

std::unique_ptr<class SimState> SimStateFactory(
                                              size_t grid_i,
                                              size_t grid_j,
                                              HeatMap * heightMap,
                                              HeatMap * normalMap,
                                              HeatMap * temperatureMap,
                                              HeatMap * precipitationMap
                                              );

class SimState {

    SimTime _simStartTime;

    SimTime _lastTime;
    std::vector<SimAgent*> _agentVec;
    float _sunPosition[4] = {0,0,0,1};

    HeatMap * _heightMap = nullptr;
    HeatMap * _normalMap = nullptr;
    HeatMap * _temperatureMap = nullptr;
    HeatMap * _precipitationMap = nullptr;

    HexGrid<SimTile> _grid;

public:

    SimState( size_t grid_i, size_t grid_j);
    ~SimState();

    void setHeightMap( HeatMap * map);
    void setNormalMap( HeatMap * map);
    void setTemperatureMap( HeatMap * map);
    void setPrecipitationMap( HeatMap * map);

    HeatMap * heightMap();
    HeatMap * normalMap();
    HeatMap * temperatureMap();
    HeatMap * precipitationMap();

    void calculateTileScores( size_t minI, size_t maxI, size_t minJ, size_t maxJ);

    void step( const SimTime &currentTime );
    float * sunPosition();

    auto agentVectorStart() ->typeof(_agentVec.begin()){
        return _agentVec.begin();
    }

    auto agentVectorEnd() ->typeof(_agentVec.end()){
        return _agentVec.end();
    }

    auto grid() ->typeof(&_grid){ // we need to vend the grid to the renderer.  Can we encaposlate this better?
        return &_grid;
    }

};
*/
#endif /* defined(__Strategy__SimState__) */
