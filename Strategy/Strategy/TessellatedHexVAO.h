//
//  TessellatedHexVAO.h
//  Strategy
//
//  Created by Cory Knapp on 10/8/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TessellatedHexVAO__
#define __Strategy__TessellatedHexVAO__

#include <iostream>
#include <vector>

#include "VAO.h"
#include "HexGrid.h"

#define DEBUG_TessellatedHexVAO_h

class TessellatedHexVAO : public VAO {

    int _vertexArrayWriteHead = 0;
    float * _vertexArray;
    size_t _vertexStride = 0;

    void _addSubdividedTriangle( const float *v1,
                                                   const float *v2,
                                                   const float *v3,
                                const int &depth );
    void _fillArrayWithVertex( const float *vertex );

public:

    TessellatedHexVAO(){

    }

    void build( int depth );

#ifdef DEBUG_TessellatedHexVAO_h
    int finalTriangleCount = 0;
#endif

};


#endif /* defined(__Strategy__TessellatedHexVAO__) */
