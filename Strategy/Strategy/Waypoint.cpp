//
//  Waypoint.cpp
//  Strategy
//
//  Created by Cory Knapp on 6/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "Waypoint.h"

std::set<Waypoint*> closestWaypointsTo(Waypoint &wp,
										  WaypointNetwork &wn,
										  unsigned const &maxVecSize){
	std::set<Waypoint*> retVal;
	for(auto e = wn.begin();
		e != wn.end();
		e++){
		insertWithLimit(retVal, *e, maxVecSize);
	}
	return retVal;
}

// this could easily be templated if nessisary !!!
bool crossExistingEdge( WaypointNetwork &net, Waypoint &a, Waypoint &b ){
	// if a and b were to be connected, would the edge cross another edge
    // all parameters should be const
    // !!! we're gonna have to figure out how to make const iterators
    for(auto outer = net.begin();
        outer != net.end();
        ++outer){
        for(auto inner = net.beginingOfNodesConnectedTo( *outer);
            inner != net.endingOfNodesConnectedTo( *outer );
            ++inner){
            if( doSegmentsIntersect(outer->mapPosition[0],
									outer->mapPosition[1],
									inner->mapPosition[0],
									inner->mapPosition[1],
									a.mapPosition[0], a.mapPosition[1],
									b.mapPosition[0], b.mapPosition[1]) )
                return true;
        }
    }
    return false;
}

float smallestDistanceToExistingEdge(WaypointNetwork &net,
									 Waypoint &a){
	// if a and b were to be connected, would the edge cross another edge
    // all parameters should be const
    // !!! we're gonna have to figure out how to make const iterators
	// ??? we could add a optional parameter to tell the function to just return
	// when we find a suffiently small value as a easy optimization.
	float smallestD = std::numeric_limits<float>::max();
    float currentD = std::numeric_limits<float>::max();
	for(auto outer = net.begin();
        outer != net.end();
        ++outer){
        for(auto inner = net.beginingOfNodesConnectedTo( *outer);
            inner != net.endingOfNodesConnectedTo( *outer );
            ++inner){
            currentD = distanceFromPointToSegment(outer->mapPosition,
												  inner->mapPosition,
												  a.mapPosition);
			if( currentD < smallestD )
				smallestD = currentD;
        }
    }
    return smallestD;
}