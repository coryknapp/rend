//
//  WorldRenderProgram.h
//  Strategy
//
//  Created by Cory Knapp on 9/3/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__WorldRenderProgram__
#define __Strategy__WorldRenderProgram__

#include <iostream>

#include "GLSLProgram.h"
#include "GeneratedGLSLProgram.h" // for attributeCodeForMenu(...)

class WorldRendererProgram : public GLSLProgram {

    // uniform locations

    GLint _projectionMatrixUniform;
    GLint _viewTransformMatrixUniform;
    GLint _modelTransformMatrixUniform;
    
    GLint _globalTintColor;

public:

    void build();

    void initBindings();

    void setProjectionMatrix(const GLfloat * m );
    void setViewTransformMatrix(const GLfloat * m );
    void setModelTransformMatrix(const GLfloat * m );

    void setGlobalTintColor( GLfloat r, GLfloat g, GLfloat b, GLfloat a );
    
};

const char * const worldRendererProgramVertSourceTop =
"#ifdef GL_ES                            \n"
"precision highp float;                  \n"
"#endif                                  \n"

"uniform mat4 projectionMatrix;          \n"
"uniform mat4 viewTransformMatrix;       \n"
"uniform mat4 modelTransformMatrix;      \n"
"uniform vec4 globalTintColor;           \n"
"out vec4 vertTintColor;                 \n";

// attributes will be inserted here

const char * const worldRendererProgramVertSourceBottom =
"void main (void)"
"{"
    "vec4 adjustedPos = POSITION_ATTRIBUTE_INDEX_0;"
    "mat4 mv = viewTransformMatrix * modelTransformMatrix;"
    "adjustedPos = mv * adjustedPos;"
    "adjustedPos = adjustedPos.yzxw;"
    "gl_Position	= (projectionMatrix * adjustedPos);"
    "vec4 lightDir = normalize( vec4( 0, 0, 5,  0 ) );"
	"lightDir = lightDir.yzxw;"
    "lightDir *= projectionMatrix * mv;"
    "float lightLevel = dot( vec3( lightDir ), NORMAL_ATTRIBUTE_INDEX_0 );"
    "vertTintColor = globalTintColor;"// * lightLevel;"
"}";

const char * const worldRendererProgramFragSource =
"#ifdef GL_ES                \n"
"precision highp float;      \n"
"#endif                      \n"

"in vec4 vertTintColor;"
"uniform sampler2D inTexture;"
"out vec4 fragColor;"

"void main (void)"
"{"
"gl_FragDepth = gl_FragCoord.z;"
"fragColor = vertTintColor;"
"fragColor.a = 1.0;"
"}";


#endif /* defined(__Strategy__WorldRenderProgram__) */
