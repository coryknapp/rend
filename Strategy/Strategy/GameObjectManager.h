//
//  GameObjectManager.h
//
//  Created by Cory Knapp on 6/17/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __GameObjectManager__
#define __GameObjectManager__

#include <iostream>
#include <map>
#include <memory>

#include "GameObject.h"
#include "ThereCanOnlyBeOne.h"

// !!! this is nearly identical to the NetworkNodeIterator, differing only
// in regard to a few times.
class GameObjectIterator{
    
    typedef typename std::map<GameObjectID, std::unique_ptr< GameObject >>::iterator internalIteratorType;
    
public:
    internalIteratorType _internalIterator;
    
    GameObjectIterator( const internalIteratorType & itt ) :
    _internalIterator(itt){}
    
    bool operator==(GameObjectIterator const& rhs) const {
        return (_internalIterator==rhs._internalIterator);
    }
    
    bool operator!=(GameObjectIterator const& rhs) const {
        return !(*this==rhs);
    }
    
    GameObject * operator++() {
        _internalIterator++;
        return _internalIterator->second.get();
    }
    
    GameObject * operator* () const {
        return _internalIterator->second.get();
    }

};

typedef GameObject*	GameObjectRef;
class GameObjectManager;

class GameObjectManager : public ThereCanOnlyBeOne<GameObjectManager>{
    
    std::map< GameObjectID, std::unique_ptr<GameObject> > _objectMap;
    GameObjectID _lastAssignedId = 0;
public:
    
    GameObjectID create(){
        _lastAssignedId++;
        _objectMap[_lastAssignedId].reset( new GameObject );
		_objectMap[_lastAssignedId]->_id = _lastAssignedId;
        return _lastAssignedId;
    }
    
    void destroy( GameObjectID gOId ){
        _objectMap[gOId].reset();
    }
    
	bool exists( GameObjectID gOId ){
		return (_objectMap.find(gOId) != _objectMap.end());
	}
	
	GameObjectRef object( GameObjectID gOId ){
        return _objectMap[gOId].get();	//if you get a bad access here
									//then there is no object for
									//gOId.
    }
    
    GameObjectIterator begin(){
        return GameObjectIterator( _objectMap.begin() );
    };
    
    GameObjectIterator end(){
        return GameObjectIterator( _objectMap.end() );
    };
};

#endif /* defined(__Strategy__GameObjectManager__) */
