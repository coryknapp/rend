//
//  OceanRenderer.h
//  Strategy
//
//  Created by Cory Knapp on 2/9/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__OceanRenderer__
#define __Strategy__OceanRenderer__

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "ThreeDRenderer.h"
#include "GeneratedGLSLProgram.h"
#include "AssimpVAO.h"
#include "TessellatedHexVAO.h"

#include "WorldCamera.h"

#include "NormalMapRenderer.h"
#include "ImageFilePlanarData.h"

#include "TerrainRenderProgram.h"
#include "TerrainHexVAOGenerator.h"

#include "OceanRenderProgram.h"
#include "OceanVAO.h"

#include "GreaterContext.h"

class OceanRenderer : public ThreeDRenderer, public GreaterContextAware {

    OceanRenderProgram _program;

    Texture _noiseTex;
    Texture _noiseNormalTex;

    TessellatedSquareVAO _oceanSurfaceVAO;
    //Texture _waterTexture;
    // use _blueTex for now.
    const float _oceanLevel = .33;
    void _initOceanRendering();


public:
    OceanRenderer( GLint fbo ) :
    ThreeDRenderer( fbo )
    {

    };
    using ThreeDRenderer::resize;

    void initialize();
    void render();

    void buildHexVAOInRange( size_t minI, size_t maxI, size_t minJ, size_t maxJ );
};


#endif /* defined(__Strategy__OceanRenderer__) */
