//
//  RenderableAsset.h
//  Strategy
//
//  Created by Cory Knapp on 6/20/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__RenderableAsset__
#define __Strategy__RenderableAsset__

#include <iostream>
#include <map>
#include <memory>
#include <vector>
#include <boost/serialization/singleton.hpp>

#include "Resources.h"
#include "AssimpVAO.h"
#include "ThereCanOnlyBeOne.h"
#include "Singleton.h"
typedef uint64_t RendAssetID;


// Maintains ownership of the VAO, and indicates what program should render it
// or at least what it needs to know to draw, i.e what to set uniforms to.
class RenderableAsset : public boost::noncopyable{
    
    RendAssetID _id = 0;
    std::unique_ptr<AssimpVAO> _vao;
    
public:
    
    RenderableAsset(const RendAssetID &assignedId,
                    const std::string &modelName );
    
    const VAO * vao(){
        return _vao.get();
    }
    
};

class RenderableAssetManager;

class RenderableAssetManager :
	public boost::noncopyable,
	public ThereCanOnlyBeOne<RenderableAssetManager>,
	public Singleton<RenderableAssetManager>{
    
    std::map< std::string, RendAssetID > _idsByName;
    std::map< RendAssetID, std::shared_ptr<RenderableAsset> >_assetsById; //!!! why a shared_ptr?

    RendAssetID _lastAsignedId = 0;
    
    void _loadAssetWithName( const std::string &name );
    
public:
    
    RendAssetID IdforName( const std::string &name );
    RenderableAsset * assetForId( const RendAssetID &id );
    
};

#endif /* defined(__Strategy__RenderableAsset__) */
