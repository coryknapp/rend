//
//  TerrainRectVAO.h
//  Strategy
//
//  Created by Cory Knapp on 5/19/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__TerrainRectVAO__
#define __Strategy__TerrainRectVAO__

#include "VAO.h"
#include "PlanarData.h"

#include <iostream>
#include <vector>

class TerrainRegion;

class TerrainRectVAO : public VAO {
    
public:
    
    TerrainRectVAO(){
    }
    
    void build(
          PlanarData<uint8_t> * heightMap,
          int xPosMin, int yPosMin,
               int xPosMax, int yPosMax );

    void build(
               TerrainRegion * region,
               unsigned resolution,
               float sizeMultiplyer,
               float posX, float posY );
    
};

#endif