//
//  MainRenderer.h
//  Strategy
//
//  Created by Cory Knapp on 9/1/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__MainRenderer__
#define __Strategy__MainRenderer__

#include <iostream>
#include "GreaterContext.h"
#include "TextureBlendingRenderer.h"
#include "WorldRenderer.h"
#include "OceanRenderer.h"
#include "BackgroundRenderer.h"
#include "Resources.h"

enum {
    regular_fbo_index = 0,
    ocean_fbo_index,
    background_fbo_index,
    render_layer_count
};


class MainBlenderProgram : public BlenderGLSLProgram {
public:
    void build(){
        BlenderGLSLProgram::build("vec4 terrainColor = texture(SAMPLE_0, varTexcoord.st, 0 );"
                                  "vec4 oceanColor = texture(SAMPLE_1, varTexcoord.st, 0 );"
                                  "vec4 backgroundColor = texture(SAMPLE_2, varTexcoord.st, 0 );"
                                  "vec4 featuresColor = mix( terrainColor, oceanColor, oceanColor.a );"
                                  "featuresColor.a = max(oceanColor.a,terrainColor.a);"
                                  "fragColor = mix( backgroundColor, featuresColor, featuresColor.a );"
                                  //"fragColor = oceanColor;"


                                  , render_layer_count);
    }
    void setTexture( GLint texture ){
        BlenderGLSLProgram::setSampleName( 0, texture);
    }
    
};

class MainRenderer : public TextureBlendingRenderer, public GreaterContextAware{

    WorldRenderer * _worldRenderer = 0;
    OceanRenderer * _oceanRenderer = 0;
    FlyCamera _safeCamera;
    
    BackgroundRenderer * _backgroundRenderer = 0;
    Texture _background;

public:

    MainRenderer( GLint fbo )
    :   TextureBlendingRenderer( fbo, render_layer_count ){ // two, one for the world renderer and one for the background;
    }

    // required of TextureBlendingRenderer sublasses;
    void prepareTexturesForRender();
    void initProgram();

    void resize(GLuint width, GLuint height);

    void setGreaterContext( GreaterContext * gc ){ // to hook up to system spesific controls
        GreaterContextAware::setGreaterContext( gc );
        _worldRenderer->setGreaterContext( gc );
        _oceanRenderer->setGreaterContext( gc );
        _backgroundRenderer->setGreaterContext( gc );


        greaterContext()->userCamera()->setViewSize( viewWidth(), viewHeight() );
    }
};

#endif /* defined(__Strategy__MainRenderer__) */
