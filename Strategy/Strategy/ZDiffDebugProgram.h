//
//  ZDiffDebugProgram.h
//
//  Created by Cory Knapp on 5/18/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__ZDiffDebugProgram__
#define __Strategy__ZDiffDebugProgram__


#include <iostream>

#include "GLSLProgram.h"
#include "GeneratedGLSLProgram.h" // for attributeCodeForMenu(...)

class ZDiffDebugProgram : public GeneratedGLSLProgram {
    
    // uniform locations
    
    GLint _projectionMatrixUniform;
    GLint _viewTransformMatrixUniform;
    GLint _modelTransformMatrixUniform;
    
public:
    
    void build();
    
    void initBindings();
    
    void setProjectionMatrix(const GLfloat * m );
    void setViewTransformMatrix(const GLfloat * m );
    void setModelTransformMatrix(const GLfloat * m );
};

const char * const ZDiffDebugProgramVertSourceTop = "    \
#ifdef GL_ES                            \n\
precision highp float;                  \n\
#endif                                  \n"
"out vec4 sampleColor;";

// attributes will be inserted here

const char * const ZDiffDebugProgramVertSourceBottom =
"void main (void)"
"{"
"vec4 adjustedPos = POSITION_ATTRIBUTE_INDEX_0;"
"mat4 mv = viewTransformMatrix * modelTransformMatrix;"
"adjustedPos = mv * adjustedPos;"
"adjustedPos = adjustedPos.yzxw;"

"sampleColor = vec4( POSITION_ATTRIBUTE_INDEX_0.z, 0, 1-POSITION_ATTRIBUTE_INDEX_0.z,1);"

"gl_Position	= (projectionMatrix * adjustedPos);"

"}";

const char * const ZDiffDebugProgramFragSource = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif  \n"

"in vec4 sampleColor;"
"out vec4 fragColor;"
"void main (void)                    \
{                                   \
gl_FragDepth = gl_FragCoord.z;"
"fragColor = sampleColor;"
"}";

#endif /* defined(__Strategy__ZDiffDebugProgram__) */
