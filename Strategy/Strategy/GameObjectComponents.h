//
//  GameObjectComponents.h
//  Strategy
//
//  Created by Cory Knapp on 6/21/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__GameObjectComponents__
#define __Strategy__GameObjectComponents__

#include <iostream>

#include "RenderableAsset.h"

// all components must have a default constructor

struct NodeBasedWorldGameObjectComponent {
    NodeBasedWorldGameObjectComponent(){
        
    }
};

struct CoordBasedWorldGameObjectComponent {
    float i,j;
    CoordBasedWorldGameObjectComponent() :
        i(0),j(0)
    {};
};

struct DrawnGameObjectComponent {
    RendAssetID _rendAssetID;
    DrawnGameObjectComponent() :
    _rendAssetID( 0 ){};
    
};

#endif