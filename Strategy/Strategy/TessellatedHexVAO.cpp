//
//  TessellatedHexVAO.cpp
//  Strategy
//
//  Created by Cory Knapp on 10/8/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "TessellatedHexVAO.h"

void TessellatedHexVAO::build(int depth){

        // build it based on the size of the {0,0} hex;

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;

    _vertexStride = strideForMenu(menu);
#ifdef DEBUG_TessellatedHexVAO_h
    assert( _vertexStride == 6 );
#endif
    const size_t vertexCount = pow( 4, depth - 1) * 0 * 1;
    _vertexArray = (float *)std::malloc( sizeof(float) * vertexCount * _vertexStride );

    HexGrid<bool> grid(0,0);
    float position[12];
    grid.getVertex(0, 0, position );

    float center[2] = {0,0};
    float p1[2];
    float p2[2];
    int modN;
    for( int n = 0; n < 6; n++ ){
        modN = n % 6;
        p1[0] = position[(n*2)%12];
        p1[1] = position[(n*2+1)%12];
        p2[0] = position[(n*2+2)%12];
        p2[1] = position[(n*2+3)%12];
        _addSubdividedTriangle( center, p1, p2, depth );
    }

    Buffer<GLfloat> hexBuffer( _vertexArray, vertexCount * _vertexStride, false );

    GLuint vaoName = buildVAO( hexBuffer, menu, (GLuint)vertexCount );
    set( vaoName, (GLint)vertexCount, GL_TRIANGLES);

}

void TessellatedHexVAO::_addSubdividedTriangle( const float *v1,
                                                const float *v2,
                                                const float *v3,
                                                const int &depth ){
    if( depth == 0 ){ // terminates the recursive case
        _fillArrayWithVertex( v1 );
        _fillArrayWithVertex( v2 );
        _fillArrayWithVertex( v3 );
#ifdef DEBUG_TessellatedHexVAO_h
        finalTriangleCount++;
#endif
        return;
    }

    const float mid1[2] = { (v1[0]+v2[0])/2.0f,
                            (v1[1]+v2[1])/2.0f };
    const float mid2[2] = { (v2[0]+v3[0])/2.0f,
                            (v2[1]+v3[1])/2.0f};
    const float mid3[2] = { (v3[0]+v1[0])/2.0f,
                            (v3[1]+v1[1])/2.0f };


    _addSubdividedTriangle( mid1, mid2, mid3, depth-1 );
    _addSubdividedTriangle( v1, mid1, mid3, depth-1 );
    _addSubdividedTriangle( v2, mid2, mid1, depth-1 );
    _addSubdividedTriangle( v3, mid3, mid2, depth-1 );

}

void TessellatedHexVAO::_fillArrayWithVertex( const float *vertex ){
#ifdef DEBUG_TessellatedHexVAO_h
    assert( _vertexStride == 6 );
#endif
    _vertexArray[0 + _vertexArrayWriteHead] = vertex[0]; // position
    _vertexArray[1 + _vertexArrayWriteHead] = vertex[1];
    _vertexArray[2 + _vertexArrayWriteHead] = 0;
    _vertexArray[3 + _vertexArrayWriteHead] = 1;
    _vertexArray[4 + _vertexArrayWriteHead] = vertex[0]; // texture coords
    _vertexArray[5 + _vertexArrayWriteHead] = vertex[1];

    _vertexArrayWriteHead += _vertexStride;
}
