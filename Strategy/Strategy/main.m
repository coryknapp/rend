//
//  main.m
//  Strategy
//
//  Created by Cory Knapp on 8/31/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
