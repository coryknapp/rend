//
//  StageState.h
//  Strategy
//
//  Created by Cory Knapp on 6/18/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__StageState__
#define __Strategy__StageState__

#include <iostream>
#include <memory>
#include <algorithm>

#include "Waypoint.h"
#include "GameObjectManager.h"
#include "GameObjectComponents.h"   
#include "PossionDisk.h"
#include "TreeLayoutGenerator.h"

#include "Geometry.h"

class StageState {
    
public:
    
    StageState();
    
    void initWithSimpleDemo();
    
    //!!! some stuff here should not be public.
    std::unique_ptr< WaypointNetwork > _roadWays;
    GameObjectManager _gameObjects;
    
};

#endif /* defined(__Strategy__StageState__) */
