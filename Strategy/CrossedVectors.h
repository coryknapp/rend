//
//  CrossedVectors.h
//  Strategy
//
//  Created by Cory Knapp on 7/12/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__CrossedVectors__
#define __Strategy__CrossedVectors__

#include <iostream>
#include <math.h>

// bad name for this file
// don't know quite where to put it yet

bool doLinesIntersect(float pai, float paj, float pbi, float pbj,
                      float qai, float qaj, float qbi, float qbj);

#endif /* defined(__Strategy__CrossedVectors__) */
