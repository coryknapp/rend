/*

*/

#import <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import "Renderer.h"
#import "imageUtil.h"

@interface RendGLView : NSOpenGLView {
	CVDisplayLinkRef displayLink;
    //id _controller;
}

-(void)setRenderer:(Renderer*) renderer;
-(Renderer*) renderer;

@end
