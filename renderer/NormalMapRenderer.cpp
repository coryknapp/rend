//
//  NormalMapRenderer.cpp
//  Strategy
//
//  Created by Cory Knapp on 10/13/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "NormalMapRenderer.h"

void convertFromColorSpaceToVectorSpace( float * fourFloats ){
    // [0,1] --> [-1,1]
    for( int n = 0; n < 4; n++ )
        fourFloats[n] = (fourFloats[n] * 2) - 1;
}

void convertFromVectorSpaceToColorSpace( float * fourFloats ){
    // [-1,1] --> [0,1]
    for( int n = 0; n < 4; n++ )
        fourFloats[n] = (fourFloats[n] + 1)/2.0;
}

void normalMapTextureForHeightMap( FrameBufferObjectCollection &heightMapFBO, unsigned height, unsigned width, const Texture &heightMap ){

    assert( height > 0 );
    assert( width > 0 );
    assert( heightMapFBO.collectionSize() > 0 );

    heightMapFBO.bind(0);

    NormalMapRenderer renderer( heightMapFBO.fboName(0) );
    renderer.resize( height, width);
    renderer.initialize();
    renderer.setTexture( heightMap );
    renderer.declareReadyToRender();

    renderer.render();
    
}

////////////////////////////////////////////////////////////////////////////
//  NormalMapProgram  //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void NormalMapProgram::build(){

    setVertexShaderHead( normalMapProgramVertSourceTop );
    setVertexShaderBody( normalMapProgramVertSourceBottom );
    setFragmentShaderBody( normalMapProgramFragSource );

    includeUniform( "sampler2D", "heightMap");

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[NORMAL_ATTRIBUTE_INDEX] = 0;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX] = 0;

    GeneratedGLSLProgram::build( menu );

}

void NormalMapProgram::initBindings(){
    getUniform( _textureName, "heightMap");
}

void NormalMapProgram::setTexture( GLint textureName ){
    glUniform1i( _textureName, textureName);
}

////////////////////////////////////////////////////////////////////////////
//  NormalMapRenderer  /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void NormalMapRenderer::setTexture( const Texture& tex ){
    _tex = tex;
}


NormalMapRenderer::NormalMapRenderer(GLuint defaultFBOName)
:Renderer( defaultFBOName )
{
    GetGLError();
}

void NormalMapRenderer::_renderFullFrameQuad(){
    GetGLError();

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    GetGLError();
    glBindVertexArray( _fullscreenQuadVAO.vaoName() );
    glDrawElements( _fullscreenQuadVAO.mode(), _fullscreenQuadVAO.elementCount(), GL_UNSIGNED_INT, 0 );
    GetGLError();

}

void NormalMapRenderer::render(){

    // clear the fbo we're supposed to be drawing on
    glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    glClearColor( 1,0,0,0 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    GetGLError();

    _program.use();
    glActiveTexture(GL_TEXTURE0 + _tex.textureName() );
    glBindTexture(GL_TEXTURE_2D, _tex.textureName() );
    _program.setTexture( _tex.textureName() );
    GetGLError();
    //printf( "texture name = %d", _tex.textureName() );
    _renderFullFrameQuad();

    GetGLError();
}


void NormalMapRenderer::resize(GLuint width, GLuint height){
    Renderer::resize( width, height);
}

void NormalMapRenderer::initialize(){
    _program.build();
    //_program.initBindings();  // called from .build();
    _fullscreenQuadVAO = createFullFrameQuadVAO();
    Renderer::initialize();
}