//
//  ThreeDRenderer.cpp
//  noise
//
//  Created by Cory Knapp on 8/19/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "ThreeDRenderer.h"

ThreeDRenderer::ThreeDRenderer(GLuint defaultFBOName)
    :Renderer( defaultFBOName )
{
    setCamera( 0 );
}

ThreeDRenderer::~ThreeDRenderer(){
    //xxx
}

void ThreeDRenderer::setCamera( Camera * camera ){
    _camera = camera;
    if( _camera )
        _camera -> setViewSize( viewWidth(), viewHeight() );
}

Camera * ThreeDRenderer::camera(){
    return _camera;
}

void ThreeDRenderer::render(){

    if( !readyToRender() )
        return;

        glFlush();
    //if( _camera )
            //            _mvpMatrix = _camera -> transformationMatrix();
    GetGLError();

    // glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    //   glClearColor( FRAMEBUFFER_CLEAR_COLOR );
    //glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        GetGLError();
}

void ThreeDRenderer::resize(GLuint width, GLuint height){

    Renderer::resize( width, height);

    if( _camera )
        _camera -> setViewSize( width, height);

    //    _fboGroup.resize( _viewWidth, _viewHeight);

}