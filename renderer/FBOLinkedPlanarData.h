//
//  FBOLinkedPlanarData.h
//  Strategy
//
//  Created by Cory Knapp on 11/19/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__FBOLinkedPlanarData__
#define __Strategy__FBOLinkedPlanarData__

#include <iostream>
#include <string>
#include <stdio.h>

#include "FrameBufferObjectCollection.h"

#include "PlanarData.h"

//template <typename T> // for now let's just do floats, but maybe we can spesiffy out own resolutions later
class FBOLinkedPlanarData : public PlanarData<float> {

    const FrameBufferObjectCollection &_fboCollection;

public:
    FBOLinkedPlanarData( const FrameBufferObjectCollection &fboCollection,
                        const size_t &height, const size_t &width ):
        _fboCollection( fboCollection ),
        PlanarData( height, width, 4 ){

            // allocate memory for 4 channels of floats
            _data = (float *)std::malloc( _height * _width * _channels * sizeof(float) );

    }
    ~FBOLinkedPlanarData();

    void updatePartial( size_t x, size_t y, size_t width, size_t height ){
        glBindFramebuffer(GL_FRAMEBUFFER, _fboCollection.fboName(0));
        glReadBuffer( GL_COLOR_ATTACHMENT0 );

        // allocate a buffer to hold the pixels.
        GLfloat bytes[width * height * _channels]; // (always 4 channels)

        glReadPixels((GLint)x, (GLint)y, (GLsizei)width, (GLsizei)height, GL_RGBA, GL_FLOAT, (GLvoid*)bytes);

        size_t index=0;
        for( int i = 0; i < width; i++ ){
            for( int j = 0; j < height; j++){
                // xxx we're doing too much copying here.
                PlanarData::setValue( i+x, j+y, 2, bytes[index + 0]);
                PlanarData::setValue( i+x, j+y, 1, bytes[index + 1]);
                PlanarData::setValue( i+x, j+y, 0, bytes[index + 2]);
                PlanarData::setValue( i+x, j+y, 3, bytes[index + 3]);
                if( bytes[index + 0] < 0 )
                    assert(false);
                if( bytes[index + 1] < 0 )
                    assert(false);
                if( bytes[index + 2] < 0 )
                    assert(false);
                if( bytes[index + 3] < 0 )
                    assert(false);
                //float r = .1;
                //float g = .2;
                //float b = .3;
                //float a = .4;
                //assert( (bytes[index + 0]>r-.02) && (bytes[index + 0]<r+.02) );
                //assert( (bytes[index + 1]>g-.02) && (bytes[index + 1]<g+.02) );
                //assert( (bytes[index + 2]>b-.02) && (bytes[index + 2]<b+.02) );
                //assert( (bytes[index + 3]>a-.02) && (bytes[index + 3]<a+.02) );
                index += 4;
            }
        }
    }

    void updateAll(){
        updatePartial( 0, 0, _width, _height );
    }
};


#endif /* defined(__Strategy__FBOLinkedPlanarData__) */
