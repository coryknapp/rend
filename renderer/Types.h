//
//  Types.h
//  Strategy
//
//  Created by Cory Knapp on 10/10/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__Types__
#define __Strategy__Types__

#include <iostream>
#include <math.h>

#include <cassert>



namespace rend {

/* xxx
 
 this was designed to be templated as 
            template <typename T, unsigned N>
 but partial template specification with to parameters is a huge pain in the ass.
 I spent a whole day trying to get it to work.
 
 T is a float now.  Deal.
 
 */

template <unsigned N>
class vecImp {

protected:
    float _v[N];

public:

    float squareLength() const{
        float sum = 0;
        for( unsigned n = 0; n < N; n++ )
            sum += _v[n] * _v[n];
        return sum;
    }

    float length() const{
        return sqrtf( squareLength() );//xxx possible loss of prosistion when T is not a float!
    }

    vecImp operator= (const vecImp &rhs){
        for( unsigned n = 0; n < N; n++ )
            _v[n] = rhs[n];
    }

    float& operator [](int n) {
        return _v[n];
    };

    vecImp& operator + (const vecImp &rhs) const{
        vecImp v;
        for( unsigned n = 0; n < N; n++ )
            v[n] = _v[n] + rhs[n];
        return v;
    }

    vecImp& operator += (const vecImp &rhs){
        for( unsigned n = 0; n < N; n++ )
            _v[n] += rhs[n];
    }

    /*vec& operator * (const vec &rhs) const{ // cross product
        assert( false );  // through some oversight, we've failed to impliment a general case for the cross product
    }*/

    //vec& operator *= (vec rhs);
    vecImp& operator * (const float &rhs){
        for( unsigned n = 0; n < N; n++ )
            _v[n] *= rhs;
    }

    //vec& operator *= (T rhs);

    vecImp& operator - (const vecImp &rhs) const{
        vecImp v;
        for( unsigned n = 0; n < N; n++ )
            v[n] = _v[n] - rhs[n];
        return v;
    }
    vecImp& operator -= (const vecImp &rhs){
        for( unsigned n = 0; n < N; n++ )
            _v[n] -= rhs[n];
    }

    //vec& operator / (vec rhs);
    //vec& operator /= (vec rhs);

    bool operator == (const vecImp &rhs) const{
        for( unsigned n = 0; n < N; n++ )
            if( _v[n] != rhs[n] )
                return false;
        return true;
    }

    bool operator != (const vecImp &rhs) const{
        return !( operator==(rhs) );
    }

    bool operator < (const vecImp &rhs) const{
        return (squareLength() < rhs.squareLength());
    }

    bool operator > (const vecImp &rhs) const{
        return (squareLength() > rhs.squareLength());
    }

    bool operator <= (const vecImp &rhs) const{
        return (squareLength() <= rhs.squareLength());
    }
    bool operator >= (const vecImp &rhs) const{
        return (squareLength() >= rhs.squareLength());
    }

    //vec& operator << (vec rhs);
    //vec& operator >> (vec rhs);
    //vec& operator <<= (vec rhs);
    //vec& operator >>= (vec rhs);

    //vec& operator ++ (vec rhs);
    //vec& operator -- (vec rhs);

    vecImp& operator % (const float &s) const{
        vecImp retVal;
        float len = length();
        for( unsigned n = 0; n < N; n++ )
            retVal[n] = _v[n] / len;
        return retVal;
    }

    vecImp& operator ! () const{
        return operator % (1);
    }

    float operator ^ (const vecImp &rhs) const{ // dot product
        float retVal = 0;
        for( unsigned n = 0; n < N; n++ ){
            retVal += _v[n] * rhs._v[n];
        }
        return retVal;
    }

    vecImp<N+1>& operator , (float rhs){
        vecImp<N+1> retVal;
        for( unsigned n = 0; n < N; n++ )
            retVal[n] = _v[n];
        retVal[N] = rhs;
        return retVal;
    }

};

/* each vecImp subclass must implement:
        appropreate constructors
        vec& operator * (const vec &rhs) const; // cross product
 */

class vec2 : public vecImp<2>{
    vec2& operator * (const vec2 &rhs) const{
        assert( false ); // cross product doesn't make sense in 2d.
    }
};

class vec3 : public vecImp<3>{
    vec3 operator * (const vec3 &rhs) const{
        vec3 retVal;
        retVal[0] = _v[1] * rhs._v[2] - rhs._v[1] * _v[2];
        retVal[1] = _v[2] * rhs._v[0] - rhs._v[2] * _v[0];
        retVal[2] = _v[0] * rhs._v[1] - rhs._v[0] * _v[1];
        return retVal;
    }
};

class vec4 : public vecImp<4>{
    vec3& operator * (const vec3 &rhs) const{
        assert( false ); // cross product doesn't make sense in 4d.
    }
};

class vec3Affine : public vecImp<4>{
    vec3Affine operator * (const vec3Affine &rhs) const{
        vec3Affine retVal;
        retVal[0] = _v[1] * rhs._v[2] - rhs._v[1] * _v[2];
        retVal[1] = _v[2] * rhs._v[0] - rhs._v[2] * _v[0];
        retVal[2] = _v[0] * rhs._v[1] - rhs._v[0] * _v[1];
        retVal[3] = 1.0;
        return retVal;
    }
};

template< unsigned N >
class vec : public vecImp<N>{
    vec3& operator * (const vec3 &rhs) const{
        assert( false ); // no general case for a cross product
    }
};


template <unsigned I, unsigned J>
class matImp {

    vec<J> _columns[I];

public:

    matImp operator= (matImp rhs){
        for( unsigned n = 0; n < I; n++ )
            _columns[n] = _columns[n];
    }

    vec<J>& operator [](int n) {
        return _columns[n];
    };

    //    mat& operator + (mat rhs);
    //    mat& operator += (mat rhs);

    matImp& operator * (matImp rhs){ // cross product

    }

    //mat& operator *= (mat rhs);

    //  mat& operator - (mat rhs);
    //  mat& operator -= (mat rhs);

    //mat& operator / (mat rhs);
    //mat& operator /= (mat rhs);

    bool operator == (matImp rhs){
        for( unsigned n = 0; n < I; n++ )
            if( _columns[n] != _columns[n] )
                return false;
        return true;
    }

    bool operator != (matImp rhs){
        return !( operator==(rhs) );
    }

    //bool operator < (mat rhs);
    //bool operator > (mat rhs);

    //bool operator <= (mat rhs);
    //bool operator >= (mat rhs);
    //mat& operator << (mat rhs);
    //mat& operator >> (mat rhs);
    //mat& operator <<= (mat rhs);
    //mat& operator >>= (mat rhs);

    //vec& operator ++ (mat rhs);
    //vec& operator -- (mat rhs);
    //mat& operator % (T s);
    //mat& operator ! ();
    //mat& operator ^ (mat rhs);
    //mat& operator , (mat rhs);

};


} // namespace rend

#endif /* defined(__Strategy__Types__) */
