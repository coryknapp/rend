//
//  ExpandablePlanar.h
//  Strategy
//
//  Created by Cory Knapp on 5/27/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__ExpandablePlanar__
#define __Strategy__ExpandablePlanar__

#include <iostream>
#include <map>
#include <memory>

#include "PlanarData.h"

template <typename T>
class ExpandablePlanar {
    
    std::map<
        int, std::map<
            int, std::unique_ptr< PlanarData<T> >
        >
    > _map;
    
    const size_t _subPlanarSize = 64;
        
public:
    
    void setValue( const int &i, const int &j, const T &value ){
        PlanarData<T> * subPlanar = _map[i/_subPlanarSize][j/_subPlanarSize].get();
        if( !subPlanar ){
            subPlanar = new PlanarData<T>( _subPlanarSize, _subPlanarSize, 1 );
            _map[i/_subPlanarSize][j/_subPlanarSize].reset(
                                                           subPlanar
                                                           );
        }
        subPlanar->setValue(i%_subPlanarSize, j%_subPlanarSize, 0, value);
    }
    
    bool getValue( const int &i, const int &j, T & value){
        PlanarData<T> * subPlanar = _map[i/_subPlanarSize][j/_subPlanarSize].get();
        if( subPlanar ){
            value = subPlanar->getValue(i%_subPlanarSize, j%_subPlanarSize, 0);
            return true;
        }
        return false;
    }
};

#endif /* defined(__Strategy__ExpandablePlanar__) */
