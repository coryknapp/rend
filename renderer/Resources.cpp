//
//  Resources.cpp
//  Escher
//
//  Created by Cory Knapp on 9/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "Resources.h"

Resources * _ResourcesSingleton = NULL;

Resources * ResourcesSingleton(){
    
    if( !_ResourcesSingleton )
        printf( "error: _ResourcesSingleton not created\n" );

    return _ResourcesSingleton;
}

Resources::Resources( const char * path ){
    
    if( _ResourcesSingleton ){
        printf( "error: Resources created more then once\n" );
    }
    _ResourcesSingleton = this;
    _path = new char[strlen( path )];
    strcpy(_path, path);
}

Resources::~Resources(){
    delete _path;
}

std::string Resources::pathForResource(const char * path ){
    
    return std::string( _path ) + '/' + std::string( path );
    
}