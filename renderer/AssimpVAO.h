//
//  AssimpVAO.h
//  Orbit
//
//  Created by Cory Knapp on 5/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef _AssimpVAO_
#define _AssimpVAO_

#include "VAO.h"

#include <iostream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "assimp_dump.h"

#include "OpenGLHelp.h"
#include "Resources.h"
#include "Buffer.h"
#include "imageUtil.h"

#define DEBUG_AssimpVAO_h

GLuint buildVAO( const aiMesh &mesh, unsigned int &elementCount );
Buffer<GLfloat> buildVertexArray( const aiMesh &mesh, GLuint &elementCount );

class AssimpVAO : public VAO {

public:

    AssimpVAO();
    AssimpVAO( const char * objFileName ); // xxx should probably never be used.  Candidate to delete.

    void loadMesh( const aiMesh &mesh );  // xxx should have some error checking mechanism.
    void loadFile( const std::string fileName );
};

#endif /* defined(__Orbit__VAO__) */
