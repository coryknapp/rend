//
//  ImageFilePlanarData.cpp
//  Strategy
//
//  Created by Cory Knapp on 12/16/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "ImageFilePlanarData.h"

#define ERROR_RETURN std::unique_ptr<class ImageFilePlanarData>()
std::unique_ptr<class ImageFilePlanarData> planarDataForImageFile( const std::string& path ){


    const char * file_name = path.c_str();
    png_byte header[8];

    FILE *fp = fopen(file_name, "rb");
    if (fp == 0)
    {
        perror(file_name);
        return ERROR_RETURN;
    }

    // read the header
    fread(header, 1, 8, fp);

    if (png_sig_cmp(header, 0, 8))
    {
        fprintf(stderr, "error: %s is not a PNG.\n", file_name);
        fclose(fp);
        return ERROR_RETURN;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
    {
        fprintf(stderr, "error: png_create_read_struct returned 0.\n");
        fclose(fp);
        return ERROR_RETURN;
    }

    // create png info struct
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        fprintf(stderr, "error: png_create_info_struct returned 0.\n");
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(fp);
        return ERROR_RETURN;
    }

    // create png info struct
    png_infop end_info = png_create_info_struct(png_ptr);
    if (!end_info)
    {
        fprintf(stderr, "error: png_create_info_struct returned 0.\n");
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
        fclose(fp);
        return ERROR_RETURN;
    }

    // the code in this if statement gets called if libpng encounters an error
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "error from libpng\n");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return ERROR_RETURN;
    }

    // init png reading
    png_init_io(png_ptr, fp);

    // let libpng know you already read the first 8 bytes
    png_set_sig_bytes(png_ptr, 8);

    // read all the info up to the image data
    png_read_info(png_ptr, info_ptr);

    // variables to pass to get info
    int bit_depth, color_type;
    png_uint_32 width, height;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                 NULL, NULL, NULL);

    //_width = temp_width;
    //_height = temp_height;

    //printf("%s: %lux%lu %d\n", file_name, temp_width, temp_height, color_type);

    if (bit_depth != 8)
    {
        fprintf(stderr, "%s: Unsupported bit depth %d.  Must be 8.\n", file_name, bit_depth);
        return ERROR_RETURN;
    }
    if ( color_type == PNG_COLOR_TYPE_PALETTE){
        fprintf(stderr, "%s: PNG_COLOR_TYPE_PALETTE unsupported. sorry!\n", file_name);
        return ERROR_RETURN;
    }
    // Update the png info struct.
    png_read_update_info(png_ptr, info_ptr);

    // Row size in bytes.
    png_size_t rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    // Allocate the image_data as a big block, to be given to opengl
    png_byte * image_data = (png_byte *)malloc(rowbytes * height * sizeof(png_byte));
    if (image_data == NULL)
    {
        fprintf(stderr, "error: could not allocate memory for PNG image data\n");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return ERROR_RETURN;
    }

    // row_pointers is for pointing to image_data for reading the png with libpng
    png_byte ** row_pointers = (png_byte **)malloc(height * sizeof(png_byte *));
    if (row_pointers == NULL)
    {
        fprintf(stderr, "error: could not allocate memory for PNG row pointers\n");
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        free(image_data);
        fclose(fp);
        return ERROR_RETURN;
    }

    // set the individual row_pointers to point at the correct offsets of image_data
    for (unsigned int i = 0; i < height; i++)
    {
        row_pointers[height - 1 - i] = image_data + i * rowbytes;
    }

    // read the png into image_data through row_pointers
    png_read_image(png_ptr, row_pointers);

    // set max size
    if( bit_depth != 8 )
    {
        fprintf(stderr, "error: unsuppored bit depth.\n");
        assert(false);
    }
    size_t channels;
    switch ( color_type ) {
        case PNG_COLOR_TYPE_GRAY:
            channels = 1;
            break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:
            channels = 2;
            break;
        case PNG_COLOR_TYPE_RGB:
            channels = 3;
            break;
        case PNG_COLOR_TYPE_RGBA:
            channels = 4;
            break;
        default:
            return ERROR_RETURN;
    }
    return std::unique_ptr<class ImageFilePlanarData>( new ImageFilePlanarData( height, width, channels, image_data));
}

ImageFilePlanarData::ImageFilePlanarData( const size_t &height, const size_t &width, const size_t &channels, uint8_t * image_data ):
PlanarData( height, width, channels )
{

    for( size_t i = 0; i < width; i++ ){
        for( size_t j = 0; j < height; j++){
            for( size_t c = 0; c < channels; c++){

                setValue( i, j, c, image_data[ ((i*_width + j)*channels) + c ] );

            }
        }
    }
}