//
//  assimp_dump.cpp
//  assimp-test
//
//  Created by Cory Knapp on 7/24/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#include "assimp_dump.h"

#ifdef REND_ASSIMP

void dumpMaterial( std::ostream &out, const aiMaterial * mat ){
    unsigned int numProps = mat->mNumProperties;
    for( int n = 0; n < numProps; n++ ){
        aiMaterialProperty * thisProp = mat->mProperties[n];
        std::cout << thisProp->mKey.data << '\t' << thisProp->mDataLength << std::endl;
    }
}

#endif // REND_ASSIMP