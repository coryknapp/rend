//
//  Buffer.h
//  Orbit
//
//  Created by Cory Knapp on 6/9/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef __ColdSoda__Buffer__
#define __ColdSoda__Buffer__

#include <memory>
#include <cassert>

#define SAFE_ALLOC_BUFFER_CHECK 1

template <class T>
class Buffer {
    
    T * _bufferPtr;
    size_t _size;
    
public:

    Buffer( size_t size ) :
        _size( size ){
        _bufferPtr = (T*)std::malloc( _size * sizeof(T));
            assert(_bufferPtr != nullptr );
    }

    Buffer( T * bufferPtr, size_t size, bool copy ) __attribute__ ((deprecated)){
        assert( bufferPtr != nullptr );
        if( copy ){
            //std::cout << "Creating Buffer by copy\n";
            _bufferPtr = (T*)malloc( size * sizeof(T) );
#ifdef SAFE_ALLOC_BUFFER_CHECK
            assert( _bufferPtr != nullptr );
#endif //SAFE_ALLOC_BUFFER_CHECK
            
            std::memcpy( _bufferPtr, bufferPtr, size * sizeof(T) );
        } else {
            //std::cout << "Creating Buffer and taking pointer ownership.\n";
            _bufferPtr = bufferPtr;
        }
        _size = size;
    }

    Buffer( const Buffer &other ){ // copy constructor
                                   //std::cout << "copy constructor.\n";
        _size = other._size;
        _bufferPtr = (T*)malloc( _size * sizeof(T) );
        memcpy( _bufferPtr, other._bufferPtr, _size * sizeof(T) );
    }
    
    Buffer& operator = (const Buffer & other){ // copy assignment
                                               //std::cout << "copy assignment.\n";
        free(_bufferPtr);
        _size = other._size;
        _bufferPtr = (T*)malloc( _size * sizeof(T) );
        memcpy( _bufferPtr, other._bufferPtr, _size * sizeof(T) );
        return *this;
    }

    Buffer( Buffer &&other ){ // move constructor
                              //std::cout << "move constructor.\n";
        _size = other._size;
        _bufferPtr = other._bufferPtr;
        other._bufferPtr = nullptr;
    }
    
    Buffer& operator = (Buffer&& other) //  move assignment
    {
        //std::cout << "move assignment.\n";
        _size = other._size;
        _bufferPtr = other._bufferPtr;
        other._bufferPtr = nullptr;
        return *this;
    }

    ~Buffer(){
        //std::cout << "~Buffer\n";
        std::free(_bufferPtr);
    }
    
    T * buffer() const{
        return _bufferPtr;
    }
    
    size_t size() const{ return _size; };
    size_t byteSize() const{ return sizeof(T) * _size; };
    
    T operator [] (size_t n) {return _bufferPtr[n];}
    
};

#endif /* defined(__ColdSoda__Buffer__) */
