//
//  macros.h
//
//  Created by Cory Knapp on 6/17/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef rend_macros_h
#define rend_macros_h

// assert
#define rend_assert( condition, message ){ printf( "%s\n", message ); assert( (condition) ); }

// math
#define INTERPOLATE( a, b, value ) ((a*(1-value))+b*value)
#define INTEGER_RATIO( numerator, denominator ) (float( numerator )/float( denominator ))

// other shiz
#define START_CODE_BLOCK {
#define END_CODE_BLOCK }

// c++
#define MANUALLY_TEMPLATE_CLASS( class_name, type_name ) template <> \
                                                        class class_name<type_name>

#endif
