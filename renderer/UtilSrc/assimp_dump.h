//
//  assimp_dump.h
//  assimp-test
//
//  Created by Cory Knapp on 7/24/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef __assimp_test__assimp_dump__
#define __assimp_test__assimp_dump__

#ifdef REND_ASSIMP

#include <iostream>
#include <assimp/material.h>

void dumpMaterial( std::ostream &out, const aiMaterial * mat );

#endif // defined REND_ASSIMP

#endif /* defined(__assimp_test__assimp_dump__) */
