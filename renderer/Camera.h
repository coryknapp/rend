//
//  Camera.h
//  Escher
//
//  Created by Cory Knapp on 9/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _Camera_
#define _Camera_

#define DEBUG_Camera_h


#include "OpenGLHelp.h"

#include "SimState.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
/*
    Camera class
 
    Abstract class.
    
    It's safe to assume that all camera implementations are going to need to manage the size of their viewport
    so, we can safely take care of that here.
 
    Also, we can manage the memory for our view and projection matrices, but we won't be calculating them here.

*/

const float cameraDepth = 50.0f;

class Camera {

protected:

    GLint _viewWidth = 0;
    GLint _viewHeight = 0;

	glm::mat4 _viewMatrix;
	glm::mat4 _projectionMatrix;

public:
    
    Camera();
    ~Camera();
    virtual void setViewSize( GLint width, GLint height );
    // Any sublass to override this must call Camera::setViewSize( width, height ) first thing

    GLint viewWidth()const {return _viewWidth;}
    GLint viewHeight()const {return _viewHeight;}

	glm::mat4 projectionMatrix() const{ return _projectionMatrix; }
	glm::mat4 viewTransformMatrix() const{ return _viewMatrix; }

    virtual void recalcViewTransformMatrix() = 0;
    virtual void recalcProjectionMatrix() = 0;
    
    Camera& operator=( const Camera& rhs );

    virtual void tick( const SimTime &d ) = 0;
};

#endif