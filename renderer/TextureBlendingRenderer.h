//
//  TextureBlendingRenderer.h
//
//  Copyright 2013 Cory Knapp. All rights reserved.
//

/*
 
 TextureBlendingRenderer is meant to be a
 abstract base class for a custom class that renders a blend of textures onto a full screen quad.

 Subclasses should implement:
 1. void prepareTexturesForRender()
    should be self explanatory.  Renderer your scenes into your FBOs.
 2. void initProgram()
    Should initilize a BlenderGLSLProgram (or a subclass of that) and call void setProgram(...). 
 For example the following code will blend two samples evenly.
 gl_fragColor = mix( SAMPLE_1, SAMPLE_1, 0.5 );
 A uniform SAMPLE_COUNT will be passed in indicating the number of samples.

 This makes scene to my objective-c addiled mind.
 Question for myself.  Would it be possible to implement this with a constructer that passed in
 blendFunction as a string and prepareTexturesForRender as a lamda function?  Eliminating the need to subclasses 99% of the time.
 
 */

#ifndef _TextureBlendingRenderer_
#define _TextureBlendingRenderer_

#include <string>

#include "Renderer.h"
#include "OpenGLHelp.h"

#include "sourceUtil.h"
#include "imageUtil.h"

#include "BlenderGLSLProgram.h"
#include "Texture.h"
#include "VAO.h"

#define FRAMEBUFFER_CLEAR_COLOR .5,0,1,1 // sort of a bluish purple

#define DEBUG_TextureBlendingRenderer_h

class TextureBlendingRenderer : public Renderer{
        
    bool _fboInits;
    VAO _fullscreenQuadVAO;

    void initFBOs();

    void _createFullFrameQuadVAO();
    void _renderFullFrameQuad();

    BlenderGLSLProgram * _blenderProgram;

protected:

    void renderFullFrameQuad();
    FrameBufferObjectCollection _samplesFBOGroup;

public:

    TextureBlendingRenderer(GLuint defaultFBOName, size_t sampleCount);
    ~TextureBlendingRenderer(){
        
    }

    virtual void resize(GLuint width, GLuint height);
    void setProgram( BlenderGLSLProgram * blenderProg );
    void render();
    void destroyLayerTextures();

    virtual void prepareTexturesForRender() = 0;
    virtual void initProgram() = 0;
    virtual void initialize();

#ifdef DEBUG_TextureBlendingRenderer_h
    bool _didWeInit_debugflag = false;
#endif

};

VAO createFullFrameQuadVAO();

#endif