//
//  Camera.cpp
//
//  Created by Cory Knapp on 9/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "Camera.h"

Camera::Camera(){
    //_focus[0] = 0; _focus[1] = 0; _focus[2] = 0; // xx currently unused?
    //_location[0] = 0; _location[1] = 0; _location[2] = 3;
    //_up[0] = 0; _up[1] = 1; _up[2] = 0; // xx currently unused?
    
    //_distance = 4;
    //_longitude = 0;
    //_latitude = 0;
 
}

Camera::~Camera(){

}

void Camera::recalcProjectionMatrix(){
	_projectionMatrix = glm::perspective(45.0f,
										 (float)_viewWidth / (float)_viewHeight,
										 .1f, cameraDepth);

}

void Camera::recalcViewTransformMatrix(){

#ifdef DEBUG_Camera_h
    assert( _viewWidth > 0 );
    assert( _viewHeight > 0 );
#endif

}

void Camera::setViewSize( int width, int height ){
    _viewWidth = width;
    _viewHeight = height;
    
    this -> recalcProjectionMatrix();
}

Camera& Camera::operator=( const Camera& rhs ){

    _projectionMatrix = rhs._projectionMatrix;
    _viewMatrix = rhs._viewMatrix;

    return *this;
}
