//
//  VAO.cpp
//
//  Created by Cory Knapp on 5/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#include "AssimpVAO.h"

Buffer<GLfloat> buildVertexArray( const aiMesh &mesh, GLuint &elementCount ){

    // xxx notice that right now we're only building for the first mesh in the scene
    // should we consider buildVertexArray( const aiMesh &mesh )?

    unsigned int size = 0; // note when we say size, we mean the total number vertexs

    for( int n = 0; n < mesh.mNumFaces; n++ ){
        size += mesh.mFaces[n].mNumIndices;
    }

    // xxx there are some more properties we might want to put in the buffer
    // also, we should give the user an option of not putting them in somehow
	int stride = 0;
    stride += VALUE_COUNT_FOR_ATTRIBUTE( POSITION_ATTRIBUTE_INDEX )
		   			* mesh.HasPositions();
    stride += VALUE_COUNT_FOR_ATTRIBUTE( NORMAL_ATTRIBUTE_INDEX )
			   		* mesh.HasNormals();
    stride += VALUE_COUNT_FOR_ATTRIBUTE( TEXTURECOORD_ATTRIBUTE_INDEX )
					* mesh.HasTextureCoords(0);
    stride += VALUE_COUNT_FOR_ATTRIBUTE( VERTEXCOLOR_ATTRIBUTE_INDEX )
	   				* mesh.HasVertexColors(0);

    GLfloat * returnArray = new GLfloat[ size * stride ];

    int index = 0;
    int vertexIndex;
    for( int n = 0; n < mesh.mNumFaces; n++ ){
        for( int m = 0; m < mesh.mFaces[n].mNumIndices; m++ ){
            vertexIndex = mesh.mFaces[n].mIndices[m];

            // copy the HasPositions
            returnArray[index       ] = mesh.mVertices[vertexIndex].x;
            returnArray[index   +1  ] = mesh.mVertices[vertexIndex].y;
            returnArray[index   +2  ] = mesh.mVertices[vertexIndex].z;
            returnArray[index   +3  ] = 1;
            index += VALUE_COUNT_FOR_ATTRIBUTE( POSITION_ATTRIBUTE_INDEX );
            //}

            if( mesh.HasNormals() ){
                returnArray[index  ] = mesh.mNormals[vertexIndex].x;
                returnArray[index   +1  ] = mesh.mNormals[vertexIndex].y;
                returnArray[index   +2  ] = mesh.mNormals[vertexIndex].z;
                index += VALUE_COUNT_FOR_ATTRIBUTE( NORMAL_ATTRIBUTE_INDEX );
            }

            if( mesh.HasTextureCoords(0) ){
                returnArray[index   +0  ] =
					mesh.mTextureCoords[0][vertexIndex].x;
                returnArray[index   +1  ] =
					mesh.mTextureCoords[0][vertexIndex].y;
                index += VALUE_COUNT_FOR_ATTRIBUTE(
						TEXTURECOORD_ATTRIBUTE_INDEX );
            }

            if( mesh.HasVertexColors(0) ){
                returnArray[index   +0  ] = mesh.mColors[0][vertexIndex].r;
                returnArray[index   +1  ] = mesh.mColors[0][vertexIndex].g;
                returnArray[index   +2  ] = mesh.mColors[0][vertexIndex].b;
                returnArray[index   +3  ] = mesh.mColors[0][vertexIndex].a;
                index += VALUE_COUNT_FOR_ATTRIBUTE(
						VERTEXCOLOR_ATTRIBUTE_INDEX );
            }

        }
    }
    Buffer<GLfloat> returnBuf( returnArray, index, false );
    elementCount = size;
    return returnBuf;
}

GLuint buildVAO( const aiMesh &mesh, GLuint &elementCount ){
    // xxx notice that right now we're only building for the first mesh in the scene
    // should we consider buildVertexArray( const aiMesh &mesh )?

    Buffer<GLfloat> vaoBuffer = buildVertexArray( mesh, elementCount );

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );

    menu[POSITION_ATTRIBUTE_INDEX]    = mesh.HasPositions();
    menu[NORMAL_ATTRIBUTE_INDEX]    = mesh.HasNormals();
    menu[TEXTURECOORD_ATTRIBUTE_INDEX]    = mesh.HasTextureCoords(0);
    menu[VERTEXCOLOR_ATTRIBUTE_INDEX]    = mesh.HasVertexColors(0);

    return buildVAO( vaoBuffer, menu, elementCount);
}

AssimpVAO::AssimpVAO() :
    VAO() {}

AssimpVAO::AssimpVAO( const char * objFileName ){
    Assimp::Importer importer;
    const aiScene * scene = importer.ReadFile(
			ResourcesSingleton()->pathForResource( objFileName ).c_str(),
			aiProcess_Triangulate );

    // If the import failed, report it
    if( !scene)
    {
        std::cerr << "error %s", importer.GetErrorString();
		assert(false);
		// xxx have yet to settle on exactly how we're to report errors yet.
        return;
    }

    aiMesh * firstMesh = scene->mMeshes[0];
    loadMesh( *firstMesh );
}


void AssimpVAO::loadMesh( const aiMesh &mesh ){

    GLuint vaoName;
    GLuint elementCount;

    vaoName = buildVAO( mesh, elementCount );
    set( vaoName, elementCount, GL_TRIANGLES );

};

void AssimpVAO::loadFile( const std::string fileName ){
    Assimp::Importer importer;
    const aiScene * scene = importer.ReadFile(
			ResourcesSingleton()->pathForResource( fileName.c_str() ).c_str(),
			aiProcess_Triangulate );
    
    // If the import failed, report it
    if( !scene)
    {
        std::cerr << "error %s", importer.GetErrorString();
		assert(false);
		// xxx have yet to settle on exactly how we're to report errors yet.
        return;
    }
    
    aiMesh * firstMesh = scene->mMeshes[0];
    loadMesh( *firstMesh );
}
