//
//  Texture.cpp
//  assimp-test
//
//  Created by Cory Knapp on 7/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#include "Texture.h"

#define ERROR_INSTRUCTION( message ) {fprintf(stderr, message, file_name);assert(false);}
GLuint buildTexture(const char * file_name, int * width, int * height)
{
    //std::cout << "building " << file_name << "\n";

    png_byte header[8];

    FILE *fp = fopen(file_name, "rb");
    if (fp == 0)
    {
        perror(file_name);
        ERROR_INSTRUCTION("unknown error (%s)");
    }

    // read the header
    fread(header, 1, 8, fp);

    if (png_sig_cmp(header, 0, 8))
    {
        fclose(fp);
        ERROR_INSTRUCTION("error: %s is not a PNG.\n");
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
    {
        fclose(fp);
        ERROR_INSTRUCTION("error: png_create_read_struct returned 0.\n (%s)");
    }

    // create png info struct
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(fp);
        ERROR_INSTRUCTION("error: png_create_info_struct returned 0.\n (%s)");
    }

    // create png info struct
    png_infop end_info = png_create_info_struct(png_ptr);
    if (!end_info)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
        fclose(fp);
        ERROR_INSTRUCTION("error: png_create_info_struct returned 0.\n (%s)");
    }

    // the code in this if statement gets called if libpng encounters an error
    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        ERROR_INSTRUCTION("error from libpng (%s)");
    }

    // init png reading
    png_init_io(png_ptr, fp);

    // let libpng know you already read the first 8 bytes
    png_set_sig_bytes(png_ptr, 8);

    // read all the info up to the image data
    png_read_info(png_ptr, info_ptr);

    // variables to pass to get info
    int bit_depth, color_type;
    png_uint_32 temp_width, temp_height;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &temp_width, &temp_height, &bit_depth, &color_type,
                 NULL, NULL, NULL);

    if (width){ *width = temp_width; }
    if (height){ *height = temp_height; }

    //printf("%s: %lux%lu %d\n", file_name, temp_width, temp_height, color_type);

    if (bit_depth != 8)
    {
        fprintf(stderr, "%s: Unsupported bit depth %d.  Must be 8.\n", file_name, bit_depth);
        ERROR_INSTRUCTION("error (%s)");
    }

    GLint format;
    switch(color_type)
    {
        case PNG_COLOR_TYPE_RGB:
            format = GL_RGB;
            break;
        case PNG_COLOR_TYPE_RGB_ALPHA:
            format = GL_RGBA;
            break;
        default:
            fprintf(stderr, "%s: Unknown libpng color type %d.\n", file_name, color_type);
            ERROR_INSTRUCTION("error (%s)");
    }

    // Update the png info struct.
    png_read_update_info(png_ptr, info_ptr);

    // Row size in bytes.
    png_size_t rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    // glTexImage2d requires rows to be 4-byte aligned
    rowbytes += 3 - ((rowbytes-1) % 4);

    // Allocate the image_data as a big block, to be given to opengl
    png_byte * image_data = (png_byte *)malloc(rowbytes * temp_height * sizeof(png_byte)+15);
    if (image_data == NULL)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        ERROR_INSTRUCTION("error: could not allocate memory for PNG image data\n (%s)");
    }

    // row_pointers is for pointing to image_data for reading the png with libpng
    png_byte ** row_pointers = (png_byte **)malloc(temp_height * sizeof(png_byte *));
    if (row_pointers == NULL)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        free(image_data);
        fclose(fp);
        ERROR_INSTRUCTION("error: could not allocate memory for PNG row pointers\n (%s)");
    }

    // set the individual row_pointers to point at the correct offsets of image_data
    for (unsigned int i = 0; i < temp_height; i++)
    {
        row_pointers[temp_height - 1 - i] = image_data + i * rowbytes;
    }

    // read the png into image_data through row_pointers
    png_read_image(png_ptr, row_pointers);

    // Generate the OpenGL texture object
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, temp_width, temp_height, 0, format, GL_UNSIGNED_BYTE, image_data);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // clean up
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    free(image_data);
    free(row_pointers);
    fclose(fp);
    return texture;
}

Texture::Texture(){

    
}

bool Texture::loadFile( const char * filePath ){ // initialize with the image file at path
    int width, height;
    _texName = buildTexture( filePath, &width, &height);
    // std::cout << "tex = " << _texName << std::endl;
    assert( width > 0 );
    assert( height > 0 );

    return true; // xxx why have a return code if it's always true?
}

bool Texture::loadPlanerData( const PlanarData<float> * plane){

    GLenum format;
    switch( plane->channels() )
    {
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            std::cerr << "Not sure how to make a texture with " << plane->channels() << "channels\n";
            assert(false);
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, (GLsizei)plane->width(), (GLsizei)plane->height(), 0, format, GL_FLOAT, plane->ptr());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GetGLError();

    _texName = texture;

    return true; // xxx why have a return code if it's always true?
}

bool Texture::loadPlanerData( const PlanarData<uint8_t> * plane){

    GLenum format;
    switch( plane->channels() )
    {
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            std::cerr << "Not sure how to make a texture with " << plane->channels() << "channels\n";
            assert(false);
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, (GLsizei)plane->width(), (GLsizei)plane->height(), 0, format, GL_UNSIGNED_BYTE, plane->ptr());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GetGLError();

    _texName = texture;

    return true; // xxx why have a return code if it's always true?
}

GLint Texture::textureName() const{
    return _texName;
}

std::string Texture::getError(){
    return _lastError;
}


Texture::~Texture(){
    
}