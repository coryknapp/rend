//
//  WorldCamera.h
//  Strategy
//
//  Created by Cory Knapp on 9/20/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __FlyCamera__
#define __FlyCamera__

#include <iostream>

#include "Camera.h"
#include <math.h>

#include "SimState.h"

class FlyCamera: public Camera {

    const float max_horizonAngle = M_PI/2; // in degrees
    const float min_horizonAngle = -M_PI/2; //
    
    // matricies
    glm::mat4 _cameraTranslation; // inverse of view translation
    glm::mat4 _cameraRotation; // inverce of view rotation;

    glm::vec4 _location;

    float _horizonAngle = 0;
    float _zRotationAngle = M_PI; // in degrees (xxx)

    //
    float _forwardMovment = 0;
    float _leftMovment = 0;
    float _upMovment = 0;
    float _flightSpeed = 1.5f;

public:

    FlyCamera();

    FlyCamera& operator=( const FlyCamera& rhs );

    virtual void recalcViewTransformMatrix();
    virtual void recalcProjectionMatrix();

    void recalcCameraMatrices();

    virtual void mouseDrag( float dx, float dy );
    virtual void setViewSize( int width, int height );

    virtual void zoomIn( const float &z );
    virtual void zoomOut( const float &z ){ zoomIn( -z ); };

    void tick( const SimTime &d );

    void setForwardMovment( const float &d ); // pass a negitive d to go back.
    void setLeftMovment( const float &d ); // pass a negitive d to go right.
    void setUpMovment( const float &d );

	glm::vec4 location(){ return _location; };
};


#endif /* defined(__Strategy__WorldCamera__) */
