//
//  GLSLProgram.h
//
//  Created by Cory Knapp on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#ifndef GLSLProgram_def
#define GLSLProgram_def

#include <string>
#include <memory>
#include <iostream>
#include <assert.h>

#include <stdlib.h>

//#include <OpenGL/OpenGL.h>

#include "sourceUtil.h"
#include "OpenGLHelp.h"
#include "Resources.h"

#define DEBUG_GLSLProgram_h

GLuint buildProgram(const std::string& vertexSource, const std::string& fragmentSource, const attributeMenu_t atributes);
void destroyProgram(GLuint prgName);

class GLSLProgram{
    
protected:
    GLint _programName = 0;
    
public:
    GLSLProgram();
    ~GLSLProgram();
    
    virtual void use();
    virtual void build(){
#ifdef DEBUG_GLSLProgram_h
        _didWeBuild_debugflag = true;
#endif
    }
    
    virtual void initBindings() = 0;

    bool getUniform( GLint &uniformLocation, const char * const shaderName );

#ifdef DEBUG_GLSLProgram_h
    bool _didWeBuild_debugflag = false;
#endif
};

#endif