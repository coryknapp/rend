//
//  Renderer.cpp
//  OpenBoxel
//
//  Created by Cory Knapp on 4/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "Renderer.h"

Renderer::Renderer(GLuint defaultFBOName)
    :   _mainFBOName(defaultFBOName),
        _viewWidth(0),
        _viewHeight(0)
{
    //const GLubyte * versionString = glGetString(GL_VERSION);
    //printf( "gl version = %s\n", glGetString(GL_VERSION) );
    //GetGLError();

}

Renderer::~Renderer(){
    //xxx
}

/*void Renderer::initGaussianDistributionMap(){
    
    const int mapSize = 32;
    const float mu = mapSize/2.0;
    const float sd = mapSize/6.0;
    
    GLfloat map[ mapSize ];
    for( int n = 0; n < mapSize; n++ ){
        map[n] = (1.0/(sd*sqrtf(2*M_PI))) * powf(M_E, -( (n - mu)*(n-mu)/(2*sd*sd)) );
    }
    
    glGenTextures(1, &_gaussianDistributionMapTextureName);
    glBindTexture( GL_TEXTURE_1D, _gaussianDistributionMapTextureName);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GetGLError();

    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GetGLError();

    glTexImage1D( GL_TEXTURE_1D, 0, 1, mapSize, 0, GL_RED, GL_FLOAT, map);
    GetGLError();

    
} */

void Renderer::resize(GLuint width, GLuint height){

	_viewWidth = width;
	_viewHeight = height;
    
    glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    glViewport(0, 0, _viewWidth, _viewHeight);

}