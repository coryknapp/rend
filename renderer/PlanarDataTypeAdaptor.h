//
//  PlanarDataTypeAdaptor.h
//  Strategy
//
//  Created by Cory Knapp on 3/30/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__PlanarDataTypeAdaptor__
#define __Strategy__PlanarDataTypeAdaptor__

#include <iostream>

#include "PlanarData.h"

template <typename IN, typename OUT>
class PlanarDataTypeAdaptor : public PlanarData<OUT> {
    
    PlanarData<IN> * _source = nullptr;
    
public:
    
    typedef IN  in_type;
    typedef OUT out_type;
    
    PlanarDataTypeAdaptor( PlanarData<IN> * source ){
        // source must last as long as the PlanarDataTypeAdaptor it wraps.
        _source = source;
    }
    
    virtual void setValue( const size_t &i, const size_t &j, const size_t &channel, const OUT &value  ){
        assert(false);  // I don't think this should ever be called.  Maybe I'm wrong.
    }
    
    virtual const OUT& getValue( const size_t &i, const size_t &j, const size_t &channel ){
        IN in_max, in_min;
        OUT out_max, out_min;
        
        if( std::is_floating_point<IN>::value ){
            in_max = 1;
            in_min = 0;
        } else { // IN is not floating.
            in_max = std::numeric_limits<IN>::max();
            in_min = std::numeric_limits<IN>::min();
        }
        
        if( std::is_floating_point<OUT>::value ){
            out_max = 1;
            out_min = 0;
        } else { // IN is not floating.
            out_max = std::numeric_limits<OUT>::max();
            out_min = std::numeric_limits<OUT>::min();
        }
        
        // linear map
        float s = float( _source->getValue( i, j, channel ) - in_min ) / float(in_max - in_min);
        return OUT( s * (out_max - out_min) - out_min );
    }
    
};

#endif /* defined(__Strategy__PlanarDataTypeAdaptor__) */
