//
//  FrameBufferObject.cpp
//  Escher
//
//  Created by Cory Knapp on 1/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#include "FrameBufferObjectCollection.h"


GLuint buildFBO(GLuint width,
				GLuint height,
				GLuint &colorTexture,
				GLuint &depthRenderBuffer){
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( width > 0 );
    assert( height > 0 );
#endif

    GLuint fb;

    glGenFramebuffers(1, &fb);
    glGenTextures(1, &colorTexture);

    if( depthRenderBuffer == 0 ){
        glGenRenderbuffers(1, &depthRenderBuffer);

        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,
							  GL_DEPTH_COMPONENT24,
							  width, height);
    }
    //switch to our fbo so we can bind stuff to it
    glBindFramebuffer(GL_FRAMEBUFFER, fb);

    glBindTexture(GL_TEXTURE_2D, colorTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
                 GL_RGBA, GL_INT, NULL);

    glFramebufferTexture2D(GL_FRAMEBUFFER,
                           GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_2D, colorTexture, 0);
    GetGLError();
    /*glBindTexture(GL_TEXTURE_2D, depthTexture);
     GetGLError();
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
     glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
     GetGLError();
     glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0,
     GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
     GetGLError();
     glFramebufferTexture2D(GL_FRAMEBUFFER,
     GL_DEPTH_ATTACHMENT,
     GL_TEXTURE_2D, depthTexture, 0);
     */
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    GetGLError();
    GLenum status;
#ifdef DEBUG_FrameBufferObjectCollection_h
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( status == GL_FRAMEBUFFER_UNDEFINED )
        printf( "    GL_FRAMEBUFFER_UNDEFINED\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n");
    if( status == GL_FRAMEBUFFER_UNSUPPORTED )
        printf( "    GL_FRAMEBUFFER_UNSUPPORTED\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
    if( status == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE )
        printf( "    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
    assert( status == GL_FRAMEBUFFER_COMPLETE );
#endif
    // Go back to regular frame buffer rendering
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, 0);

    return fb;
}

void destroyFBO(GLuint fboName){

    if(0 == fboName)
	{
		return;
	}

    glBindFramebuffer(GL_FRAMEBUFFER, fboName);


    GLint maxColorAttachments = 1;


	// OpenGL ES on iOS 4 has only 1 attachment.
	// There are many possible attachments on OpenGL
	// on MacOSX so we query how many below
#if !ESSENTIAL_GL_PRACTICES_IOS
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxColorAttachments);
#endif

	GLint colorAttachment;

	// For every color buffer attached
    for(colorAttachment = 0; colorAttachment < maxColorAttachments; colorAttachment++)
    {
		// Delete the attachment
		deleteFBOAttachment(GL_COLOR_ATTACHMENT0+colorAttachment);
	}

	// Delete any depth or stencil buffer attached
    deleteFBOAttachment(GL_DEPTH_ATTACHMENT);

    deleteFBOAttachment(GL_STENCIL_ATTACHMENT);

    glDeleteFramebuffers(1,&fboName);
}

void deleteFBOAttachment(GLenum attachment){
    GLint param;
    GLuint objName;

    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, attachment,
                                          GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
                                          &param);

    if(GL_RENDERBUFFER == param)
    {
        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, attachment,
                                              GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
                                              &param);

        objName = ((GLuint*)(&param))[0];
        glDeleteRenderbuffers(1, &objName);
    }
    else if(GL_TEXTURE == param)
    {

        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, attachment,
                                              GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
                                              &param);

        objName = ((GLuint*)(&param))[0];
        glDeleteTextures(1, &objName);
    }


}


void FrameBufferObjectCollection::_cleanUp(){
    for( int i = 0; i < _collectionSize; i++ ){
        destroyFBO( _fboArray[i] );
        glDeleteTextures( 1, &_textureColorNameArray[i]);
        _fboArray[i] = 0;
        _textureColorNameArray[i] = 0;
    }
    glDeleteRenderbuffers( 1, &_depthRenderBufferName);
    _depthRenderBufferName = 0;
    _fboBuilt = false;
    GetGLError();

}

FrameBufferObjectCollection::FrameBufferObjectCollection( size_t size ){
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( size > 0 );
#endif
    _collectionSize = size;
    _fboArray = (GLuint*)malloc( _collectionSize * sizeof(GLuint));
    _textureColorNameArray = (GLuint*)malloc( _collectionSize * sizeof(GLuint));
    _width = 0;
    _height = 0;
    _depthRenderBufferName = 0;
    _fboBuilt = false;
}

FrameBufferObjectCollection::~FrameBufferObjectCollection(){
    _cleanUp();
}

void FrameBufferObjectCollection::setDepthRenderBuffer( GLint depthRenderBuffer ){
    // xxx
    std::cerr << "FrameBufferObjectCollection::setDepthRenderBuffer unimplemented" << std::endl;
}

void FrameBufferObjectCollection::build(){
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( _width > 0 );   // the FBO must be resized before being built
    assert( _height > 0);
#endif
    if( _fboBuilt ){
        _cleanUp();
        GetGLError();
    }
    if (  !_fboBuilt ) {
        for( int i = 0; i < _collectionSize; i++ ){
            _fboArray[i] = buildFBO( _width, _height, _textureColorNameArray[i], _depthRenderBufferName);
            glViewport(0, 0, _width, _height);
            GetGLError();
        }
        _fboBuilt = true;
    }
#ifdef DEBUG_FrameBufferObjectCollection_h
    _didWeInit_debugflag = true;
#endif

}

void FrameBufferObjectCollection::bind( int fboIndex ){
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( _didWeInit_debugflag == true );
#endif
    glBindFramebuffer(GL_FRAMEBUFFER, _fboArray[ fboIndex ]);
    GetGLError();
}

void FrameBufferObjectCollection::clear(){
    glClearColor( FBO_COLLECTION_CLEAR_COLOR );
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, _width, _height);
    GetGLError();
}

void FrameBufferObjectCollection::clearAll(){
    for( int i = 0; i < _collectionSize; i++ ){
        bind( i );
        clear();
    }
    clearDepth();
}

void FrameBufferObjectCollection::clearDepth(){
    bind( 0 );
    glClearDepth(1.0);
    glDepthFunc(GL_LESS);
    glClear(GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, _width, _height);
    GetGLError();
}

void FrameBufferObjectCollection::resize( int width, int height ){
    _width = width;
    _height = height;
    build();
}

GLuint FrameBufferObjectCollection::textureColorName( int fboIndex ) const{
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( fboIndex < _collectionSize );
#endif
    return _textureColorNameArray[ fboIndex ];
}

GLuint FrameBufferObjectCollection::fboName( int i ) const{
#ifdef DEBUG_FrameBufferObjectCollection_h
    assert( i < _collectionSize );
#endif
    return _fboArray[i];
}

GLuint FrameBufferObjectCollection::depthRenderBufferName() const{
    return _depthRenderBufferName;
}
