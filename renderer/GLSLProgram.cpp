//
//  GLSLProgram.cpp
//  Escher
//
//  Created by Cory Knapp on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#include "GLSLProgram.h"

GLSLProgram::GLSLProgram(){
}

GLSLProgram::~GLSLProgram(){
    if( _programName != 0 )
        glDeleteProgram( _programName );
}

void GLSLProgram::use(){
    glUseProgram( _programName );
}

bool GLSLProgram::getUniform( GLint &uniformLocation, const char * const shaderUniformName ){
    use();
    uniformLocation = glGetUniformLocation( _programName, shaderUniformName);
    GetGLError();

#ifdef DEBUG_GLSLProgram_h
    assert( uniformLocation >= 0); // couldn't find the uniform.
                                   // Note: not necessarily a bug, as a -1 would be returned if shaderUniformName is
                                   // present but the shader optimized it out because it's unused.
                                   // We'll treat it like a bug though, unless we find a compelling reason not to.
#endif
    return (bool)uniformLocation;
}


GLuint buildProgram(const std::string& vertexSource, const std::string& fragmentSource, const attributeMenu_t atributesMenu){ // array must be ATTRIB_IDX_COUNT in length
    GLuint prgName;

    GLint logLength, status;

    // String to pass to glShaderSource
    GLchar* sourceString = NULL;

    // Determine if GLSL version 140 is supported by this context.
    //  We'll use this info to generate a GLSL shader source string
    //  with the proper version preprocessor string prepended
    float  glLanguageVersion;

#if ESSENTIAL_GL_PRACTICES_IOS
    sscanf((char *)glGetString(GL_SHADING_LANGUAGE_VERSION), "OpenGL ES GLSL ES %f", &glLanguageVersion);
#else
    sscanf((char *)glGetString(GL_SHADING_LANGUAGE_VERSION), "%f", &glLanguageVersion);
#endif

    // GL_SHADING_LANGUAGE_VERSION returns the version standard version form
    //  with decimals, but the GLSL version preprocessor directive simply
    //  uses integers (thus 1.10 should 110 and 1.40 should be 140, etc.)
    //  We multiply the floating point number by 100 to get a proper
    //  number for the GLSL preprocessor directive
    GLuint version = 100 * glLanguageVersion;

    // Get the size of the version preprocessor string info so we know
    //  how much memory to allocate for our sourceString
    const GLsizei versionStringSize = sizeof("#version 123\n");

    // Create a program object
    prgName = glCreateProgram();

    // Indicate the attribute indicies on which vertex arrays will be
    //  set with glVertexAttribPointer
    //  See buildVAO to see where vertex arrays are actually set
    FOR_EACH_ATTRIBUTE_INDEX( index_n ){
        for (attributeMenuItem_t n = 0; n<atributesMenu[index_n]; n++) {
            glBindAttribLocation(prgName, index_n, attributeNameForIndex(index_n,n).c_str() );
        }
    }

    //////////////////////////////////////
    // Specify and compile VertexShader //
    //////////////////////////////////////

    // Allocate memory for the source string including the version preprocessor information
    sourceString = (GLchar *)malloc(vertexSource.length() + versionStringSize);

    // Prepend our vertex shader source string with the supported GLSL version so
    //  the shader will work on ES, Legacy, and OpenGL 3.2 Core Profile contexts
    sprintf(sourceString, "#version %d\n%s", version, vertexSource.c_str() );

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, (const GLchar **)&(sourceString), NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &logLength);

#ifdef DEBUG_GLSLProgram_h
    if (logLength > 0)
    {
        GLchar *log = (GLchar*) malloc(logLength);
        glGetShaderInfoLog(vertexShader, logLength, &logLength, log);
        printf("Vtx Shader compile log:%s\n", log);
        free(log);
    }
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        printf("Failed to compile vtx shader:\n%s\n", sourceString);
        assert(0); // vertex shader failed to compile
        return 0;
    }
#endif
    GetGLError();


    free(sourceString);
    sourceString = NULL;

    // Attach the vertex shader to our program
    glAttachShader(prgName, vertexShader);


    /////////////////////////////////////////
    // Specify and compile Fragment Shader //
    /////////////////////////////////////////

    // Allocate memory for the source string including the version preprocessor	 information
    sourceString = (GLchar *)malloc(fragmentSource.length() + versionStringSize);

    // Prepend our fragment shader source string with the supported GLSL version so
    //  the shader will work on ES, Legacy, and OpenGL 3.2 Core Profile contexts
    sprintf(sourceString, "#version %d\n%s", version, fragmentSource.c_str());

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, (const GLchar **)&(sourceString), NULL);
    glCompileShader(fragShader);
    glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);

#ifdef DEBUG_GLSLProgram_h
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc(logLength);
        glGetShaderInfoLog(fragShader, logLength, &logLength, log);
        printf("Frag Shader compile log:\n%s\n", log);
        free(log);
    }
    glGetShaderiv(fragShader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        printf("Failed to compile frag shader:\n%s\n", sourceString);
        assert(0); // frag shader failed to compile
        return 0;
    }
#endif

    free(sourceString);
    sourceString = NULL;

    // Attach the fragment shader to our program
    glAttachShader(prgName, fragShader);


    //////////////////////
    // Link the program //
    //////////////////////

    glLinkProgram(prgName);
    glGetProgramiv(prgName, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc(logLength);
        glGetProgramInfoLog(prgName, logLength, &logLength, log);
        printf("Program link log:\n%s\n", log);
        free(log);
    }

    glGetProgramiv(prgName, GL_LINK_STATUS, &status);
    if (status == 0)
    {
        printf("Failed to link program");
        return 0;
    }

    glValidateProgram(prgName);
    glGetProgramiv(prgName, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar*)malloc(logLength);
        glGetProgramInfoLog(prgName, logLength, &logLength, log);
        printf("Program validate log:\n%s\n", log);
        free(log);
    }

    glGetProgramiv(prgName, GL_VALIDATE_STATUS, &status);
    if (status == 0)
    {
        printf("Failed to validate program.\n");
        return 0;
    }
    GetGLError();


    glUseProgram(prgName);

    GetGLError();
    
    return prgName;
    

    
}

void destroyProgram(GLuint prgName){
    if(0 == prgName)
	{
		return;
	}
	
	GLsizei shaderNum;
	GLsizei shaderCount;
	
	// Get the number of attached shaders
	glGetProgramiv(prgName, GL_ATTACHED_SHADERS, &shaderCount);
	
	GLuint* shaders = (GLuint*)malloc(shaderCount * sizeof(GLuint));
	
	// Get the names of the shaders attached to the program
	glGetAttachedShaders(prgName,
						 shaderCount,
						 &shaderCount,
						 shaders);
	
	// Delete the shaders attached to the program
	for(shaderNum = 0; shaderNum < shaderCount; shaderNum++)
	{
		glDeleteShader(shaders[shaderNum]);
	}
	
    free(shaders);
	
	// Delete the program
	glDeleteProgram(prgName);
	glUseProgram(0);
}
