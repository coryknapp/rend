//
//  FlyCamera.cpp
//  Strategy
//
//  Created by Cory Knapp on 9/20/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#include "FlyCamera.h"

FlyCamera::FlyCamera(){
    _location[0] = 0; _location[1] = 0; _location[2] = 4; _location[3];
    _flightSpeed = .15;
}

FlyCamera& FlyCamera::operator=( const FlyCamera& rhs ){
    Camera::operator=(rhs);

    _location = rhs._location;

    _horizonAngle = rhs._horizonAngle;
    _zRotationAngle = rhs._zRotationAngle;

    //_forwardMovment = rhs._forwardMovment;
    //_leftMovment = rhs._leftMovment;
    //_upMovment = rhs._upMovment;
    _flightSpeed = rhs._flightSpeed;

    return *this;
}

void FlyCamera::recalcProjectionMatrix(){
	_projectionMatrix = glm::perspective(45.0f,
										 (float)_viewWidth / (float)_viewHeight,
										 .1f, 100.0f);
}

void FlyCamera::recalcViewTransformMatrix(){

#ifdef DEBUG_Camera_h
    assert( _viewWidth > 0 );
    assert( _viewHeight > 0 );
#endif

    recalcCameraMatrices();

    _viewMatrix = _cameraRotation * _cameraTranslation;


    return;

}

void FlyCamera::recalcCameraMatrices(){

    glm::mat4 idMat;
	
    _cameraTranslation = glm::translate(idMat,
										glm::vec3(-_location[0],
												  -_location[1],
												  -_location[2] ));
	
	_cameraRotation = glm::rotate(idMat, _horizonAngle,
								  glm::vec3(0,1,0));
	_cameraRotation = glm::rotate(_cameraRotation, _zRotationAngle,
								  glm::vec3(0,0,1));
}

void FlyCamera::mouseDrag( float dx, float dy ){
	
	const float mouseMoveRate = M_PI/180.0;
    _horizonAngle -= dy * mouseMoveRate;
    _zRotationAngle += dx * mouseMoveRate;
    
    if( _horizonAngle > max_horizonAngle )
        _horizonAngle = max_horizonAngle;
    if( _horizonAngle < min_horizonAngle )
        _horizonAngle = min_horizonAngle;

    recalcViewTransformMatrix();
}

void FlyCamera::setViewSize( int width, int height ){
    _viewWidth = width;
    _viewHeight = height;

    this -> recalcProjectionMatrix();
}

void FlyCamera::zoomIn( const float &z ){

}

void FlyCamera::tick( const SimTime &d ){

    glm::vec4 forward(-1,0,0,1);
    glm::vec4 left(0,-1,0,1);
    glm::vec4 up(0,0,1,1);

    glm::vec4 normalizedForward = forward * _cameraRotation;
    glm::vec4 normalizedLeft = left * _cameraRotation;
    glm::vec4 normalizedUp = up * _cameraRotation;
    
	// this should be updated for glm !!!
    _location[0] += _forwardMovment * normalizedForward[0] * _flightSpeed * .2;
    _location[1] += _forwardMovment * normalizedForward[1] * _flightSpeed * .2;
    _location[2] += _forwardMovment * normalizedForward[2] * _flightSpeed * .2;

    _location[0] += _leftMovment * normalizedLeft[0] * _flightSpeed * .2;
    _location[1] += _leftMovment * normalizedLeft[1] * _flightSpeed * .2;
    _location[2] += _leftMovment * normalizedLeft[2] * _flightSpeed * .2;

    _location[0] += _upMovment * normalizedUp[0] * _flightSpeed * .2;
    _location[1] += _upMovment * normalizedUp[1] * _flightSpeed * .2;
    _location[2] += _upMovment * normalizedUp[2] * _flightSpeed * .2;

    recalcViewTransformMatrix();
}

void FlyCamera::setForwardMovment( const float &d ){
    _forwardMovment = d;
}

void FlyCamera::setLeftMovment( const float &d ){
    _leftMovment = d;
}

void FlyCamera::setUpMovment( const float &d ){
    _upMovment = d;
}