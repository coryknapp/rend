//
//  Resources.h
//  Escher
//
//  Created by Cory Knapp on 9/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _Resources_
#define _Resources_

#include "OpenGLHelp.h"

#include <string>
#include <stdio.h>
#include <map>

class Resources {
    
    char * _path;

public:

    Resources( const char * path );
    ~Resources();
    
    std::string pathForResource( const char * path );
};

Resources * ResourcesSingleton();

#endif