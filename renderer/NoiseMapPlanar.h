//
//  NoiseMapPlanar.h
//  Strategy
//
//  Created by Cory Knapp on 3/28/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__NoiseMapPlanar__
#define __Strategy__NoiseMapPlanar__

#include <iostream>

#include "noiseutils.h"

#include "PlanarData.h"

class NoiseMapPlanar : public PlanarData<float> {
    
    noise::utils::NoiseMap _map;
    
public:
    
    NoiseMapPlanar( const noise::utils::NoiseMap &map ) :
    _map(map){
        _map = map;
    }

    
    virtual void setValue( const size_t &i, const size_t &j, const size_t &channel, const float &value  ){
        std::cerr << "NoiseMapPlanar::setValue - Can't call on NoiseMapPlanar" << std::endl;
    }

    virtual const float& getValue( const size_t &i, const size_t &j, const size_t &channel ){
        return *(_map.GetConstSlabPtr(int(i), int(j)));
    }
};

#endif /* defined(__Strategy__NoiseMapPlanar__) */
