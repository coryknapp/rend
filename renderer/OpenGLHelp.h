//
//  OpenGLHelp.h
//  Escher
//
//  Created by Cory Knapp on 9/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _OpenGLHelp_
#define _OpenGLHelp_

//#include <OpenGL/OpenGL.h>
//#include <OpenGL/gl3.h>
#include "glUtil.h"

#include <memory>
#include <map>
#include <string>
#include <iostream>
#include <assert.h>

#define CSTRING_FROM_VARIABLE_NAME( var ) #var

// http://en.wikipedia.org/wiki/X_Macro

// list format
// ( name, number of floats per vertex )
#define X_MACRO_LIST_OF_ATTRIBUTE_INDICES \
X_MACRO( POSITION_ATTRIBUTE_INDEX, 4 ) \
X_MACRO( NORMAL_ATTRIBUTE_INDEX, 3 ) \
X_MACRO( TEXTURECOORD_ATTRIBUTE_INDEX, 2 ) \
X_MACRO( VERTEXCOLOR_ATTRIBUTE_INDEX, 4 ) \
X_MACRO( INDEX_ATTRIBUTE_INDEX, 2 ) \
//X_MACRO( ATTRIBUTE_INDEX_COUNT, 0 )

enum {
#define X_MACRO( name, values_per_att ) name,
    X_MACRO_LIST_OF_ATTRIBUTE_INDICES
#undef X_MACRO
    ATTRIBUTE_INDEX_COUNT
};

const std::map<int, int>valuesPerAttributeMap = {
#define X_MACRO( name, values_per_att ) {name,values_per_att},
    X_MACRO_LIST_OF_ATTRIBUTE_INDICES
#undef X_MACRO
};

const char * const attributeMap[ATTRIBUTE_INDEX_COUNT] = {
#define X_MACRO( name, values_per_att ) CSTRING_FROM_VARIABLE_NAME( name ),
    X_MACRO_LIST_OF_ATTRIBUTE_INDICES
#undef X_MACRO
};

typedef short unsigned attributeMenuItem_t;
typedef attributeMenuItem_t attributeMenu_t[ATTRIBUTE_INDEX_COUNT]; // xxx this has a bad name
#define VALUE_COUNT_FOR_ATTRIBUTE( att ) (valuesPerAttributeMap.find( att )->second)
#define FOR_EACH_ATTRIBUTE_INDEX( index_n ) for(int index_n=0; index_n < ATTRIBUTE_INDEX_COUNT; index_n++)
#define CLEAR_ATTRIBUTE_MENU( am ) std::memset( (void *)am, 0, sizeof( attributeMenu_t ) )


inline std::string attributeNameForIndex( int at, attributeMenuItem_t n ){
    char name[ strlen( attributeMap[ at ] ) + 4];
    sprintf( name, "%s_%d", attributeMap[ at ], n);
    return std::string( name );
}

inline void dumpOpenGLState(){
    // we'll just add more stuff here as we need it.
    // http://www.opengl.org/sdk/docs/man/xhtml/glGet.xml
    
    GLint program;
    glGetIntegerv( GL_CURRENT_PROGRAM, &program);
    std::cout << "OpenGL state info:" << std::endl;
    std::cout << "program\t\t" << program << std::endl;
}



#define GetGLError()									\
{														\
GLenum err = glGetError();							\
while (err != GL_NO_ERROR) {						\
printf("GLError %s set in File:%s Function:%s Line:%d\n",	\
GetGLErrorString(err),					\
__FILE__,								\
__func__,                               \
__LINE__);								\
err = glGetError();						\
dumpOpenGLState();                      \
assert(0);\
}													\
}

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

//static GLsizei GetGLTypeSize(GLenum type);

#endif