//
//  Copyright 2013 Cory Knapp. All rights reserved.
//

#include "TextureBlendingRenderer.h"

TextureBlendingRenderer::TextureBlendingRenderer(GLuint defaultFBOName, size_t sampleCount)
    :Renderer( defaultFBOName ),
    _samplesFBOGroup( sampleCount )
{
    GetGLError();
}

void TextureBlendingRenderer::renderFullFrameQuad(){
    glBindVertexArray( _fullscreenQuadVAO.vaoName() );
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glDrawElements( _fullscreenQuadVAO.mode(), _fullscreenQuadVAO.elementCount(), GL_UNSIGNED_INT, 0 );
}

void TextureBlendingRenderer::render(){

#ifdef DEBUG_TextureBlendingRenderer_h
    assert( _blenderProgram != nullptr );
#endif

    // clear the fbo we're supposed to be drawing on
    glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    glClearColor( FRAMEBUFFER_CLEAR_COLOR );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // clear all our fbos.  Should this be a job for prepareTexturesForRender()?
    glClearColor( 1, 0, 1, 0);
    _samplesFBOGroup.clearAll();


    prepareTexturesForRender();

    // blend them together
    _blenderProgram->use();
    glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);

    for( int n = 0; n < _samplesFBOGroup.collectionSize(); n++ ){
        glActiveTexture(GL_TEXTURE0 + _samplesFBOGroup.textureColorName( n ));
        glBindTexture(GL_TEXTURE_2D, _samplesFBOGroup.textureColorName( n ));
        _blenderProgram->setSampleName( n, _samplesFBOGroup.textureColorName( n ) );
    }

    renderFullFrameQuad();
 
    GetGLError();
}


void TextureBlendingRenderer::destroyLayerTextures(){
    

}

void TextureBlendingRenderer::_createFullFrameQuadVAO(){
    _fullscreenQuadVAO = createFullFrameQuadVAO();
}

void TextureBlendingRenderer::resize(GLuint width, GLuint height){
    Renderer::resize( width, height);

    //glBindFramebuffer( GL_FRAMEBUFFER, _mainFBOName);
    //glViewport(0, 0, _viewWidth, _viewHeight);

    _samplesFBOGroup.resize( _viewWidth, _viewHeight);

}

void TextureBlendingRenderer::initialize(){
    GetGLError();

    _createFullFrameQuadVAO();

    this->initProgram();
    _samplesFBOGroup.build();

    Renderer::initialize();
}

void TextureBlendingRenderer::setProgram( BlenderGLSLProgram * blenderProg ){
    _blenderProgram = blenderProg;
}

// xxx maybe should be a subclass of VAO in it's own file?
VAO createFullFrameQuadVAO(){

#ifdef DEBUG_TextureBlendingRenderer_h
    // if we're debugging let's make the full frame a little less then full
    // that way, we can see what's going on near the edges of the main FBO
    const float lowerBound = -1.0;
    const float upperBound = 1.0;
#else
    const float lowerBound = -1.0;
    const float upperBound = 1.0;
#endif

    const size_t bufferSize = 6 * 4;
    GLfloat * data = (GLfloat *)std::malloc( sizeof(GLfloat)*( 29 ) );

    data[0] = lowerBound;     data[1] = lowerBound;     data[2] = -1;  data[3] = 1;        // bottom left
    data[4] = 0.0;     data[5] = 0.0;

    data[6] = lowerBound;     data[7] = upperBound;     data[8] = -1; data[9] = 1;        // top left
    data[10] = 0.0;     data[11] = 1.0;

    data[12] = upperBound;     data[13] = upperBound;     data[14] = -1;   data[15] = 1;     // top right
    data[16] = 1.0;     data[17] = 1.0;

    data[18] = upperBound;     data[19] = lowerBound;     data[20] = -1;   data[21] = 1;     // bottom right
    data[22] = 1.0;     data[23] = 0;

    Buffer<GLfloat> quadBuffer( data, bufferSize, false );

    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = 1;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = 1;

    GLuint vaoName = buildVAO( quadBuffer, menu, 4);
    VAO fullscreenQuadVAO;
    fullscreenQuadVAO.set( vaoName, 4, GL_TRIANGLE_FAN );
    return fullscreenQuadVAO;
}