//
//  GeneratedGLSLSource.h
//  assimp-test
//
//  Created by Cory Knapp on 7/13/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef assimp_test_GeneratedGLSLSource_h
#define assimp_test_GeneratedGLSLSource_h

const char * const vertSourceTop = "    \
#ifdef GL_ES                \n          \
precision highp float;      \n          \
#endif                      \n          \
                                        \
uniform mat4 modelViewProjectionMatrix; \
uniform vec3 inLightPos;                \
";

const char * const vertSourceBottom = "                                     \
out vec2 varTexcoord;                                                       \
out float varLightLevel;                                                    \
                                                                            \
void main (void)                                                            \
{                                                                           \
	gl_Position	= modelViewProjectionMatrix * POSITION_ATTRIBUTE_INDEX_0;   \
    varTexcoord = TEXTURECOORD_ATTRIBUTE_INDEX_0;                           \
                                                                            \
    vec4 lightDir = normalize( vec4( 5, 5, -5, 0 ) );                       \
    lightDir *= modelViewProjectionMatrix;                                  \
    varLightLevel = dot( vec3( lightDir ), NORMAL_ATTRIBUTE_INDEX_0 );      \
                                                                            \
}";

const char * const fragSourceTop = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif                      \n      \
                                    \
in vec2 varTexcoord;                \
in vec3 varNormal;                  \
in vec4 varTintColor;               \
in float varDepth;                  \
in float varLightLevel;             \
                                    \
uniform sampler2D inTexture;        \
                                    \
out vec4 fragColor;                 \
                                    \
void main (void)                    \
{                                   \
    gl_FragDepth = gl_FragCoord.z;  \
                                    \
    fragColor = texture( inTexture, varTexcoord );  \
    fragColor.a = 1.0;              \
}";

#endif
