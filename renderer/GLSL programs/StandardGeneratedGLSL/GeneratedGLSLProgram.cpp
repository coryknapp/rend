//
//  GeneratedGLSLProgram.cpp
//
//  Created by Cory Knapp on 1/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#include "GeneratedGLSLProgram.h"

std::string attributeCodeForMenu( attributeMenu_t menu ){
    std::ostringstream stringStream;
    FOR_EACH_ATTRIBUTE_INDEX( att_index ){
        for( attributeMenuItem_t n = 0; n< menu[att_index]; n++ ){
            stringStream << "in vec" << VALUE_COUNT_FOR_ATTRIBUTE( att_index ) << " " << attributeNameForIndex(att_index,n) << ";\n";
        }
    }
    return stringStream.str();
}

void GeneratedGLSLProgram::build(){
    std::cerr << "no GeneratedGLSLProgram::build()." << std::endl;
}

void GeneratedGLSLProgram::build( attributeMenu_t menu ){

    std::ostringstream stringStream;
    stringStream << _vertexShaderHead;

    for(auto it = _uniforms.begin(); it != _uniforms.end(); ++it) {
        stringStream << "uniform " << it->type << " " << it->name << ";\n";
    }

    stringStream << attributeCodeForMenu( menu );
    stringStream << _vertexShaderBody;

    std::string fragmentSrc = _fragmentShaderBody;
    _programName = buildProgram( stringStream.str(), fragmentSrc, menu );
    GetGLError();

    initBindings();
}

void GeneratedGLSLProgram::includeUniform(  std::string type, std::string name ){
    UniformDetail newDetail;
    newDetail.type = type;
    newDetail.name = name;
    _uniforms.push_back( newDetail );
}

void GeneratedGLSLProgram::setVertexShaderHead( std::string glslCode ){
    _vertexShaderHead = glslCode;
}

void GeneratedGLSLProgram::setVertexShaderBody( std::string glslCode ){
    _vertexShaderBody = glslCode;
}

void GeneratedGLSLProgram::setFragmentShaderBody( std::string glslCode ){
    _fragmentShaderBody = glslCode;
}


void GeneratedGLSLProgram::initBindings(){
    assert( false );
}