//
//  StandardGLSLProgram.h
//  Escher
//
//  Created by Cory Knapp on 1/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#ifndef GeneratedGLSLProgram_def
#define GeneratedGLSLProgram_def

#include "GLSLProgram.h"
#include "Texture.h"

#include <sstream>
#include <iostream>
#include <vector>

struct UniformDetail{
    std::string type;
    std::string name;
};

std::string attributeCodeForMenu( attributeMenu_t menu );

class GeneratedGLSLProgram : public GLSLProgram{

    std::vector<UniformDetail> _uniforms;
    std::string _vertexShaderHead;
    std::string _vertexShaderBody;
    std::string _fragmentShaderBody;

public:

    virtual void build(); // does nothing
    virtual void build( attributeMenu_t menu );

    virtual void includeUniform(  std::string type, std::string name );

    virtual void setVertexShaderHead( std::string glslCode );
    virtual void setVertexShaderBody( std::string glslCode );
    virtual void setFragmentShaderBody( std::string glslCode );

    virtual void initBindings();

    //    virtual const bool * atributesArray();
};

#endif