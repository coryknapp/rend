//
//  BlenderGLSLProgram.h
//  Escher
//
//  Created by Cory Knapp on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#ifndef BlenderGLSLProgram_def
#define BlenderGLSLProgram_def

#include "macro.h"

#include "GLSLProgram.h"

#include <assert.h>

#include <string>
#include <sstream>

#define BLENDER_GLSL_MAX_SAMPLE_COUNT 64

#define DEBUG_BlenderGLSLProgram_h

class BlenderGLSLProgram : public GLSLProgram{
    
    size_t _sampleCount;
    GLint _sampleName[ BLENDER_GLSL_MAX_SAMPLE_COUNT ];
    
public:

    virtual void build( const std::string &blendCode, size_t sampleCount );

    virtual const char * vertexShaderName();
    virtual const char * fragmentShaderName();
        
    virtual void initBindings();
    
    void setSampleName( size_t index, GLint sampleName );

#ifdef DEBUG_BlenderGLSLProgram_h
    bool _didWeBuild_debugflag = false;
#endif
};

const char * const blenderVertProgram = "               \
#ifdef GL_ES                        \n                  \
precision highp float;              \n                  \
#endif                              \n                  \
                                                        \
in vec4 POSITION_ATTRIBUTE_INDEX_0;                     \
in vec2 TEXTURECOORD_ATTRIBUTE_INDEX_0;                 \
out vec2 varTexcoord;                                   \
                                                        \
void main (void)                                        \
{                                                       \
    gl_Position	=  POSITION_ATTRIBUTE_INDEX_0;          \
    varTexcoord = TEXTURECOORD_ATTRIBUTE_INDEX_0;       \
}";

const char * const blenderFragProgram1 = "\
#ifdef GL_ES                        \n"
"precision highp float;              \n"
"#endif                              \n"

"in vec2 varTexcoord;"
"out vec4 fragColor;";


const char * const blenderFragProgram2 = "               \
void main (void)                                        \
{                                                       \
";

const char * const blenderFragProgram3 = "               \
return;                                             \
}";

#endif