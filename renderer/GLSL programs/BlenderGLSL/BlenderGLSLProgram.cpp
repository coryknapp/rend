//
//  BlenderGLSLProgram.cpp
//  Escher
//
//  Created by Cory Knapp on 1/21/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#include "BlenderGLSLProgram.h"

void BlenderGLSLProgram::build( const std::string &blendCode, size_t sampleCount ){

#ifdef DEBUG_BlenderGLSLProgram_h
    assert( _didWeBuild_debugflag == false ); // if this bell rings, we've tryed to build the program twice.
    _didWeBuild_debugflag = true;
#endif

    _sampleCount = sampleCount;

    std::string vertexSrc = blenderVertProgram;

    std::ostringstream fragStream;
    fragStream << blenderFragProgram1;

    // add in the samples
    for( int n = 0; n < _sampleCount; n++ ){
        // will break for shaders with over 99 samples


        fragStream << "uniform sampler2D SAMPLE_" << n << ";" << std::endl;;
    }

    fragStream << blenderFragProgram2;
    fragStream << blendCode;
    fragStream << blenderFragProgram3;


    std::string fragmentSrc = fragStream.str();


    attributeMenu_t menu;
    CLEAR_ATTRIBUTE_MENU( menu );
    menu[POSITION_ATTRIBUTE_INDEX] = true;
    menu[TEXTURECOORD_ATTRIBUTE_INDEX] = true;

    GetGLError();

    _programName = buildProgram( vertexSrc, fragmentSrc, menu );

    initBindings();
}

const char * BlenderGLSLProgram::vertexShaderName(){
    return "BlenderGLSLProgram.vsh";
}

const char * BlenderGLSLProgram::fragmentShaderName(){
    return "BlenderGLSLProgram.fsh";
}

void BlenderGLSLProgram::initBindings(){
#ifdef DEBUG_BlenderGLSLProgram_h
    assert( _didWeBuild_debugflag == true );
#endif
    for( int n = 0; n < _sampleCount; n++ ){
        // will break for shaders with over 99 samples
        size_t nameLength = n<10 ? 8 : 9;
        char name[ nameLength ];
        strcpy( name, "SAMPLE_" );
        if( nameLength == 10 ){
            name[7] = '1';
            name[8] = '0' + (n % 10);
        } else {
            name[7] = '0' + n;
        }
        name[ nameLength ] = 0;

        _sampleName[n] = glGetUniformLocation( _programName , name);
#ifdef DEBUG_BlenderGLSLProgram_h
        assert( _sampleName[n] != -1); // no sample found.
                                       // NOTE that this will also trip when/if the sample isn't used in the program;
#endif
    }
}

void BlenderGLSLProgram::setSampleName(  size_t index, GLint sampleName ){
#ifdef DEBUG_BlenderGLSLProgram_h
    assert( _didWeBuild_debugflag == true );
    assert( sampleName > 0 );
#endif
    glUniform1i( _sampleName[ index ], sampleName);
    GetGLError();  // this may blow if the sample (SAMPLE_X) isn't referancved in the program's source code.
}