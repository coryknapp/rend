//
//  ThreeDRenderer.h
//  noise
//
//  Created by Cory Knapp on 8/19/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __ThreeDRenderer__
#define __ThreeDRenderer__

#include "Renderer.h"

#include <iostream>
#include <string>

#include "Renderer.h"
#include "OpenGLHelp.h"
#include "Camera.h"
#include "math.h"

#include "FrameBufferObjectCollection.h"
#include "BlenderGLSLProgram.h"

#define FRAMEBUFFER_CLEAR_COLOR .5,0,1,1 // sort of a bluish purple

class ThreeDRenderer : public Renderer {

protected:

    Camera * _camera;

    // for now, i think it'll be best if we have the subclasses provide the FBO on which it
    // renders it's scene, as well as the public accessor functions.

public:

    ThreeDRenderer(GLuint defaultFBOName);
    ~ThreeDRenderer();

    void setCamera( Camera * camera );
    Camera * camera();

    void resize(GLuint width, GLuint height);

    virtual void initialize(){
        ///
        ///
        ///
        Renderer::initialize();
    }

    virtual void render();
    // called when for each frame.
    // the ThreeDRenderer render() clears the buffer and sets the mvpMatrix for your convenience.
};


#endif /* defined(__ThreeDRenderer__) */

