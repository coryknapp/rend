//
//  PlanarData.h
//  Strategy
//
//  Created by Cory Knapp on 11/5/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__PlanarData__
#define __Strategy__PlanarData__

#include <math.h>
#include <iostream>
#include <memory>
#include <cassert>

template <typename T>
class PlanarData {

protected:

    T * _data = nullptr;
    size_t _height;
    size_t _width;
    size_t _channels;

public:

    PlanarData() :
        _height(0),
        _width(0),
        _channels(0),
        _data(nullptr){

    }

    PlanarData( const size_t& height,const size_t& width,const size_t& channels ) :
        _height(height),
        _width(width),
        _channels(channels){
            _data = (T*)std::malloc( sizeof(T) * _width * _height * channels );
    }

    ~PlanarData(){
        if( _data )
            std::free( _data );
    }

    size_t height() const{   return _height;      }
    size_t width() const{    return _width;       }
    size_t channels() const{ return _channels;    }
    void * ptr() const { return (void *)_data;    }

    void setAll( const T& clearValue ){
        for( int i = 0; i < width(); i++ ){
            for( int j = 0; j < height(); j++ ){
                for( int n = 0; n < channels(); n++ )
                    setValue(i, j, n, clearValue);
            }
        }
    }

    virtual void setValue( const size_t &i, const size_t &j, const size_t &channel, const T &value  ){
        assert( i < _width );
        assert( j < _height );
        assert( channel < _channels );

        _data[ _channels * (j * width() + i) + channel ] = value;
    }

    virtual const T& getValue( const size_t &i, const size_t &j, const size_t &channel ){
        assert( i < _width );
        assert( j < _height );
        assert( channel < _channels );

        //std::cout<< "_channels * (j * _width + i) + channel = " << _channels * (j * _width + i) + channel << '\n';
        return _data[ _channels * (j * _width + i) + channel ];

    }

    virtual const T& getWrappedValue( const size_t &i, const size_t &j, const size_t &channel ){ /// xxx should be able to accept negitive sizes, if we're wrapping.

        return getValue( i %_width, j %_height, channel);
        
    }

    virtual float getInterpolationValue( const float &i, const float &j, const size_t &channel ){
        assert( i >= 0 );
        assert( j >= 0 );
        assert( i <= 1 );
        assert( j <= 1 );
        assert( channel < _channels );

        // xxx now a value of .5 means the value right in the middle of the plain.
        // maybe we need a function where .5 would be the value between pixel 0 and pixel one?
        
        struct interpolationPoint {
            size_t x,y;
            float normalizedX, normalizedY;
            float value;
        };

        interpolationPoint p11;
        p11.x = i * _width;
        p11.normalizedX = (float)p11.x/(_width+1);
        p11.y = j * _height;
        p11.normalizedY = (float)p11.y/(_height+1);

        interpolationPoint p22;
        p22.x = p11.x + 1;
        p22.normalizedX = (float)p22.x/(_width+1);
        p22.y = p11.y + 1;
        p22.normalizedY = (float)p22.y/(_height);

        interpolationPoint p12;
        p12.x = p11.x;
        p12.normalizedX =  (float)p11.normalizedX;
        p12.y = p22.y;
        p12.normalizedY =  (float)p22.normalizedY;

        interpolationPoint p21;
        p21.x = p22.x;
        p21.normalizedX =  (float)p22.normalizedX;
        p21.y = p11.x;
        p21.normalizedY =  (float)p11.normalizedY;

        p11.value = getWrappedValue( p11.x, p11.y, channel );
        p12.value = getWrappedValue( p12.x, p12.y, channel );
        p22.value = getWrappedValue( p22.x, p22.y, channel );
        p21.value = getWrappedValue( p21.x, p21.y, channel );

        return p11.value; // xxxxxxxxxxxxxxxxxxxx

#define printIP( ip ) std::cout << "value = " << ip.value << " normalizedX = " << ip.normalizedX << " normalizedY = " << ip.normalizedY << std::endl

        //printIP( p11 );
        //printIP( p12 );
        //printIP( p21 );
        //printIP( p22 );

        float returnVal =
            ( 1.0/((p22.normalizedX-p11.normalizedX)*(p22.normalizedY-p11.normalizedY) ) )

        * (
            p11.value * (p22.normalizedX - i ) * (p22.normalizedY - j ) +
            p21.value * (i - p11.normalizedX ) * (p22.normalizedY - j ) +
            p12.value * (p22.normalizedX - i ) * (j - p11.normalizedY ) +
            p22.value * (i - p11.normalizedX ) * (j - p11.normalizedY )
                                                                                );
        //assert(returnVal >= 0 );
        //assert(returnVal < 256 );
        return returnVal;

    }

    virtual float getWrappedInterpolationValue( const float &i, const float &j, const size_t &channel ){

        float throwaway;
        float ip = modff( i, &throwaway);
        float jp = modff( j, &throwaway);
        if( ip < 0 )
            ip += 1.0;
        if( jp < 0 )
            jp += 1.0;

        assert( ip >= 0 );
        assert( jp >= 0 );
        assert( ip <= 1 );
        assert( jp <= 1 );

        float returnVal = getInterpolationValue( ip, jp, channel );
        //assert(returnVal >= 0 ); // these are useless without knowing what type T is.
        //assert(returnVal <= 1 );
        return returnVal;
    }

};

#endif /* defined(__Strategy__PlanarData__) */
