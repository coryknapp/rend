//
//  ImageFilePlanarData.h
//  Strategy
//
//  Created by Cory Knapp on 12/16/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__ImageFilePlanarData__
#define __Strategy__ImageFilePlanarData__

#include <cassert>

#include <iostream>
#include <png.h>
#include "PlanarData.h"

// a lot of stuff here is less then ideal.  We shouldn't have to declare the height and width
// when we construct, we should be able to use uint8 if the png has that depth.
// We should be able to usse non-RGBA pngs. (now we can)
// More probably. xxx

std::unique_ptr<class ImageFilePlanarData> planarDataForImageFile( const std::string& path );

class ImageFilePlanarData : public PlanarData<uint8_t> {

public:

    float normalizedValue( const float &i, const float &j, const size_t &channel ){
        assert( i >= 0 );
        assert( i <= 1 );
        assert( j >= 0 );
        assert( j <= 1 );
        return ( getInterpolationValue( i, j, channel ) /UINT8_MAX );
    }



    ImageFilePlanarData( const size_t &height, const size_t &width, const size_t &channels, png_byte * image_data );
    // implementation in .cpp file
};


#endif /* defined(__Strategy__ImageFilePlanarData__) */
