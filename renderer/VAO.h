//
//  VAO.h
//  Orbit
//
//  Created by Cory Knapp on 5/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef __Orbit__VAO__
#define __Orbit__VAO__

#include <iostream>

#include <boost/utility.hpp>

#include "OpenGLHelp.h"
#include "Resources.h"
#include "Buffer.h"
#include "imageUtil.h"

#define DEBUG_VAO_h

GLuint buildVAO( const Buffer<GLfloat> &buff, const attributeMenu_t &attributeMenu, const GLuint &vertexCount, GLint * elementArray = nullptr, const GLint &elementCount = 0 );
void destroyVAO( GLuint vaoName);

size_t strideForMenu( const attributeMenu_t &attributeMenu );

class VAO : boost::noncopyable {
    
    GLuint _vaoName;
    GLuint _elementCount;
    GLenum _mode = GL_TRIANGLES;

    //void _retain();
    //void _release();

public:
    
    VAO();
    VAO( GLuint vaoName, GLuint elementCount, GLuint mode );

    VAO( const VAO &other ); // copy constructor
    VAO& operator = (const VAO & r){ // copy assignment
        if (vaoName() != r.vaoName())
            // if name is the same, the VAOs are the same
        {
            _vaoName = r._vaoName;
            _elementCount = r._elementCount;
            _mode = r._mode;
            assert(false); // should never be copied.  Only moved.
        }
        return *this;
    }
    
    VAO( VAO &&other ); // move constructor
    VAO& operator = (VAO&& r) //  move assignment
    {
        if (vaoName() != r.vaoName())
            // if the name is the same, the VAOs are the same
        {
            _vaoName = r._vaoName;
            _elementCount = r._elementCount;
            _mode = r._mode;
            r.set( 0, 0, 0);
        }
        return *this;
    }

    void set( GLuint vaoName, GLuint elementCount, GLuint mode );
    
    VAO( const char * objFileName );
    
    ~VAO();
    
    GLuint vaoName() const;
    GLuint elementCount() const;
    GLenum mode() const;
#ifdef DEBUG_VAO_h
    friend std::ostream& operator << (std::ostream &out, const VAO &);
#endif
};

#ifdef DEBUG_VAO_h
void printVAOLayoutDescription( const attributeMenu_t &attributeMenu );
#endif

#endif /* defined(__Orbit__VAO__) */
