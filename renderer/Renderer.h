//
//  Renderer.h
//
//  Created by Cory Knapp on 4/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//








#ifndef _Renderer_
#define _Renderer_

#define _Renderer_debug_

#include <string>

#include "OpenGLHelp.h"
#include "Camera.h"
#include "math.h"

#include "imageUtil.h"
#include "sourceUtil.h"
#include "imageUtil.h"

#include "FrameBufferObjectCollection.h"
#include "BlenderGLSLProgram.h"
#include "Texture.h"
#include "VAO.h"

#include <mutex>

#define FRAMEBUFFER_CLEAR_COLOR .5,0,1,1 // sort of a bluish purple

#define DEBUG_Renderer_h

class Renderer {
            
protected:

    GLuint _viewWidth;
    GLuint _viewHeight;
    GLuint _mainFBOName;

    bool _readyToRender = 0;
    std::mutex _naiveMasterLock; // to assure we're done with our setup before we try to render with it, which can happen.

public:


    Renderer(GLuint defaultFBOName);
    virtual ~Renderer();


    bool readyToRender(){ return _readyToRender; };
    void declareReadyToRender(){
        _naiveMasterLock.lock();
        _readyToRender = true;
        _naiveMasterLock.unlock();
    }
    void renderIfReady(){
        if( readyToRender())
            this -> render();
    };

    virtual void resize(GLuint width, GLuint height);
    // override to receive resizing events, but sure to call
    // the superclass's ::resize(...)

    GLuint viewWidth(){ return _viewWidth; }
    GLuint viewHeight(){ return _viewHeight; }

    virtual void render() = 0;
    virtual void initialize(){
    // Override this to initialize programs and framebufferobjects,
    // but be sure to call Renderer::initialize() in your subclasses at THE LAST LINE.
    // Guaranteed to be called while the OpenGL context is active.
    // be sure to call declareReadyToRender() right after.
#ifdef DEBUG_Renderer_h
        assert( _viewWidth > 0 );   // the Renderer must be resized before being built
        assert( _viewHeight > 0);
        assert( _didWeInit_debugflag == false ); // If this bell rings, it's because we've tryed
                                                 // to initialize the renderer more then once.
        _didWeInit_debugflag = true;
#endif
    }

#ifdef DEBUG_Renderer_h
    bool _didWeInit_debugflag = false;
#endif
};

#endif