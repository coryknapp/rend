//
//  Random.cpp
//  Strategy
//
//  Created by Cory Knapp on 2/24/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#include "Random.h"

template <typename T>
T random0to1() {
    std::cerr << "random0to1() not defined for type." << std::endl;
}

template <>
float random0to1<float>() {
    return (float)rand()/RAND_MAX;
}

template <>
uint8_t random0to1<uint8_t>() {
    return rand() % UINT8_MAX;
}