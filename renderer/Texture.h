//
//  Texture.h
//  assimp-test
//
//  Created by Cory Knapp on 7/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#ifndef __assimp_test__Texture__
#define __assimp_test__Texture__

#include <iostream>

#include "OpenGLHelp.h"
#include "imageUtil.h"
#include "PlanarData.h"

#include <png.h>
#include <boost/utility.hpp>

#define DEBUG_Texture_h

class Texture /*: boost::noncopyable maybe later*/ {
    GLuint _texName;

    std::string _lastError;

public:

    Texture();
    bool loadFile( const char * filePath ); // xxx I want this gone and the PlanerData usage instead.
    bool loadPlanerData( const PlanarData<float> *plane);
    bool loadPlanerData( const PlanarData<uint8_t> * plane);

    GLint textureName() const;

    std::string getError();

    ~Texture();
};


GLuint buildTexture(const char * file_name, int * width, int * height);

#endif /* defined(__assimp_test__Texture__) */
