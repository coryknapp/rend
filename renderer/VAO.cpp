//
//  VAO.cpp
//  Orbit
//
//  Created by Cory Knapp on 5/23/13.
//  Copyright (c) 2013 Cory Knapp. All rights reserved.
//

#include "VAO.h"

GLuint buildVAO( const Buffer<GLfloat> &buff, const attributeMenu_t &attributeMenu, const GLuint &vertexCount, GLint * elementArray, const GLint &elementCount){
    // xxx  maybe we'll need to set up some method for aligning bytes.

    GLuint vaoName;
    // Create a vertex array object (VAO) to cache model parameters
    glGenVertexArrays(1, &vaoName);
    glBindVertexArray(vaoName);

    GLuint vboName;
    // Create a vertex buffer object (VBO)
    glGenBuffers(1, &vboName);
    glBindBuffer(GL_ARRAY_BUFFER, vboName);


    int stride = (int)strideForMenu( attributeMenu );

    glBufferData(GL_ARRAY_BUFFER,  vertexCount * sizeof(GLfloat) * stride, buff.buffer(), GL_STATIC_DRAW);

    int bufferOffset = 0; // accumulates the offsets

    GetGLError();

    FOR_EACH_ATTRIBUTE_INDEX( index_n ) {
        if( attributeMenu[index_n] > 0 ){
            glEnableVertexAttribArray(index_n);
            glVertexAttribPointer(index_n, VALUE_COUNT_FOR_ATTRIBUTE( index_n ) , GL_FLOAT, GL_FALSE, stride * sizeof( GLfloat ), BUFFER_OFFSET(bufferOffset * sizeof(GLfloat)));
            bufferOffset += VALUE_COUNT_FOR_ATTRIBUTE( index_n );
        }
    }
    GetGLError();

#ifdef DEBUG_VAO_h
    assert( stride == bufferOffset );
#endif

    int finalElementCount;
    bool deleteElementArray = false;
    if( !elementArray ) {
        elementArray = new GLint[vertexCount];
        deleteElementArray = true;

        for ( int n=0; n<vertexCount; n++ ) {
            elementArray[n] = n;
        }
        finalElementCount = vertexCount;
    }
    else{ // we did pass an elementArray
#ifdef DEBUG_VAO_h
        assert( elementCount != 0); // You passed an elementArray, but no elementCount.
#endif
        finalElementCount = elementCount;
    }

    GLuint elementBufferName;
    GetGLError();

    // Create a VBO to vertex array elements
    // This also attaches the element array buffer to the VAO
    glGenBuffers(1, &elementBufferName);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferName);
    GetGLError();

    // Allocate and load vertex array element data into VBO
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (finalElementCount) * sizeof( GLint ), elementArray, GL_STATIC_DRAW);

    GetGLError();

    if( deleteElementArray ) {
        delete elementArray;
    }

#ifdef DEBUG_VAO_h
    assert( glIsVertexArray(vaoName) );
#endif

    return vaoName;
}

void destroyVAO( GLuint vaoName){
    GLuint index;
    GLuint bufName;

    // Bind the VAO so we can get data from it
    glBindVertexArray(vaoName);

    // For every possible attribute set in the VAO
    for(index = 0; index < 16; index++)
    {
        // Get the VBO set for that attibute
        glGetVertexAttribiv(index , GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING, (GLint*)&bufName);

        // If there was a VBO set...
        if(bufName)
        {
            //...delete the VBO
            glDeleteBuffers(1, &bufName);
        }
    }

    // Get any element array VBO set in the VAO
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, (GLint*)&bufName);

    // If there was a element array VBO set in the VAO
    if(bufName)
    {
        //...delete the VBO
        glDeleteBuffers(1, &bufName);
    }

    // Finally, delete the VAO
    glDeleteVertexArrays(1, &vaoName);
    
    GetGLError();
}

inline size_t strideForMenu( const attributeMenu_t &attributeMenu ){
    int stride = 0;
    FOR_EACH_ATTRIBUTE_INDEX( n ){
        if( attributeMenu[ n ] ){
            stride += VALUE_COUNT_FOR_ATTRIBUTE( n );
        }
    }
    return stride;
}

/*void VAO::_retain(){
    (*_refCount)++;
}

void VAO::_release(){
    (*_refCount)--;
    /// debugging test
    if( _refCount <= 0 )
        std::cout << *this << " should be gone by now!" << std::endl;
}*/

VAO::VAO() :
    _vaoName( 0 ),
    _elementCount( 0 ),
    _mode( 0 )
{

}

VAO::VAO( GLuint vaoName, GLuint elementCount, GLuint mode ) :
    _vaoName( vaoName ),
    _elementCount( elementCount ),
    _mode( mode )
{

}

VAO::VAO( const VAO &other ) : // copy constructor
    _vaoName( other.vaoName() ),
    _elementCount( other.elementCount() ),
    _mode( other.mode() )
{
    std::cout << "VAO copy constructor\n";
    assert(false); // should never be copied.  Only moved.
}

VAO::VAO( VAO&& other ) : // move constructor
    _vaoName( other.vaoName() ),
    _elementCount( other.elementCount() ),
    _mode( other.mode() ){
        std::cout << "VAO move constructor\n";

        _vaoName = other._vaoName;
        _elementCount = other._elementCount;
        _mode = other._mode;

        // set the other's VAO name to 0, so it's not deleted when other is deconstructed
        other.set( 0, 0, 0);
        //other._release();
    }

void VAO::set( GLuint vaoName, GLuint elementCount, GLuint mode ){
    _vaoName = vaoName;
    _elementCount = elementCount;
    _mode = mode;
}

VAO::~VAO(){
    if( _vaoName > 0){
        //assert(false);// for now, prevent VAOs from being destructed.
        //xxx are we sure that all vao names are non-zero?
        std::cout << "warning: deleteing VAO " << _vaoName << ".\n";
        destroyVAO( _vaoName );
    }
}

GLuint VAO::vaoName() const{
    return _vaoName;
}

GLuint VAO::elementCount() const{
    return _elementCount;
}

GLenum VAO::mode() const{
    return _mode;
}

#ifdef DEBUG_VAO_h
std::ostream& operator << (std::ostream &out, const VAO &vao_)     //output
{
    out << "( VAO name = "<< vao_._vaoName << "\telementCount =" << vao_._elementCount << "\t";
    switch ( vao_._mode ) {
        case GL_TRIANGLES:
            out << "GL_TRIANGLES";
            break;
        case GL_TRIANGLE_STRIP:
            out << "GL_TRIANGLE_STRIP";
            break;
        case GL_TRIANGLE_FAN:
            out << "GL_TRIANGLE_FAN";
            break;
        default:
            out << "unknown type";
            break;
    }
    return out;
}

void printVAOLayoutDescription( const attributeMenu_t &attributeMenu ){
    int stride = 0;
    FOR_EACH_ATTRIBUTE_INDEX( n ){
        if( attributeMenu[ n ] > 0 ){
            std::cout << attributeNameForIndex( n , 0 ) << "\t" << stride << " to ";
            stride += VALUE_COUNT_FOR_ATTRIBUTE( n );
            std::cout << stride << std::endl;
        }
    }
}
#endif