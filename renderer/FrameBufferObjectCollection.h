//
//  FrameBufferObject.h
//  Escher
//
//  Created by Cory Knapp on 1/22/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//
/*
 
    A collection of framebuffer objects that all share the same depth buffer
 
 */


#ifndef FrameBufferObjectCollection_def
#define FrameBufferObjectCollection_def

//#include <OpenGL/OpenGL.h>

#include "OpenGLHelp.h"
#include <memory>
#include <iostream>
#include <cstdlib>

#define FBO_COLLECTION_CLEAR_COLOR .5,1,.8,0 // sort of a pail green

#define DEBUG_FrameBufferObjectCollection_h
#include <assert.h>

GLuint buildFBO( GLuint width, GLuint height, GLuint &colorTexture, GLuint &depthTexture);
void destroyFBO(GLuint fboName);
void deleteFBOAttachment(GLenum attachment);


class FrameBufferObjectCollection{
        
    GLuint *_fboArray;
    GLuint *_textureColorNameArray;
    GLuint _depthRenderBufferName;
        
    int _width;
    int _height;
    size_t _collectionSize;
    bool _fboBuilt; // are the fbos up and running?
    
    void _cleanUp();
    
public:
    
    FrameBufferObjectCollection( size_t collectionSize );
    
    ~FrameBufferObjectCollection();
    
    void setDepthRenderBuffer( GLint depthRenderBuffer );
    
    void build();
    void bind( int );
    
    void clear();
    void clearAll();
    void clearDepth();
    
    void resize( int width, int height );

    GLuint textureColorName( int ) const;
    GLuint fboName( int ) const;
    GLuint depthRenderBufferName() const;
    size_t collectionSize() const { return _collectionSize; };

#ifdef DEBUG_FrameBufferObjectCollection_h
    bool _didWeInit_debugflag = false;
#endif

};

#endif