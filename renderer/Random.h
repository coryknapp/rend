//
//  Random.h
//  Strategy
//
//  Created by Cory Knapp on 2/24/14.
//  Copyright (c) 2014 Cold Soda. All rights reserved.
//

#ifndef __Strategy__Random__
#define __Strategy__Random__

#include <iostream>

template <typename T>
T random0to1();

template <>
float random0to1<float>();

template <>
uint8_t random0to1<uint8_t>();

#endif /* defined(__Strategy__Random__) */
