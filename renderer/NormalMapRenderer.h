//
//  NormalMapRenderer.h
//  Strategy
//
//  Created by Cory Knapp on 10/13/13.
//  Copyright (c) 2013 Cold Soda. All rights reserved.
//

#ifndef __Strategy__NormalMapRenderer__
#define __Strategy__NormalMapRenderer__

#include <iostream>
#include "TextureBlendingRenderer.h"
#include "GeneratedGLSLProgram.h"
#include "Texture.h"
#include "FrameBufferObjectCollection.h"

// convenience function for quickly getting a normal map.
// (and a good example of how to use NormalMapRenderer.)

/*
 I'm rendering a map of normal vectors which I'm calculating from a height map (as a passed in texture)
 onto another texture.  As I understand it, RGBA values will be clamped into [1,0], where as the vector
 components should be in [-1,1] (in this case, although in other applications it could fall outside of this,
 i guess)

 */

void convertFromColorSpaceToVectorSpace( float * fourFloats );
void convertFromVectorSpaceToColorSpace( float * fourFloats );

void normalMapTextureForHeightMap( FrameBufferObjectCollection &heightMapFBO, unsigned height, unsigned width, const Texture &heightMap );

class NormalMapProgram : public GeneratedGLSLProgram{

    GLint _textureName;

public:
    void build();
    void initBindings();
    void setTexture( GLint textureName );
};

class NormalMapRenderer : public Renderer{

    Texture _tex;
    NormalMapProgram _program;

    VAO _fullscreenQuadVAO;

    void _renderFullFrameQuad();

public:

    NormalMapRenderer( GLuint fbo );

    void setTexture( const Texture& tex );

    virtual void resize(GLuint width, GLuint height);
    void render();
    void destroyLayerTextures();

    virtual void initialize();

};


const char * const normalMapProgramVertSourceTop = "\
#ifdef GL_ES                            \n\
precision highp float;                  \n\
#endif                                  \n";

// attributes will be inserted here

const char * const normalMapProgramVertSourceBottom =
"out vec2 texcoord;"
"void main (void)"
"{"
"gl_Position	= POSITION_ATTRIBUTE_INDEX_0;"
"texcoord = TEXTURECOORD_ATTRIBUTE_INDEX_0;"
"}";

const char * const normalMapProgramFragSource = "\
#ifdef GL_ES                \n      \
precision highp float;      \n      \
#endif                      \n"
"in vec2 texcoord;"
"uniform sampler2D heightMap;"
"out vec4 fragColor;"
"void main (void)"
"{"
"vec4 sampleColor = texture(heightMap, texcoord);"
"const vec2 size = vec2(.015,0);"       // smaller normal x values will yield normals farther from k (0,0,1).
                                        // xxx we'll need a mechanism to adjust it later.
"const ivec3 off = ivec3(2,0,-2);"
"float right = textureOffset(heightMap, texcoord, off.xy).a;"   // off = (1,0)
"float left = textureOffset(heightMap, texcoord, off.zy).a;"   // off = (-1,0)
"float up = textureOffset(heightMap, texcoord, off.yx).a;"      // off = (0,1)
"float down = textureOffset(heightMap, texcoord, off.yz).a;"    // off = (0,-1)
"vec3 va = normalize( vec3( size.xy, left-right));" // vec3( X, 0, dif )
"vec3 vb = normalize( vec3( size.yx, down-up));"    // vec3( 0, X, dif )
"vec3 normal = normalize( cross(va,vb) );" // "vec3 normal = normalize( cross(va,vb));"
"fragColor = ( vec4( normal, 1.0 ) + 1)/2.0;"
"}";

#endif /* defined(__Strategy__NormalMapRenderer__) */
